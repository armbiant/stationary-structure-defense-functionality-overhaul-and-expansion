package org.ithirahad.bastioninitiative.vfx.particle;

import api.utils.particle.ModParticle;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.graphicsengine.core.Controller;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import static org.schema.common.FastMath.sign;

public class RadialHexParticle extends ModParticle {
    /**
     * @param origin radial point to come from visually
     * @param start actual point to start the particle at
     * @param scale the scale of the effect
     */

    private final Vector4f originalColor;
    private final float originalScale;
    private final Vector4f zero = new Vector4f(0.0f,1.0f,0.85f,0f);

    protected RadialHexParticle(Vector3f origin, Vector3f start, float scale) {
        velocity = new Vector3f(origin);
        velocity.sub(start);
        velocity.scale(-scale * 0.0005f);
        originalScale = scale;
        sizeX *= scale;
        sizeY *= scale;
        lifetimeMs = 750;
        originalColor = new Vector4f(BastionFieldEffects.hexesColour);
        //rotate(this,PI/2); //may be unnecessary
        setArbitraryNormalOverride(Vector3fTools.projectOnPlane(velocity, Controller.getCamera().getForward())); //are these in the right order?
    }

    @Override
    public void update(long l) {
        super.update(l);
        colorOverTime(this,l,originalColor,zero);
        sizeOverTime(this,l,originalScale,2*originalScale);
    }
}
