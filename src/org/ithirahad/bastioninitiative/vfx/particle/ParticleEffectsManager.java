package org.ithirahad.bastioninitiative.vfx.particle;

import api.utils.particle.ModParticleUtil;
import org.ithirahad.bastioninitiative.BastionInitiative;
import javax.imageio.ImageIO;
import javax.vecmath.Vector3f;

public class ParticleEffectsManager {
    public static int TX_STO_LINES;
    public static int TX_BST_HEX;

    public static void init(ModParticleUtil.LoadEvent event, BastionInitiative instance){ //TODO: call method in main
        try { //TODO: use this STO style glowing line for DD beams. maybe even damage beams :D
            TX_STO_LINES = event.addParticleSprite(ImageIO.read(instance.getJarResource("org/ithirahad/bastioninitiative/assets/image/particle/tallglint-1x1-c.png")), instance);
            TX_BST_HEX = event.addParticleSprite(ImageIO.read(instance.getJarResource("org/ithirahad/bastioninitiative/assets/image/particle/hex-1x1-c.png")), instance);
        } catch (Exception ex) {
            System.err.println("[MOD][Resources ReSourced][ERROR] Unable to retrieve particle sprites!");
            ex.printStackTrace();
            System.err.println("[MOD][Resources ReSourced][WARNING] Particles may be invisible or break the game.");
            TX_STO_LINES = 0;
            TX_BST_HEX = 0;
        }
        //load particles to reference ints here
    }

    public static void spawnDDEffect(int sectorId, Vector3f position, Vector3f velocity, int lifetimeMs){

    }
}
