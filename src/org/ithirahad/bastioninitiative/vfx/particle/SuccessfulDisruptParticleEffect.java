package org.ithirahad.bastioninitiative.vfx.particle;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import org.ithirahad.bastioninitiative.network.DisruptionFXRemoteExecutePacket;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.ithirahad.resourcesresourced.network.ScannerFXRemoteExecuteTemporaryPacket;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import java.util.Random;

import static org.ithirahad.bastioninitiative.vfx.particle.ParticleEffectsManager.TX_STO_LINES;

public class SuccessfulDisruptParticleEffect {
    private static final Vector4f haxGreen = new Vector4f(0.4f,1f,0.12f,1f);
    private static final Vector4f lighterGreen = new Vector4f(0.76f,1f,0.35f,0.95f);
    private static final Vector3f CLOUD_LINES_DOWN_VELO = new Vector3f(0.0f,-0.83f,0.0f); //float downwards

    public static void FireLineCloudEffectClient(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, final int diameterMultiplier, final Vector4f color){
        final int repeatModifier = 1 + (diameterMultiplier/8);
        new RealTimeDelayedAction(40, 18, true){
            final Vector3f offset = new Vector3f();
            final Vector3f basePos = new Vector3f(position);
            final Vector3f pos = new Vector3f();
            final Random rng = new Random();

            {
                float adj = diameterMultiplier;
                basePos.y += adj; //particles fly down; as such, we need them to start somewhere above the base
            }

            @Override
            public void doAction() {
                for(int i = 0; i < 5 * repeatModifier; i++) { //groups of 5; may want more tbh but performance is a concern
                    offset.set(nextSignedFloat(), nextSignedFloat(), nextSignedFloat());
                    offset.scale(diameterMultiplier);
                    VerticalSlideLineParticle particle = new VerticalSlideLineParticle(position);
                    particle.velocity.set(velocity); //generally should be straight down
                    ModParticle.setColorF(particle, color);
                    pos.set(basePos);
                    pos.add(offset);
                    ModParticleUtil.playClient(sectorId, pos, TX_STO_LINES, particle); //TODO: NOT EXTRACTOR GAS
                }
            }

            private float nextSignedFloat(){
                return (rng.nextFloat()*2)-1;
            }
        }.startCountdown();
    }

    //Borrows the scanner ring effect from RRS, except in green and without forward motion.
    public static void FireEffectServer(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, int diameterMultiplier){
        ScannerFXRemoteExecuteTemporaryPacket ringEffectPacket = new ScannerFXRemoteExecuteTemporaryPacket(sectorId, sector, position, velocity, diameterMultiplier, haxGreen);
        DisruptionFXRemoteExecutePacket linesEffectPacket = new DisruptionFXRemoteExecutePacket(sectorId, sector, position, CLOUD_LINES_DOWN_VELO, diameterMultiplier, lighterGreen);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4){
                PacketUtil.sendPacket(player, ringEffectPacket);
                PacketUtil.sendPacket(player, linesEffectPacket);
            }
        }
    }
}
