package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.network.packets.PacketUtil;
import org.ithirahad.bastioninitiative.BIConfiguration;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisSubsystem;
import org.ithirahad.bastioninitiative.network.DisruptionHUDUpdatePacket;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.rails.RailController;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import java.io.IOException;
import java.util.*;

import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.aegisInterference;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.bastionFieldStatusEffect;
import static org.ithirahad.bastioninitiative.util.BIUtils.getSelfAndAllDocks;
import static org.ithirahad.bastioninitiative.util.BIUtils.syncToNearbyClientsSafe;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.SHIP;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class BastionField extends AegisSubsystem {
    private static short bastionFieldID = -1;
    private float rawDisruptionDamage; //damage accumulated from incoming beams
    private boolean lastRemoteActiveStatus = false;
    private transient List<SegmentController> tmpToApply = new LinkedList<>();
    private transient Set<SegmentController> applied = new HashSet<>();
    private static HashMap<String,Long> notificationTimeTracker = new HashMap<>();
    private transient long lastInvulnMessageTime = 0;
    private transient long lastDamageUpdate = 0;
    private double lastRemoteEffectiveStructure = 100.0d;
    private boolean needsToUpdateClients = false;
    private transient BastionField referenceBastionField; //bastion field to base all behaviour off of, primarily for planets

    public BastionField(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, elementEntries.get("Aegis Invulnerability").id);
        lastDamageUpdate = now();
        referenceBastionField = this;
    }

    @Override
    protected void onServerBlockLinkedOrLoadedLinked() {

    }

    @Override
    protected void onServerBlockUnlinked() {

    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        return BASTION_FIELD_FUEL_BASE +
                Math.round(structPoints * BIConfiguration.BASTION_FIELD_FUEL_PER_STRUCTURE_POINT) +
                Math.round(getEnhancement() * BASTION_FIELD_FUEL_PER_ENHANCER);
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return BIConfiguration.BASTION_FIELD_FUEL_TO_ONLINE_BASE + Math.round(structPoints * BIConfiguration.BASTION_FIELD_ONLINING_COST_PER_STRUCTURE_POINT);
    }

    @Override
    public double getPowerConsumptionActive() {
        return BASTION_FIELD_ADDITIVE_POWER + (segmentController.getMassWithDocks() * BASTION_FIELD_POWER_PER_MASS);
        //actually using mass here makes sense imho as it require some investment to e.g. protect a planet/plate, though it shouldn't cost that much.
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return super.getPowerConsumerCategory();
    }

    @Override
    public void handleServer(Timer timer, boolean b) {
        boolean currentActiveStatus = referenceBastionField.meetsOperationRequirements();
        if(currentActiveStatus != lastRemoteActiveStatus){
            lastRemoteActiveStatus = currentActiveStatus;
            needsToUpdateClients = true;
        }

        handleCommon(timer,b);
        AegisCore core = referenceBastionField.getParentCore();

        if(core != null && !referenceBastionField.blocks.isEmpty()){
            long now = now();
            long delta = now-referenceBastionField.lastDamageUpdate;
            if(delta >= 1000 && rawDisruptionDamage > 0.0) { //check every second, discretely.
                // This sidesteps any accuracy issues with a real-time DPS limiter and small numbers, while also giving the appearance of a
                // computer's loading or progress bar, which is actually quite thematic for a beam based on "Hollywood hacking".
                double deltaSeconds = delta * 0.001;
                double maxDamage = (DISRUPTOR_MAX_FRACTION_PER_SECOND * core.getMaxHP() * deltaSeconds); //greatest amount of disruption DPS possible
                if (rawDisruptionDamage > maxDamage)
                    rawDisruptionDamage = (float) maxDamage;

                double maxHP = core.getMaxHP();
                double effStructurePoints = getEffectiveStructPointsForDisruptor();
                double hpReductionRatio = maxHP/(effStructurePoints * DISRUPTION_HP_PER_STRUCTURE_POINT);
                double finalDamage = rawDisruptionDamage * hpReductionRatio;
                core.doDisruptionDamage((float) finalDamage); //reduce proportionally to raw HP/EHP ratio

                rawDisruptionDamage = 0;
                lastDamageUpdate = now;
            }
        }

        if(needsToUpdateClients){
            syncToNearbyClientsSafe(this);
            needsToUpdateClients = false;
        }
    }

    @Override
    protected void handleClient(Timer timer, boolean hasBlocks) {
        super.handleClient(timer, hasBlocks);
        handleCommon(timer,hasBlocks);
    }

    private void handleCommon(Timer timer, boolean hasBlocks) {
        EntityType type = segmentController.getType();
        if(type == PLANET_SEGMENT){
            Planet thisPlate = (Planet) segmentController;
            PlanetCore core = thisPlate.getCore();
            if(core != null) {
                if (referenceBastionField.meetsOperationRequirements()) {
                    core.setHitPoints(Float.MAX_VALUE);
                    core.setDestroyed(false);
                } else if (core.getHitPoints() > 10000000) core.setHitPoints(10000000);
            }
        }

        tmpToApply.clear();
        if(segmentController.getUniqueIdentifierFull().equals(referenceBastionField.segmentController.getUniqueIdentifierFull())) {
            tmpToApply.add(segmentController);
            getSelfAndAllDocks(segmentController, tmpToApply, true);
        }

        boolean activated = false;
        if(hasBlocks || referenceBastionField.hasBlocks()) {
            if(segmentController.isFullyLoaded() && referenceBastionField.segmentController.isFullyLoaded()) {
                boolean powered = isPowered();
                activated = powered && referenceBastionField.meetsOperationRequirements();
            } else activated = referenceBastionField.meetsOperationRequirements();
        }
        if(hasBlocks()) applyDamageHandlingChangesToNewEntities(tmpToApply);

        for(SegmentController s : tmpToApply){
            ConfigEntityManager cem = s.getConfigManager();
            if (activated && (!s.getDockingController().isTurretDocking() || !s.isAIControlled())) {
                if (!cem.isActive(bastionFieldStatusEffect))
                    cem.addEffectAndSend(bastionFieldStatusEffect, true, s.getNetworkObject());
            } else if (cem.isActive(bastionFieldStatusEffect))
                cem.removeEffectAndSend(bastionFieldStatusEffect, true, s.getNetworkObject());
        }
        tmpToApply.clear();
        //add/remove effect as necessary
    }

    private void applyDamageHandlingChangesToNewEntities(Collection<SegmentController> allEntities) {
        for(SegmentController s : allEntities)
            if(!applied.contains(s)){
                if(s instanceof ManagedUsableSegmentController<?>){
                    ManagedUsableSegmentController<?> ms = (ManagedUsableSegmentController<?>) s;
                    BastionField sb = ((BastionField)ms.getManagerContainer().getModMCModule(getBlockId()));
                    if(sb != null) sb.referenceBastionField = this;
                }
                applied.add(s);
        }
    }

    public boolean meetsOperationRequirements() {
        if(isOnServer() &&
                segmentController.isFullyLoaded() &&
                segmentController.completedFirstUpdate &&
                !segmentController.isWrittenForUnload()) {
            return (isPresentAndLinked() &&
                    getParentCore() != null &&
                    getParentCore().getRemoteStatus() != VirtualAegisSystem.AegisSystemStatus.VULNERABLE &&
                    isSuppliedAegisCharge() &&
                    !segmentController.getConfigManager().isActive(aegisInterference));
        }
        else return lastRemoteActiveStatus;
    }

    public double getEffectiveStructPointsForDisruptor(){
        double v;
        if(isOnServer()) {
            AegisCore parent = referenceBastionField.getParentCore();
            if (parent != null) if (!blocks.isEmpty()) {
                double fromEnhancement = getEnhancement() * BASTION_FIELD_EFFECTIVE_STRUCTURE_POINTS_PER_ENHANCER;
                v = parent.getStructurePoints(false) + fromEnhancement;
            } else v = parent.getStructurePoints(false);
            else v = 100.0d;

            if(lastRemoteEffectiveStructure != v) needsToUpdateClients = true;
            lastRemoteEffectiveStructure = v;
        }
        return lastRemoteEffectiveStructure;
    }

    @Override
    public void onSerialize(PacketWriteBuffer b) throws IOException {
        b.writeBoolean(lastRemoteActiveStatus);
        b.writeDouble(lastRemoteEffectiveStructure);
        b.writeFloat(rawDisruptionDamage);
    }

    @Override
    public void onDeserialize(PacketReadBuffer b) throws IOException {
        lastRemoteActiveStatus = b.readBoolean();
        lastRemoteEffectiveStructure = b.readDouble();
        rawDisruptionDamage = b.readFloat();

        if(tmpToApply == null) tmpToApply = new ArrayList<>();
        if(applied == null) applied = new HashSet<>();
        if(referenceBastionField == null) referenceBastionField = this;
    }

    @Override
    public String getShortPurposeString() {
        return "Grants invulnerability to the structure, docked ships, and inactive turrets";
    }

    @Override
    public String getFullSystemName() {
        return "Aegis Bastion Field Subsystem";
    }

    @Override
    public List<String> getInfoLines() {
        List<String> result = new LinkedList<>();
        result.add("Effective Structure Points vs. Disruption: " + String.format("%.2f",getEffectiveStructPointsForDisruptor()));
        result.add("(From Structure: " + String.format("%.2f",getParentCore().getStructurePoints(false)) + "; From Enhancers: " + getEnhancement() * BASTION_FIELD_EFFECTIVE_STRUCTURE_POINTS_PER_ENHANCER + ")");
        return result;
    }

    public void sendInvulnerabilityMessageIfReady(PlayerState playerState) {
        if (isOnServer()) {
            long now = now();
            String player = playerState.toString();
            if (!notificationTimeTracker.containsKey(player) || notificationTimeTracker.get(player) > MIN_INVULN_MESSAGE_INTERVAL_MS) {
                playerState.sendServerMessage(Lng.astr(segmentController.getRealName() + " is protected by a Bastion Field! Its blocks are invulnerable to weapon damage while its Aegis System is active."), MESSAGE_TYPE_INFO);
                notificationTimeTracker.put(player,now);
            }
        }
    }

    public void doDisruptionDamage(float power, int factionId) {
        if(lastRemoteActiveStatus) {
            AegisCore core = referenceBastionField.getParentCore();
            if(core != null) core.setLastHitter(factionId);
            referenceBastionField.rawDisruptionDamage += power;
        }
    }

    public void sendDisruptionUIInfo(SegmentController beamShooter) {
        AegisCore parent = referenceBastionField.getParentCore();
        if(parent != null && isOnServer()) {
            Set<PlayerState> players = new HashSet<>(((ManagedUsableSegmentController<?>) beamShooter).getAttachedPlayers());
            players.addAll(((ManagedUsableSegmentController<?>) segmentController).getAttachedPlayers());
            for (PlayerState player : players) {
                PacketUtil.sendPacket(player, new DisruptionHUDUpdatePacket(parent.getCurrentHP(), parent.getMaxHP(), parent.canDisrupt()));
            }
        }
    }

    public static boolean isSegmentControllerInvulnerable(SegmentController sc) {
        if(sc == null) return false;
        RailController rdc = sc.railController;
        if(rdc.getRoot().getType() == SHIP) return false;
        if (sc instanceof ManagedUsableSegmentController<?> && !(rdc.isTurretDocked() && sc.isAIControlled()))
        {
            /*
            List<ConfigGroup> effects = sc.getConfigManager().getPermanentEffects();
            boolean hasBastion = effects.contains(bastionFieldStatusEffect);
            return hasBastion;
             */

            if (bastionFieldID == -1) bastionFieldID = elementEntries.get("Aegis Invulnerability").id; //yeah yeah, I know, side effect

            ManagedUsableSegmentController<?> rootMSC = (ManagedUsableSegmentController<?>) rdc.getRoot();
            BastionField bsf = (BastionField) rootMSC.getManagerContainer().getModMCModule(bastionFieldID);
            return bsf.meetsOperationRequirements(); //na, nanana na na na, ...can't touch this!

        }
        return false;
    }

    public boolean getCachedActiveStatus() {
        return lastRemoteActiveStatus;
    }

    public BastionField getReferenceModule() {
        return referenceBastionField;
    }
}
