package org.ithirahad.bastioninitiative.gui.corepanel;

import api.network.packets.PacketUtil;
import api.utils.gui.GUIInputDialog;
import api.utils.gui.GUIInputDialogPanel;
import org.ithirahad.bastioninitiative.network.AegisUIInputPacket;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class RecalibrationTimeInput extends GUIInputDialog {
    private static RecalibrationTimeDialog dialogPanel;


    long lastTargetBlock = 0;
    String lastTargetEntity = "";
    boolean lastStatus = false;

    @Override
    public GUIInputDialogPanel createPanel() {
        dialogPanel = new RecalibrationTimeDialog(getState(), this);
        return dialogPanel;
    }

    @Override
    public void activate() {
        super.activate();
    } //pls don't call this; it will not be able to send validation-conforming packets without the info in the args

    public void activate(String entityUID, long uid) {
        lastTargetEntity = entityUID;
        lastTargetBlock = uid;
        super.activate();
    }

    public void setCurrentRecalibrationTime(long recalibrationStart) {
        dialogPanel.setCurrentTime(recalibrationStart);
    }

    @Override
    public void callback(GUIElement callingElement, MouseEvent mouseEvent) {
        if(!isOccluded() && mouseEvent.pressedLeftMouse()) {
            //User pointer is typically the displayed label / text on the button
            switch((String) callingElement.getUserPointer()) {
                case "X":
                case "CANCEL":
                    //Deactivate dialog
                    deactivate();
                    break;
                case "OK":
                case "YES":
                    AegisUIInputPacket pk = new AegisUIInputPacket(lastTargetEntity, lastTargetBlock, dialogPanel.getSetTime());
                    PacketUtil.sendPacketToServer(pk);
                    deactivate();
                    break;
            }
        }
    }
}
