package org.ithirahad.bastioninitiative.vfx.particle;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import org.ithirahad.bastioninitiative.network.DecloakLineCloudFXRemoteExecutePacket;
import org.ithirahad.bastioninitiative.network.DisruptionFXRemoteExecutePacket;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Random;

import static org.ithirahad.bastioninitiative.vfx.particle.ParticleEffectsManager.TX_STO_LINES;

public class DecloakParticleEffect {
    private static final Vector4f ringColour = new Vector4f(0.3f,0.3f,0.75f,0.7f);
    private static final Vector4f lighterBlue = new Vector4f(0.5f,0.7f,1f,0.3f);
    private static final Vector3f CLOUD_LINES_DOWN_VELO = new Vector3f(0.0f,-1.6f,0.0f); //float downwards

    public static void FireLineCloudEffectClient(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, final int diameterMultiplier, final Vector4f color){
        new RealTimeDelayedAction(40, 8, true){
            final Vector3f offset = new Vector3f();
            final Vector3f basePos = new Vector3f(position);
            final Vector3f pos = new Vector3f();
            final Random rng = new Random();

            {
                float adj = diameterMultiplier/2f;
                basePos.y += adj; //particles fly down; as such, we need them to start somewhere above the base
            }

            @Override
            public void doAction() {
                for(int i = 0; i < 5; i++) { //groups of 5; may want more tbh but performance is a concern
                    offset.set(nextSignedFloat(), nextSignedFloat(), nextSignedFloat());
                    offset.scale(diameterMultiplier/2f);
                    VerticalSlideLineParticle particle = new VerticalSlideLineParticle(position);
                    particle.lifetimeMs /= 2; //half the default
                    particle.velocity.set(velocity); //generally should be straight down
                    ModParticle.setColorF(particle, color);
                    pos.set(basePos);
                    pos.add(offset);
                    ModParticleUtil.playClient(sectorId, pos, TX_STO_LINES, particle); //TODO: NOT EXTRACTOR GAS
                }
            }

            private float nextSignedFloat(){
                return (rng.nextFloat()*2)-1;
            }
        }.startCountdown();
    }

    //Borrows the scanner ring effect from RRS, except in green and without forward motion.
    public static void FireEffectServer(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, int diameterMultiplier){
        //TODO FlatRingFXRemoteExecutePacket ringEffectPacket = new FlatRingFXRemoteExecutePacket(...);
        Vector3f aggVelocity = new Vector3f(CLOUD_LINES_DOWN_VELO);
        aggVelocity.add(velocity);
        DecloakLineCloudFXRemoteExecutePacket linesEffectPacket = new DecloakLineCloudFXRemoteExecutePacket(sectorId, sector, position, aggVelocity, diameterMultiplier, lighterBlue);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4){
                //PacketUtil.sendPacket(player, ringEffectPacket);
                PacketUtil.sendPacket(player, linesEffectPacket);
            }
        }
    }
}
