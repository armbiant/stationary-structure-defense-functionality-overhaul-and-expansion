package org.ithirahad.bastioninitiative;

import api.config.BlockConfig;
import api.mod.StarMod;
import api.utils.element.CustomModRefinery;
import api.utils.textures.StarLoaderTexture;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static api.config.BlockConfig.*;
import static org.ithirahad.bastioninitiative.BIConfiguration.AEGIS_ANTI_FTL_ALLOWED;
import static org.ithirahad.bastioninitiative.BIConfiguration.BASTION_FIELD_PROTECTS_TURRETS;
import static org.ithirahad.bastioninitiative.BIRecipeManager.CENTRIFUGE_REFINE_TIME;
import static org.ithirahad.bastioninitiative.BIRecipeManager.centrifugeRecipes;
import static org.ithirahad.bastioninitiative.BastionInitiative.*;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.*;
import static org.schema.game.common.data.element.ElementKeyMap.*;

public class BIElementInfoManager {
    public static boolean elementsInitialized = false;

    public static long CASTELLIUM_RAW_TX;
    public static int CASTELLIUM_RAW_ICON;
    //public static BufferedImage CASTELLIUM_RAW_ICON;

    public static long CASTELLIUM_CAPSULE_TX;
    public static BufferedImage CASTELLIUM_CAPSULE_ICON;

    public static long AEGIS_CELL_TX; //texture, probably nothing as this would be a 3D object
    public static BufferedImage AEGIS_CELL_ICON;

    public static short CENTRIFUGE_FRONT;
    public static short CENTRIFUGE_SIDE;

    public static short AEGIS_CONTROLLER_TX;
    public static BufferedImage AEGIS_CONTROLLER_ICON;

    public static short AEGIS_ENHANCER_TX;
    public static BufferedImage AEGIS_ENHANCER_ICON;

    public static short DISRUPTOR_COMP_FRONT;
    public static short DISRUPTOR_COMP_BACK; //just pull some computer back, maybe tractor beam
    public static short DISRUPTOR_COMP_SIDE;
    public static BufferedImage DISRUPTOR_COMP_ICON;

    public static short DISRUPTOR_FRONT;
    public static short DISRUPTOR_SIDE;
    public static short DISRUPTOR_CAPS;

    public static short SUBSYS_INVULN_FRONT;
    public static short SUBSYS_INVULN_SIDE;

    public static short SUBSYS_UNCLOAK_FRONT;
    public static short SUBSYS_UNCLOAK_SIDE;

    public static short SUBSYS_STARCONTROL_FRONT;
    public static short SUBSYS_STARCONTROL_SIDE;

    public static short SUBSYS_DISTRIB_FRONT;
    public static short SUBSYS_DISTRIB_SIDE;

    public static short SUBSYS_ANTIFTL_FRONT;
    public static short SUBSYS_ANTIFTL_SIDE;

    public static int ENERGY_CAN_ICON;

    public static int DEFAULT_PRICE = 6969;

    public static final ShortOpenHashSet aegisSubsystemControllers = new ShortOpenHashSet(10);
    private static final Short2ObjectOpenHashMap<ElementInformation> bsiBlocks = new Short2ObjectOpenHashMap<>();
    private static final HashMap<String,StarLoaderTexture> blockIconMap = new HashMap<>();
    private static LinkedList<ElementInformation> toAddIcon = new LinkedList<>();

    //TODO: add tex IDs
    public static void loadElements(StarMod mod) {
        try{
            ENERGY_CAN_ICON = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/item/Component_T3_Canister.png"))).getTextureId();
            CASTELLIUM_RAW_ICON = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/item/Castellium_raw.png"))).getTextureId();

            CENTRIFUGE_FRONT  = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/QuantumCentrifugeFront.png"))).getTextureId();
            CENTRIFUGE_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/QuantumCentrifugeSide.png"))).getTextureId();

            AEGIS_CONTROLLER_TX = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisCore.png"))).getTextureId();

            AEGIS_ENHANCER_TX = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisChamberOpen.png"))).getTextureId();

            DISRUPTOR_COMP_FRONT = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/DDComputer.png"))).getTextureId();
            DISRUPTOR_COMP_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/DDComputerSide.png"))).getTextureId();

            DISRUPTOR_FRONT = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/DDModuleFront.png"))).getTextureId();
            DISRUPTOR_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/DDModuleSide.png"))).getTextureId();
            DISRUPTOR_CAPS = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/DDModuleBack.png"))).getTextureId();

            SUBSYS_INVULN_FRONT = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisProtectionFront.png"))).getTextureId();
            SUBSYS_INVULN_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisProtectionSide.png"))).getTextureId();

            SUBSYS_UNCLOAK_FRONT = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisDecloakFront.png"))).getTextureId();
            SUBSYS_UNCLOAK_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisDecloakSide.png"))).getTextureId();

            SUBSYS_ANTIFTL_FRONT = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisInterdictFront.png"))).getTextureId();
            SUBSYS_ANTIFTL_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisInterdictSide.png"))).getTextureId();

            SUBSYS_DISTRIB_FRONT = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisDistribFront.png"))).getTextureId();
            SUBSYS_DISTRIB_SIDE = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/block/AegisDistribSide.png"))).getTextureId();
        } catch (Exception ex){
            System.err.println("[MOD][BastionInitiative] Error retrieving image assets!");
            ex.printStackTrace();
        }

        short genericTopPlate = getInfo(POWER_SUPPLY_BEAM_COMPUTER).getTextureIds()[2];
        short genericBottomPlate = getInfo(POWER_SUPPLY_BEAM_COMPUTER).getTextureIds()[3];
        short genericBackPlate = getInfo(SALVAGE_CONTROLLER_ID).getTextureIds()[0]; //should be a green lights-panel?

        ElementInformation energyCan = BlockConfig.newElement(mod, "Component: Field-Stabilized Canister", getInfo(342).getTextureIds());
        setBasicInfo(energyCan, "An advanced type of containment cell which uses spatial distortion fields to ensure that whatever energy or matter is contained within, can only be released in a controlled manner. Used in Aegis Systems and related devices.",
                DEFAULT_PRICE, CAPSULE_MASS, true, false, ENERGY_CAN_ICON);
        energyCan.volume = RARE_COMPONENT_VOLUME;
        energyCan.setTextureId(getInfo(242).getTextureIds()); //green paint; not very useful but you can technically try to 'place' any item so eh
        energyCan.blockStyle = BlockStyle.SPRITE;
        energyCan.type = elementEntries.get("Component: Prismatic Circuit").getType();
        addElementType("Component: Field-Stabilized Canister", energyCan);

        ElementInformation centrifuge = BlockConfig.newRefinery(mod, "Castellium Centrifuge", getInfo(SCANNER_MODULE).getTextureIds(),
                new CustomModRefinery(centrifugeRecipes, "Castellium Centrifuge", "Separating Castellium Condensate...", CENTRIFUGE_REFINE_TIME)
        );
        setBasicInfo(centrifuge, "A magnetic plasma vortex centrifuge that separates trace amounts of Castellium Condensate from other fluid-state resources. Deprived of their castellon content, these resources immediately decay into a shower of radiation which is contained by the reaction vessel.\r\nThe Castellium Centrifuge can process both raw materials and capsules, however raw materials give better Castellium yields, as some Castellon particles are lost in the waste products of the Capsule Refinery process. \r\nWarning: Do not open lid while in operation.",
                DEFAULT_PRICE, getInfo(FACTORY_BASIC_ID).mass, true, true, getInfo(SCANNER_MODULE).getBuildIconNum());
        centrifuge.setTextureId(new short[]{CENTRIFUGE_FRONT,CENTRIFUGE_FRONT,CENTRIFUGE_SIDE,CENTRIFUGE_SIDE,CENTRIFUGE_FRONT,CENTRIFUGE_FRONT});
        centrifuge.volume = getInfo(SCANNER_MODULE).volume;
        centrifuge.type = elementEntries.get("Block Assembler").type;
        centrifuge.individualSides = 3; //?
        centrifuge.setFactoryBakeTime(1f);
        setBlocksConnectable(centrifuge,ElementKeyMap.getInfo(CARGO_SPACE));
        setBlocksConnectable(centrifuge,ElementKeyMap.getInfo(FACTORY_INPUT_ENH_ID));
        addBlockType("Castellium Centrifuge", centrifuge);

        ElementInformation castelliumRaw = BlockConfig.newElement(mod, "Castellium Condensate", RESS_ORE_FERTIKEEN);
        setBasicInfo(castelliumRaw, "Castellium is composed of castellon flux in a condensed, coherent state.\r\nCastellons are rare quantum particles found within exotic substances such as Anbaric Vapor, Parsyne Plasma, and Thermyn Amalgam, and protect their unusual particle composition and structure from collapsing into more familiar forms.\r\nCastellium can be isolated from various exotic fluid resources via the Castellium Centrifuge, and refined into a usable state via the Capsule Refinery.",
                DEFAULT_PRICE, RAW_ORE_MASS, true, false, CASTELLIUM_RAW_ICON);
        castelliumRaw.volume = RAW_FLUID_VOLUME;
        castelliumRaw.type = elementEntries.get("Anbaric Vapor").getType();
        castelliumRaw.blockResourceType = 0;
        addElementType("Castellium Condensate", castelliumRaw);

        ElementInformation castelliumCapsule = BlockConfig.newElement(mod,"Castellium Capsule", (short)178 /*fertikeen capsule*/);
        setCapsuleInfo(castelliumCapsule, "A stabilized, purified sample of castellium contained within a capsule.", (short)178/*fertikeen capsule*/);
        addElementType("Castellium Capsule", castelliumCapsule); //technically a block but we don't need to do block-things with it

        ElementCategory aegisSystemCategory = BlockConfig.newElementCategory(getCategoryHirarchy().find("spacestation"), "Aegis System");

        ElementInformation aegisCell = BlockConfig.newElement(mod, "Aegis Cell", (short)866/* Hardener - unused in RRS */);
        setBasicInfo(aegisCell, "A specialized containment cell containing a highly exotic parsyne-based plasma condensate. Aegis Cells contain such incredible power that the binding energies holding the plasma together weigh more than most other known substances. The energy fields also mean that Aegis Cells must be stored at a fair distance from one another to avoid catastrophic collapse in storage, making their stored volume unusually high. The highly-condensed parsyne itself is also very dense and extremely heavy, resulting in a barely man-portable energy cell with such heft that it seems to defy the laws of physics... \r\n\r\n" +
                "Aegis Cells are used as fuel for the Aegis Core, a device which can grant incredible capabilities to space stations and ground-based installations.\r\n\r\n" +
                "Each Aegis Cell grants 10 Aegis Charge.",
                DEFAULT_PRICE, CAPSULE_MASS * 100, true, false, getInfo(866).buildIconNum);
        aegisCell.volume = CAPSULE_VOLUME * 100; //they'll be made from roughly 100 capsules and supposedly have some odd storage reqs, so...
        //TODO: replace goofy constants here
        aegisCell.placable = false;
        aegisCell.type = aegisSystemCategory;
        addElementType("Aegis Cell", aegisCell);

        short roundthing = getInfo(WARP_GATE_CONTROLLER).getTextureIds()[2];

        ElementInformation aegisCore = BlockConfig.newElement(mod, "Aegis Core", (short) 866);
        setBasicInfo(aegisCore, "A specialized control unit for stations. Linking Aegis Defense Subsystems to this unit enables various powerful defensive and support functions for stations.\r\n"+
                        "Requires Aegis Cells to function. Will collect Aegis Cells from any linked storage.\r\n\r\n" +
                        "Due to the current limitations of Aegis technology, the Aegis Core must recalibrate its subsystems daily.\r\n" +
                        "If the Aegis System includes a Bastion Field subsystem for invulnerability, this process can be disrupted with a Disruption Datalink system, and will bring the Aegis system offline for a certain period after the following day.",
                DEFAULT_PRICE, getInfo(ElementKeyMap.BUILD_BLOCK_ID).getMass(), true, true, getInfo(ElementKeyMap.REACTOR_STABILIZER_STREAM_NODE).buildIconNum);
        //TODO: prettyprint time reading (d/h:m:s) derived from configured disruption time
        aegisCore.setTextureId(new short[]{AEGIS_CONTROLLER_TX,AEGIS_CONTROLLER_TX,AEGIS_CONTROLLER_TX,AEGIS_CONTROLLER_TX,AEGIS_CONTROLLER_TX,AEGIS_CONTROLLER_TX});
        aegisCore.systemBlock = true;
        aegisCore.lightSource = true; //just to force the ManagerContainer to actually acknowledge its existence since it isn't a controller
        //accursed spaghetti garbage
        //pretty lights are also nice though :D
        aegisCore.lightSourceColor.set(0.1f,0.75f,1.0f,0.2f);
        aegisCore.mainCombinationController = true;
        aegisCore.volume = getInfo(BUILD_BLOCK_ID).getVolume();
        aegisCore.type = aegisSystemCategory; //there is a magic type "spacestation" that has to be here
        aegisCore.setCanActivate(true);
        //aegisCore.signal = true;
        aegisCore.controlledBy.add(ACTIVAION_BLOCK_ID);
        /* aegisCore.controlledBy.add(SIGNAL_AND_BLOCK_ID);
        aegisCore.controlledBy.add(SIGNAL_DELAY_BLOCK_ID);
        aegisCore.controlledBy.add(SIGNAL_OR_BLOCK_ID);
        aegisCore.controlledBy.add(LOGIC_FLIP_FLOP);
        aegisCore.controlledBy.add(SIGNAL_NOT_BLOCK_ID);
        aegisCore.controlledBy.add(LOGIC_BUTTON_NORM);
        aegisCore.controlledBy.add(LOGIC_WIRELESS);
        aegisCore.controlledBy.add(993); //small activator
        aegisCore.controlledBy.add(399); //small button
         */
        addBlockType("Aegis Core", aegisCore);

        setBlocksConnectable(aegisCore, getInfo(STASH_ELEMENT));
        setBlocksConnectable(aegisCore, getInfo(SHOP_BLOCK_ID));
        setBlocksConnectable(aegisCore, elementEntries.get("Block Assembler"));

        ElementInformation aegisEnhancer = BlockConfig.newElement(mod, "Aegis Subsystem Enhancer", POWER_CAP_ID);
        setBasicInfo(aegisEnhancer, "A module that can enhance the functionality of Aegis subsystems at the cost of extra power and Aegis Charge. Press C on the subsystem unit and V on the subsystem enhancement module to link them together, or shift-V to link large groups at once.",
                DEFAULT_PRICE, getInfo(FACTORY_INPUT_ENH_ID).getMass(), true, true, getInfo(POWER_CAP_ID).buildIconNum);
        aegisEnhancer.setTextureId(new short[]{AEGIS_ENHANCER_TX,AEGIS_ENHANCER_TX,roundthing,roundthing,AEGIS_ENHANCER_TX,AEGIS_ENHANCER_TX});
        aegisEnhancer.maxHitPointsFull = getInfo(FACTORY_INPUT_ENH_ID).maxHitPointsFull; //TODO this for everything else too
        aegisEnhancer.individualSides = 6;
        aegisEnhancer.systemBlock = true;
        aegisEnhancer.volume = getInfo(FACTORY_INPUT_ENH_ID).volume;
        aegisEnhancer.type = aegisSystemCategory;
        addBlockType("Aegis Enhancer", aegisEnhancer);

        ElementInformation aegisAntiStealth = BlockConfig.newElement(mod,"Aegis Stealth Interruptor Subsystem", (short) 1128);
        setBasicInfo(aegisAntiStealth, "An Aegis Subsystem that emits hyperchromatic energy pulses, overloading ship stealth systems and forcing them into a cooldown cycle.\r\n" +
                        "These pulses are tuned to only affect the stealth systems of enemy and neutral ships, not allies' or your own.\r\n" +
                        "Unlike other Aegis Subsystems, the Stealth Interruptor can continue to function even when the Aegis System is disrupted and in a vulnerable state.\r\n\r\n" +
                        "Link Aegis Subsystem Enhancers to this module to increase the range of the pulses.",
                DEFAULT_PRICE, getInfo(BUILD_BLOCK_ID).mass, true, true, getInfo(DAMAGE_PULSE_COMPUTER).buildIconNum);
        aegisAntiStealth.systemBlock = true;
        aegisAntiStealth.mass = getInfo(BUILD_BLOCK_ID).mass;
        aegisAntiStealth.volume = getInfo(BUILD_BLOCK_ID).volume;
        aegisAntiStealth.type = aegisSystemCategory;
        aegisAntiStealth.setTextureId(new short[]{genericBackPlate,SUBSYS_UNCLOAK_FRONT,genericTopPlate,genericBottomPlate,SUBSYS_UNCLOAK_SIDE,SUBSYS_UNCLOAK_SIDE});
        aegisAntiStealth.individualSides = 6;
        BlockConfig.add(aegisAntiStealth);
        addBlockType("Aegis Anti Stealth", aegisAntiStealth);
        aegisSubsystemControllers.add(aegisAntiStealth.id);

        setBlocksConnectable(aegisCore, aegisAntiStealth);
        registerComputerModulePair(aegisAntiStealth.id, aegisEnhancer.id);

        ElementInformation aegisInvuln = BlockConfig.newElement(mod, "Aegis Bastion Field Subsystem", SHIELD_DRAIN_CONTROLLER_ID);
        setBasicInfo(aegisInvuln, "An Aegis Subsystem that surrounds static structures with an impenetrable energy field, protecting them from block damage.\r\n" +
                        "The exotic physics of the Bastion Field have the curious side-effect of distributing explosion damage across all active shields on the structure. This effect does not apply to cannon or beam hits, which will damage shields normally."
                        +(BASTION_FIELD_PROTECTS_TURRETS? "" : "\r\n(-- WARNING: The field cannot protect active turrets. --)")+"\r\n\r\n" +
                "The Bastion Field can be disrupted by the Aegis Disruption Datalink, causing the Aegis System (including other Subsystems) to cease functioning for a period of time after its next refuel cycle.\r\n"+
                "While in a Disrupted state, the structure will not be modifiable (even if the Aegis System is taken offline).\r\n"+
                "Link Aegis Subsystem Enhancers to this module to increase the system's resistance to disruption.",
                DEFAULT_PRICE, getInfo(ElementKeyMap.BUILD_BLOCK_ID).mass, true, true, getInfo(SHIELD_DRAIN_CONTROLLER_ID).buildIconNum);
        aegisInvuln.systemBlock = true;
        aegisInvuln.mass = getInfo(BUILD_BLOCK_ID).mass;
        aegisInvuln.volume = getInfo(BUILD_BLOCK_ID).volume;
        aegisInvuln.type = aegisSystemCategory;
        aegisInvuln.canActivate = true; //just gives some info
        aegisInvuln.setTextureId(new short[]{genericBackPlate,SUBSYS_INVULN_FRONT,genericTopPlate,genericBottomPlate,SUBSYS_INVULN_SIDE,SUBSYS_INVULN_SIDE});
        aegisInvuln.individualSides = 6;
        BlockConfig.add(aegisInvuln);
        addBlockType("Aegis Invulnerability", aegisInvuln);
        aegisSubsystemControllers.add(aegisInvuln.id);

        setBlocksConnectable(aegisCore,aegisInvuln);
        registerComputerModulePair(aegisInvuln.id, aegisEnhancer.id);

        ElementInformation aegisStarsystemNexus = BlockConfig.newElement(mod, "Aegis Stellar Nexus Subsystem", DECORATIVE_PANEL_3); //TODO: make it the orange deco computer tex
        setBasicInfo(aegisStarsystemNexus, "An Aegis Subsystem that allows a station, asteroid, or planet to act as a central faction control hub for the star system.\r\n" +
                        "The Stellar Nexus takes one cycle to establish a connection with every new Aegis System activated within a star system, but once the connection is established with other Aegis Systems," +
                        "the Nexus's Aegis System is invulnerable to Disruption Datalinks until all other fields are deactivated or destroyed, and is also capable of protecting turrets.\r\n" +
                        "Once the Nexus has made connections to other Aegis Systems, it also gives a constant boost to your faction's claim over the system based on the power and quantity of other structures that are linked.\r\n" +
                        "However, if another Aegis System is destroyed during its vulnerability window, the backlash from the broken link prevents the Nexus from connecting to any new Aegis Systems for the duration two refuel cycles.\r\n\r\n" +
                        "CAUTION: Two Stellar Nexus in the same system belonging to one faction will result in both Nexus experiencing Aegis Interference," +
                        "which renders Bastion Field protection completely ineffective.\r\n" +
                        "Also, be aware that bringing this module online requires a considerable amount of Aegis Charge to be drained from your system.\r\n\r\n"+
                        "Link Aegis Subsystem Enhancers to this module to increase your network's influence on system ownership.",
                DEFAULT_PRICE, getInfo(FACTORY_INPUT_ENH_ID).getMass(), true, true, getInfo(DECORATIVE_PANEL_3).buildIconNum);
        aegisStarsystemNexus.systemBlock = true;
        aegisStarsystemNexus.mass = getInfo(BUILD_BLOCK_ID).mass;
        aegisStarsystemNexus.volume = getInfo(BUILD_BLOCK_ID).volume;
        aegisStarsystemNexus.type = aegisSystemCategory;
        aegisStarsystemNexus.canActivate = true; //just gives some info
        aegisStarsystemNexus.setTextureId(getInfo(DECORATIVE_PANEL_3).getTextureIds());
        aegisStarsystemNexus.individualSides = 6;
        aegisStarsystemNexus.deprecated = true; //TODO Phase 3 / Phase 4
        aegisStarsystemNexus.shoppable = false;

        addBlockType("Aegis Stellar Nexus", aegisStarsystemNexus);
        aegisSubsystemControllers.add(aegisStarsystemNexus.id);

        ElementInformation aegisTransmitter = BlockConfig.newElement(mod,"Aegis Transmitter Subsystem", (short) 1128);
        setBasicInfo(aegisTransmitter, "An Aegis Subsystem that transmits Aegis Charge to nearby stations in the same faction.\r\n" +
                        "The transmitter cannot send Aegis Charge to or from homebases, and can only transmit to systems controlled by the faction.\r\n\r\n" +
                        "Link Aegis Subsystem Enhancers to this module to increase its range (diminishing returns apply).",
                DEFAULT_PRICE, getInfo(FACTORY_INPUT_ENH_ID).getMass(), true, true, getInfo(DAMAGE_PULSE_COMPUTER).buildIconNum);
        aegisTransmitter.systemBlock = true;
        aegisTransmitter.mass = getInfo(BUILD_BLOCK_ID).mass;
        aegisTransmitter.volume = getInfo(BUILD_BLOCK_ID).volume;
        aegisTransmitter.type = aegisSystemCategory;
        aegisTransmitter.canActivate = true; //just gives some info
        aegisTransmitter.setTextureId(new short[]{genericBackPlate, SUBSYS_DISTRIB_FRONT,genericTopPlate,genericBottomPlate, SUBSYS_DISTRIB_SIDE, SUBSYS_DISTRIB_SIDE});
        aegisTransmitter.individualSides = 6;
        //aegisTransmitter.deprecated = true; //TODO
        aegisTransmitter.shoppable = false;
        //TODO: Consider some mechanic for aegis storage cap/aegis capacitors to increase it (would require delays when adding fuel, to avoid inv storage workaround). Faction "freewheeling" with tiny stations and no maintenance reqs due to huge stored aegis pool is not ideal.
        addBlockType("Aegis Transmitter", aegisTransmitter);
        aegisSubsystemControllers.add(aegisTransmitter.id);

        setBlocksConnectable(aegisCore,aegisEnhancer);
        registerComputerModulePair(aegisTransmitter.id, aegisEnhancer.id);

        ElementInformation aegisAntiFTL = BlockConfig.newElement(mod,"Aegis FTL Interception Subsystem", (short) 1128);
        setBasicInfo(aegisAntiFTL, "An Aegis Subsystem that creates a multispatial convergence field, intercepting ships that jump through its radius and redirecting them to the location of the structure where the system is mounted.\r\n" +
                        "System will not function within the burn radius of a star due to the intense gravitic and electromagnetic interference. \r\n"+
                        "Link Aegis Subsystem Enhancers to this module to increase its range.",
                DEFAULT_PRICE, getInfo(BUILD_BLOCK_ID).mass, true, true, getInfo(DAMAGE_PULSE_COMPUTER).buildIconNum);

        if(warpSpaceIsPresentAndEnabled) aegisAntiFTL.description = "An Aegis Subsystem that creates a multispatial convergence field, intercepting warp travel past the nearest jump point and forcing ships from warp back to realspace.\r\n" +
                "Intercepted ships will come out of warp at the station's location. Does not affect ships from your faction or allied factions.\r\n\r\n" +
                "Linking enhancers to this module is possible, but does not have any beneficial effect at the moment.";
        aegisAntiFTL.systemBlock = true;
        aegisAntiFTL.mass = getInfo(BUILD_BLOCK_ID).mass;
        aegisAntiFTL.volume = getInfo(BUILD_BLOCK_ID).volume;
        aegisAntiFTL.type = aegisSystemCategory;
        aegisAntiFTL.setTextureId(new short[]{genericBackPlate,SUBSYS_ANTIFTL_FRONT,genericTopPlate,genericBottomPlate,SUBSYS_ANTIFTL_SIDE,SUBSYS_ANTIFTL_SIDE});
        aegisAntiFTL.individualSides = 6;
        aegisAntiFTL.setDeprecated(!AEGIS_ANTI_FTL_ALLOWED);
        addBlockType("Aegis Anti FTL", aegisAntiFTL);
        aegisSubsystemControllers.add(aegisAntiFTL.id);

        setBlocksConnectable(aegisCore, aegisAntiFTL);
        registerComputerModulePair(aegisAntiFTL.id, aegisEnhancer.id);

        //TODO: Automatic Distress Beacon - calls any patrolling fleets or sth when an enemy or disruptor is present
        // also Automatic Disruption Distress Beacon - calls any patrolling fleets to guard sector for the duration of the disruption window
        /*TODO
             **RANDOM AEGIS SYSTEM IDEAS**
            [@Ithirahad] Aegis Delocalized Banking Subsystem:
            (+Banking Terminal)
                --Holds central reserve funds e.g. from shop taxes, only founders can access (or map to permission edit permission or something)
                --Plebs can have their own personal accounts, can be outside of faction but nonetheless accounts are stored per-faction
                --(So you can theoretically have a whole banker faction!)
                --(On that note: Central reserve lending, held-cash lending % settings + interest, and debt system? idk loan sharks and Switzerland in SM would be funny, and bank runs would be too :D)
                --Does not benefit from or link to Aegis Enhancers, but has a flat and nontrivial power and cell requirement to function
                --Need at least one bank in a system to be able to collect taxes
                --Unavailable if disrupted?
            [@Ithirahad] Aegis Intelligence Scanner Subsystem:
                --Tracks nearby fleets and individual ships on map
                --Sends to personal owner, faction mates, and allies by default
                --Range increases with enhancers
                --Goes offline if disrupted?
            [@Ithirahad] <Any type of defensive aura chamber (conflicts with Derp's mod though)>

         */


        ElementInformation ddComputer = BlockConfig.newElement(mod, "Disruption Datalink Computer", POWER_SUPPLY_BEAM_COMPUTER);
        setBasicInfo(ddComputer, "The Disruption Datalink system exploits a vulnerability in the Bastion Field regulator system to inject malicious code into the Aegis Core via specially modulated field fluctuations, causing the entire Aegis System to deactivate for a certain period after its next daily refueling cycle.\r\n" +
                "Disruption is not effective on Aegis Systems that do not include a Bastion Field, and disrupting more massive targets requires more time or a larger Disruption Datalink system. Link more Disruption Datalink Modules to this computer to increase the disruption power of the system.\r\n" +
                "The Disruption Datalink System consumes Aegis Cells to operate. Larger systems require more Aegis Cells.\r\n\r\n"+
                "Note from Schemadyne Industries: This unit is *ABSOLUTELY NOT* based on an off-brand toaster, we promise. :)",
                DEFAULT_PRICE, getInfo(WEAPON_CONTROLLER_ID).getMass(), true, true, getInfo(POWER_SUPPLY_BEAM_COMPUTER).buildIconNum);
        //TODO: replace "a certain period" with the actual configured value
        ddComputer.systemBlock = true;
        ddComputer.volume = getInfo(WEAPON_CONTROLLER_ID).volume;
        ddComputer.type = getInfo(POWER_DRAIN_BEAM_COMPUTER).type;
        ddComputer.setTextureId(new short[]{genericBackPlate,DISRUPTOR_COMP_FRONT,genericTopPlate,genericBottomPlate,DISRUPTOR_COMP_SIDE,DISRUPTOR_COMP_SIDE});
        ddComputer.individualSides = 6;
        ddComputer.canActivate = true;
        addBlockType("Hacking Computer", ddComputer);
        //Detail: Any Aegis System has a maximum disruption rate. Adding more modules doesn't let you exceed this, it just lets you reach it.
        //TODO: Maximum speed allowed to use DD system?
        // Maybe some sort of diminishing effectiveness based on how fast you're kiting?
        setBlocksConnectable(ddComputer,ElementKeyMap.getInfo(STASH_ELEMENT));

        ElementInformation ddModule = BlockConfig.newElement(mod, "Disruption Datalink Module", POWER_SUPPLY_BEAM_MODULE);
        setBasicInfo(ddModule, "The Disruption Datalink system exploits a vulnerability in the Bastion Field regulator system to inject malicious code into the Aegis Core via specially modulated energy field fluctuations, causing the entire Aegis System to deactivate for a certain period after its next daily refueling cycle.\r\n" +
                        "Disruption is not effective on Aegis Systems that do not include a Bastion Field, and disrupting more massive targets requires more time or a larger Disruption Datalink system. Link these modules to a Disruption Datalink Computer to create a disruption system.\r\n"+
                        "Disruption Datalink Systems consume Aegis Cells to operate. Larger systems require more Aegis Cells.",
                DEFAULT_PRICE, getInfo(WEAPON_CONTROLLER_ID).getMass(), true, true, getInfo(POWER_SUPPLY_BEAM_MODULE).buildIconNum);
        ddModule.systemBlock = true;
        ddModule.volume = getInfo(WEAPON_CONTROLLER_ID).volume;
        ddModule.type = getInfo(POWER_DRAIN_BEAM_MODULE).type;
        ddModule.setTextureId(new short[]{DISRUPTOR_FRONT,DISRUPTOR_SIDE,DISRUPTOR_CAPS, DISRUPTOR_CAPS,DISRUPTOR_SIDE,DISRUPTOR_SIDE});
        ddModule.individualSides = 6;
        addBlockType("Hacking Module", ddModule);

        registerComputerModulePair(ddComputer.id,ddModule.id);

        ElementInformation bastionShieldChamber = BlockConfig.newChamber(mod, "Fortress Shield Amplification", (short)1050  /*shield base chamber*/);
        bastionShieldChamber.setDescription("Fortress Shield Amplification greatly enhances the shield capacity of stationary constructs.");
        bastionShieldChamber.chamberPermission = 6; //same as warpgate distance, hopefully this works
        bastionShieldChamber.chamberCapacity = 0.04f; //basic station amenity for a shielded station honestly... doesn't need to be expensive
        bastionShieldChamber.chamberRoot = (short)1046;
        bastionShieldChamber.chamberParent = (short)1050;
        bastionShieldChamber.chamberPrerequisites.clear();
        bastionShieldChamber.chamberPrerequisites.add((short)1050);
        bastionShieldChamber.chamberConfigGroupsLowerCase.add("bastion_shield_amp");
        bastionShieldChamber.setTextureId(getInfo((short)1050).getTextureIds());
        BlockConfig.add(bastionShieldChamber); //direct add since non craftable

        ElementInformation bastionWeaponChamber = BlockConfig.newChamber(mod, "Fortress Weapons Amplification", (short)1046/*base defense chamber*/);
        bastionWeaponChamber.setDescription("Fortress Weapons Amplification greatly increases the range of stationary constructs' turrets.");
        bastionWeaponChamber.chamberPermission = 6;
        bastionWeaponChamber.chamberCapacity = 0.06f;
        bastionWeaponChamber.chamberRoot = (short)1046;
        bastionWeaponChamber.chamberConfigGroupsLowerCase.add("bastion_weapon_amp");
        bastionWeaponChamber.setTextureId(getInfo((short)1050).getTextureIds());
        BlockConfig.add(bastionWeaponChamber);

        if(DRAW_ICONS) {
            try {
                for (ElementInformation type : toAddIcon) {
                    type.setBuildIconNum(StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/bastioninitiative/assets/image/item/Icon_Placeholder.png"))).getTextureId());
                    blockIconMap.put(type.getName().toLowerCase().replaceAll(" ", "-") + "-icon", StarLoaderTexture.textures.get(type.buildIconNum));
                }
            } catch (Exception e) {
                System.err.println("[MOD][BastionInitiative] Error retrieving placeholder icon!");
                e.printStackTrace();
            }
        }

        elementsInitialized = true;
    }

    private static void addElementType(String elementEntryName, ElementInformation type) {
        BlockConfig.add(type);
        //icon is added manually, so do not add to iconmap
        elementEntries.put(elementEntryName,type);
    }

    private static void addBlockType(String elementEntryName, ElementInformation type) {
        if(!DRAW_ICONS){
            String path = "org/ithirahad/bastioninitiative/assets/image/item/" + type.getName().toLowerCase().replaceAll(" ", "-") + "-icon.png";
            try{
                type.setBuildIconNum(StarLoaderTexture.newIconTexture(ImageIO.read(modInstance.getJarResource(path))).getTextureId());
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
        addElementType(elementEntryName,type);
        if(DRAW_ICONS) {
            toAddIcon.add(type);
        }
        bsiBlocks.put(type.id,type);
    }

    public static void doPostTasks() {
        //elementEntries.get("Castellium Centrifuge").type = getInfo(FACTORY_BASIC_ID).type; //dodge RRS deprecation routine
        //elementEntries.get("Castellium Condensate").type = elementEntries.get("Anbaric Vapor").getType(); //raw resources
        //elementEntries.get("Castellium Capsule").type = elementEntries.get("Anbaric Capsule").getType();
        //elementEntries.get("Component: Field-Stabilized Canister").type = elementEntries.get("Component: Prismatic Circuit").getType();
    }

    public static ArrayList<ElementInformation> getAllBlocks() {
        return new ArrayList<>(bsiBlocks.values());
    }

    public static StarLoaderTexture getTexture(String textureName) {
        return blockIconMap.get(textureName);
    }
}
