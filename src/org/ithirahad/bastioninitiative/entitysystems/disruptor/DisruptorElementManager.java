package org.ithirahad.bastioninitiative.entitysystems.disruptor;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.combination.BeamCombiSettings;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import java.util.HashSet;
import java.util.Set;

import static com.google.common.primitives.Ints.max;
import static org.ithirahad.bastioninitiative.BIConfiguration.DISRUPTOR_FUEL_PER_CYCLE_PER_BLOCK;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.game.common.data.element.ElementKeyMap.STASH_ELEMENT;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class DisruptorElementManager extends BeamElementManager<DisruptorUnit,DisruptorCollectionManager,DisruptorElementManager> implements TagModuleUsableInterface {
    public static final String TAG_ID = "ADM";

    public static float TICK_RATE = 0.2f;

    public static float COOL_DOWN = 10.0f;
    public static float BURST_TIME = 15.05f;
    public static float INITIAL_TICKS = 1f;

    public static double RAIL_HIT_MULTIPLIER_PARENT = 0f;
    public static double RAIL_HIT_MULTIPLIER_CHILD = 0f; //can't hack yourself


    @Override
    public double getRailHitMultiplierParent(){
        return RAIL_HIT_MULTIPLIER_PARENT;
    }
    @Override
    public double getRailHitMultiplierChild(){
        return RAIL_HIT_MULTIPLIER_CHILD;
    }


    public DisruptorElementManager(SegmentController segmentController) {
        super(elementEntries.get("Hacking Computer").id, elementEntries.get("Hacking Module").id, segmentController);
    }

    @Override
    protected String getTag() {
        return "mainreactor";
    } //empty, ergo no errors possible :DDDDD

    @Override
    public void updateActivationTypes(ShortOpenHashSet typesThatNeedActivation) {
        typesThatNeedActivation.add(elementEntries.get("Hacking Module").id);
    }

    @Override
    public DisruptorCollectionManager getNewCollectionManager(
            SegmentPiece block, Class<DisruptorCollectionManager> clazz) {
        return new DisruptorCollectionManager(block, getSegmentController(), this);
    }

    @Override
    public void doShot(DisruptorUnit firingUnit, DisruptorCollectionManager collectionManager, ShootContainer shootContainer, PlayerState playerState, float beamTimeout, Timer timer, boolean focusBeamOnClient) {
        if(FactionManager.isNPCFactionOrPirateOrTrader(getSegmentController().getFactionId()) && getSegmentController().isAIControlled()) {
            super.doShot(firingUnit, collectionManager, shootContainer, playerState, beamTimeout, timer, focusBeamOnClient);
            return; //if it's just an AI ship, ignore all checks
        }

        if(firingUnit.canUse(timer.currentTime, false)) {
            Set<PlayerState> players = new HashSet<>(getAttachedPlayers());
            if(playerState != null) players.add(playerState); //idk if in-core firer is considered an attached player
            Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> linked = BIUtils.findLinkedBlocks(collectionManager.getSegmentController(),collectionManager.getControllerIndex());
            if (linked.containsKey(STASH_ELEMENT)) {
                Inventory inv = BIUtils.getInventoryAt(collectionManager.getSegmentController(), linked.get(STASH_ELEMENT).iterator().next());
                //should only be one, TODO ensure this for all these blocks (make a list in elementinfomanager) in block linking event

                if (inv != null) {
                    short fuel = elementEntries.get("Aegis Cell").id;
                    int fuelConsNeeded = max(1,(int)(DISRUPTOR_FUEL_PER_CYCLE_PER_BLOCK/(BURST_TIME/TICK_RATE)) * firingUnit.size());
                    int fuelExtant = inv.getOverallQuantity(fuel);
                    if (fuelExtant >= fuelConsNeeded) {
                        int invmod = inv.incExistingOrNextFreeSlotWithoutException(fuel, -fuelConsNeeded);
                        inv.sendInventoryModification(invmod);
                        super.doShot(firingUnit, collectionManager, shootContainer, playerState, beamTimeout, timer, focusBeamOnClient);
                    } else if (isOnServer()) for (PlayerState player : players)
                        player.sendServerMessage(Lng.astr("Cannot Fire: Insufficient Aegis Cells!\r\n(" + fuelExtant + '/' + fuelConsNeeded + ") required by disruption unit of size " + firingUnit.size() + " to initiate firing cycle."), MESSAGE_TYPE_INFO);
                } else if (isOnServer()) for (PlayerState player : players)
                    player.sendServerMessage(Lng.astr("The Disruption Datalink cannot function without a linked storage!"), MESSAGE_TYPE_INFO);
            } else if (isOnServer()) for (PlayerState player : players)
                player.sendServerMessage(Lng.astr("The Disruption Datalink cannot function without a linked storage!"), MESSAGE_TYPE_INFO);
        }
    }

    @Override
    public String getManagerName() {
        return Lng.str("Disruption Datalink System Collective");
    }

    @Override
    protected void playSound(DisruptorUnit fireingUnit, Transform where) {

    }

    /* (non-Javadoc)
     * @see org.schema.game.common.controller.elements.UsableControllableBeamElementManager#getGUIUnitValues(org.schema.game.common.data.element.BeamUnit, org.schema.game.common.controller.elements.ControlBlockElementCollectionManager, org.schema.game.common.controller.elements.ControlBlockElementCollectionManager, org.schema.game.common.controller.elements.ControlBlockElementCollectionManager)
     */
    @Override
    public ControllerManagerGUI getGUIUnitValues(DisruptorUnit firingUnit,
                                                 DisruptorCollectionManager col,
                                                 ControlBlockElementCollectionManager<?, ?, ?> supportCol,
                                                 ControlBlockElementCollectionManager<?, ?, ?> effectCol) {

        float damage = firingUnit.getBeamPower();
        float tickRate = firingUnit.getTickRate();
        float distance = firingUnit.getDistance();

        return ControllerManagerGUI.create((GameClientState) getState(), Lng.str("Beam Unit"), firingUnit,
                new ModuleValueEntry(Lng.str("Disruption Power"), StringTools.formatPointZero(damage)),
                new ModuleValueEntry(Lng.str("TickRate"), StringTools.formatPointZeroZero(tickRate)),
                new ModuleValueEntry(Lng.str("Range"), StringTools.formatPointZero(distance)),
                new ModuleValueEntry(Lng.str("PowerConsumptionResting"), firingUnit.getPowerConsumedPerSecondResting()),
                new ModuleValueEntry(Lng.str("PowerConsumptionCharging"), firingUnit.getPowerConsumedPerSecondCharging()),
                new ModuleValueEntry(Lng.str("AegisCellsConsumptionPerCycle"), firingUnit.size() * DISRUPTOR_FUEL_PER_CYCLE_PER_BLOCK)
        );
    }

    @Override
    public float getTickRate() {
        return TICK_RATE;
    }

    @Override
    public float getCoolDown() {
        return COOL_DOWN;
    }

    @Override
    public float getBurstTime() {
        return BURST_TIME;
    }

    @Override
    public HackingBeamMetaDataDummy getDummyInstance() {
        return new HackingBeamMetaDataDummy(); //dumb dummy is dumb. maybe we will need to give it fields someday...
    }

    @Override
    public String getTagId() {
        return TAG_ID;
    }
    @Override
    public float getInitialTicks() {
        return INITIAL_TICKS;
    }
    public double getMiningScore() {
        return totalSize;
    }

    @Override
    public CombinationAddOn<DisruptorUnit, DisruptorCollectionManager, ? extends DisruptorElementManager, BeamCombiSettings> getAddOn() {
        return null; //tractor beam just returns null, so we will too :D
    }
}
