package org.ithirahad.bastioninitiative.util;

import org.apache.poi.util.NotImplemented;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import static java.util.Calendar.HOUR_OF_DAY;

public class TemporalShortcuts {
    public static long currentTimeOfDay(){
        return now() - getLastMidnight();
    }
    public static long now() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static long getLastMidnight(){
        Calendar result = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.ENGLISH);
        result.set(Calendar.MILLISECOND,0);
        result.set(Calendar.SECOND,0);
        result.set(Calendar.MINUTE,0);
        result.set(Calendar.HOUR_OF_DAY,0);
        return result.getTimeInMillis();
    }

    public static Calendar msToTimeToday(long timeOfDayInMS) {
        Calendar resultCal = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.ENGLISH);
        resultCal.setTimeInMillis(getLastMidnight());
        resultCal.add(Calendar.MILLISECOND,(int)timeOfDayInMS);
        return resultCal;
    }

    public static String getFormattedGMTTimeAndDate(long t) {
        Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.ENGLISH);
        c.setTimeInMillis(t);
        return getFormattedCalendarTimeAndDate(c);
    }

    public static String getFormattedCalendarTimeAndDate(Calendar t) {
        return String.format("%02d",t.get(HOUR_OF_DAY)) + ":" + String.format("%02d",t.get(Calendar.MINUTE)) + " Galactic Mean Time, on " + t.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + ' ' + t.get(Calendar.DATE);
    }

    public static String getFormattedGMTTime(long v) {
        Calendar t = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.ENGLISH);
        t.setTimeInMillis(v);
        return String.format("%02d",t.get(HOUR_OF_DAY)) + ":" + String.format("%02d",t.get(Calendar.MINUTE)) + " Galactic Mean Time";
    }

    @NotImplemented
    public static String getFormattedLocalTime(long v, boolean includeDate) {
        String result;
        Calendar t = new GregorianCalendar(TimeZone.getDefault(), Locale.ENGLISH);
        t.setTimeInMillis(v);
        result = String.format("%02d",t.get(HOUR_OF_DAY)) + ":" + String.format("%02d",t.get(Calendar.MINUTE)) + " Galactic Mean Time";
        if(includeDate) result += t.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + ' ' + t.get(Calendar.DATE);
        return result;
    }
}
