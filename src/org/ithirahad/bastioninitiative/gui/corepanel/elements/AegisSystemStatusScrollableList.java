package org.ithirahad.bastioninitiative.gui.corepanel.elements;

import org.ithirahad.bastioninitiative.gui.corepanel.elements.AegisSystemStatusScrollableList.AegisStatusEntry;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

import javax.vecmath.Vector4f;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;

import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.*;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_DAYS;

/**
 * [Description]
 *
 * @author TheDerpGamer (MrGoose#0027)
 */
public class AegisSystemStatusScrollableList extends ScrollableTableList<AegisStatusEntry> {

	private final VirtualAegisSystem.AegisSystemStats systemStats;
	private final VirtualAegisSystem.AegisSystemStatus status;

	public AegisSystemStatusScrollableList(InputState inputState, GUIElement guiElement, VirtualAegisSystem.AegisSystemStats systemStats, VirtualAegisSystem.AegisSystemStatus status) {
		super(inputState, guiElement.getWidth(), guiElement.getHeight(), guiElement);
		this.systemStats = systemStats;
		this.status = status;
	}

	@Override
	protected ArrayList<AegisStatusEntry> getElementList() {
		ArrayList<AegisStatusEntry> elementList = new ArrayList<>();

		double aegisChargePerMS = systemStats.getAegisSystemDailyCost();
		aegisChargePerMS /= MS_TO_DAYS;

		boolean needsToConsumeActivationCharge = systemStats.isAwaitingActivationCharge() || !systemStats.isActivated();

		double storedChargeForTime = systemStats.getAegisChargeStored();
		if(needsToConsumeActivationCharge) storedChargeForTime -= systemStats.getAegisSystemActivationCost();

		String chargeDurationString;
		if(needsToConsumeActivationCharge && storedChargeForTime <= 0) {
		chargeDurationString = " (Insufficient Charge to Activate)";
		} else {
			chargeDurationString = " (Will last " + StringTools.formatTimeFromMS((long) (storedChargeForTime / aegisChargePerMS));
			if (needsToConsumeActivationCharge) chargeDurationString += " after activation cost";
			chargeDurationString += ")";
		}

		Vector4f white = new Vector4f(1,1,1,1);
		Vector4f red = new Vector4f(1,0.1f,0.1f,1);
		Vector4f yellow = new Vector4f(1,1,0,1);
		Vector4f statusColor = new Vector4f();

		if(status == ACTIVE) statusColor.set(0.1f,1,0.4f,1);
		else if(status == VULNERABLE || status == NO_CHARGE || status == ERROR_STALE) statusColor.set(red);
		else if(status == ACTIVE_HACKED) statusColor.set(1,0.5f,0,1);
		else if(status == ONLINING) statusColor.set(yellow);
		else if(status == DISABLED) statusColor.set(0.6f,0.6f,0.63f,1);

		elementList.add(new AegisStatusEntry("AEGIS CHARGE COST TO BRING ONLINE: ", String.valueOf(StringTools.formatPointZeroZeroZero(systemStats.getAegisSystemActivationCost())), ((systemStats.getAegisSystemActivationCost() <= systemStats.getAegisChargeStored())? white : red), 1));
		elementList.add(new AegisStatusEntry("AEGIS CHARGE CONSUMED PER DAY: ", String.valueOf(StringTools.formatPointZero(systemStats.getAegisSystemDailyCost())), ((systemStats.getAegisSystemDailyCost() <= systemStats.getAegisChargeStored())? white : yellow), 2));
		elementList.add(new AegisStatusEntry("STORED AEGIS CHARGE: ", StringTools.formatPointZeroZero(systemStats.getAegisChargeStored()), white, 3));
		elementList.add(new AegisStatusEntry("", chargeDurationString, white, 4));
		elementList.add(new AegisStatusEntry("STATUS: ", status.getNiceName().toUpperCase(), statusColor, 5));
		return elementList;
	}

	@Override
	public void initColumns() {
		Column parameterNameColumn = addColumn("", 12.0f, new Comparator<AegisStatusEntry>() {
			@Override
			public int compare(AegisStatusEntry o1, AegisStatusEntry o2) {
				return Integer.compare(o1.index,o2.index);
			}
		});
		addColumn("", 9f, new Comparator<AegisStatusEntry>() {
			@Override
			public int compare(AegisStatusEntry o1, AegisStatusEntry o2) {
				return Integer.compare(o1.index,o2.index);
			}
		});

		activeSortColumnIndex = 0;
		//continousSortColumn = 0;
		defaultSortByColumn = parameterNameColumn; //otherwise order is random lol
	}


	@Override
	public void updateListEntries(GUIElementList guiElementList, Set<AegisStatusEntry> set) {
		guiElementList.deleteObservers();
		guiElementList.addObserver(this);
		int index = 0;
		for(AegisStatusEntry datum : set) {
			if(index < set.size()) {
				GUITextOverlayTable keyTable = new GUITextOverlayTable(10, 10, getState());
				keyTable.setTextSimple(datum.label);
				GUIClippedRow keyRow = new GUIClippedRow(getState());
				keyRow.attach(keyTable);

				GUITextOverlayTable valueTable = new GUITextOverlayTable(10, 10, getState());
				valueTable.setTextSimple(datum.value);
				valueTable.setColor(datum.valueColour);
				GUIClippedRow valueRow = new GUIClippedRow(getState());
				valueRow.attach(valueTable);

				AegisSystemStatusScrollableListRow row = new AegisSystemStatusScrollableListRow(getState(), datum, keyRow, valueRow);
				row.onInit();
				guiElementList.add(row);
			}
			index ++;
		}
		guiElementList.updateDim();
	}

	public class AegisSystemStatusScrollableListRow extends ScrollableTableList<AegisStatusEntry>.Row {
		public AegisSystemStatusScrollableListRow(InputState inputState, AegisStatusEntry pair, GUIElement... guiElements) {
			super(inputState, pair, guiElements);
		}
	}
	
	public static class AegisStatusEntry {
		String label;
		String value;
		Vector4f valueColour;
		int index;
	
		public AegisStatusEntry(String label, String value, Vector4f valueColour, int index){
			this.label = label;
			this.value = value;
			this.valueColour = valueColour;
			this.index = index;
		}
	}
}
