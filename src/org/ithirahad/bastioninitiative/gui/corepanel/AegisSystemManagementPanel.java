package org.ithirahad.bastioninitiative.gui.corepanel;

import api.network.packets.PacketUtil;
import api.utils.gui.GUIMenuPanel;
import org.ithirahad.bastioninitiative.gui.corepanel.elements.AegisSystemStatusScrollableList;
import org.ithirahad.bastioninitiative.network.AegisUIInputPacket;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.util.TemporalShortcuts;
import org.newdawn.slick.Color;
import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

import javax.vecmath.Vector3f;
import java.util.Calendar;

import static org.ithirahad.bastioninitiative.gui.corepanel.BIGUIInstanceContainer.recalibrationTimeDialog;
import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.*;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_DAYS;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea.HButtonType;

public class AegisSystemManagementPanel extends GUIMenuPanel {

	private int windowWidth;
	private int windowHeight;

	private VirtualAegisSystem.AegisSystemStats systemStats;
	private VirtualAegisSystem.AegisSystemStatus status;

	private GUITextOverlay statusTextOverlay;
	private GUIContentPane contentPane;

	private GUITextOverlay tempScheduleTextOverlay;

	private static final Vector3f padding = new Vector3f(5,5,0);
	private float prevHeight = -1;

	public AegisSystemManagementPanel(InputState inputState) {
		super(inputState, "AegisSystemManagementPanel", 1100, 600);
	}

	@Override
	public void recreateTabs() {
		guiWindow.activeInterface = this;
		createAegisTab();
	}

	@Override
	public void onInit() {
		super.onInit();
		guiWindow.orientate(ORIENTATION_HORIZONTAL_MIDDLE | ORIENTATION_VERTICAL_MIDDLE);
	}

	/**
	 * Updates the aegis system stats and status. Use before drawing the UI or when any changes are made to the system.
	 *
	 * @param systemStats The system stats
	 * @param status The system status
	 */
	public void updateInfo(VirtualAegisSystem.AegisSystemStats systemStats, VirtualAegisSystem.AegisSystemStatus status) {
		this.systemStats = systemStats;
		this.status = status;
	}

	private GUIContentPane createAegisTab() {
		if(systemStats == null) contentPane = guiWindow.addTab("[ERROR] UNABLE TO BUILD WINDOW: SYSTEM STATS NOT AVAILABLE");
		else {
			if(!guiWindow.getTabs().isEmpty()) guiWindow.clearTabs();
			contentPane = guiWindow.addTab("AEGIS SYSTEM");
			windowWidth = guiWindow.getInnerWidthTab();
			windowHeight = guiWindow.getInnerHeigth();

			int leftWidth = (int) (windowWidth / 1.5f);
			int rightWidth = windowWidth - leftWidth;
			int bottomHeight = (int) (windowHeight / 2.0f);
			int topHeight = windowHeight - bottomHeight;
			int labelHeight = FontLibrary.FontSize.BIG.getRowHeight() + 2;

			contentPane.addDivider(rightWidth); //Divider between subsystems and core info/control stack
			contentPane.setTextBoxHeightLast(windowHeight);

			{ //Subsystems
				contentPane.setTextBoxHeight(0, 0, windowHeight);

				GUITextOverlay labelOverlay = new GUITextOverlay(leftWidth, labelHeight, getState());
				labelOverlay.setFont(FontLibrary.FontSize.BIG.getFont());
				labelOverlay.setColor(Color.cyan);
				labelOverlay.setTextSimple("SUBSYSTEMS");
				labelOverlay.onInit();
				labelOverlay.getPos().add(padding);
				contentPane.getContent(0, 0).attach(labelOverlay);

				String subsystemInfo = systemStats.getSubsystemInfoText();
				int textPanelHeight = FontLibrary.getBlenderProBook16().getHeight(subsystemInfo);
				GUIScrollableTextPanel subsystemsTextPanel = new GUIScrollableTextPanel(leftWidth, textPanelHeight, contentPane.getContent(0, 0), getState());
				subsystemsTextPanel.setLeftRightClipOnly = false;

				GUITextOverlay subsystemsTextOverlay = new GUITextOverlay(leftWidth, textPanelHeight, getState());
				subsystemsTextOverlay.setFont(FontLibrary.getBlenderProBook16());
				subsystemsTextOverlay.autoWrapOn = subsystemsTextPanel;
				subsystemsTextOverlay.setTextSimple(subsystemInfo);
				subsystemsTextOverlay.onInit();

				subsystemsTextPanel.setContent(subsystemsTextOverlay);
				subsystemsTextPanel.onInit();
				subsystemsTextPanel.getPos().add(padding);
				subsystemsTextPanel.setWidth(subsystemsTextPanel.getWidth() - padding.x);
				subsystemsTextPanel.setHeight(subsystemsTextPanel.getHeight() - padding.y);
				subsystemsTextPanel.getPos().y += labelHeight; //Move text panel down a bit to leave room for the label.
				subsystemsTextPanel.setHeight(subsystemsTextPanel.getHeight() - labelHeight); //Re-adjust height.
				subsystemsTextOverlay.autoHeight = true;
				contentPane.getContent(0, 0).attach(subsystemsTextPanel);
			}

			{ //Schedule
				GUITextOverlay labelOverlay = new GUITextOverlay(rightWidth, labelHeight, getState());
				labelOverlay.setFont(FontLibrary.FontSize.BIG.getFont());
				labelOverlay.setColor(Color.cyan);
				labelOverlay.setTextSimple("SCHEDULE");
				labelOverlay.onInit();
				contentPane.getContent(1, 0).attach(labelOverlay);

				tempScheduleTextOverlay = new GUITextOverlay(rightWidth, 75, getState()); //TODO remove
				tempScheduleTextOverlay.setFont(FontLibrary.getBlenderProBook14());
				tempScheduleTextOverlay.setTextSimple(getTempScheduleString()); //TODO: figure out what is wrong with time changer cooldown
				tempScheduleTextOverlay.autoHeight = true;
				tempScheduleTextOverlay.getPos().y += labelHeight; //Once again, move text panel down a bit to leave room for the label.

				contentPane.getContent(1, 0).attach(tempScheduleTextOverlay);

				contentPane.getContent(1,0).onInit(); //idk when this should be done

				tempScheduleTextOverlay.getPos().add(padding);
				labelOverlay.getPos().add(padding);

				contentPane.setTextBoxHeight(1, 0, labelHeight + 75); //Leave room for temp. schedule information text box.

				//TODO: add AegisRecalibrationElement instead of tempScheduleTextOverlay; it was screwing up the whole layout and doesn't work yet

				GUIAncor recalContainer = contentPane.addNewTextBox(1, 28); //Add another text box to the content pane to hold the recalibration button.
				//GUITextButton timeChangeButton = getRecalibrationTimeChangeButton(contentPane.getContent(1, 1));
				//contentPane.getContent(1, 1).attach(timeChangeButton);
				//contentPane.setTextBoxHeight(1, 1, (int) timeChangeButton.getHeight());
				createRecalibrationTimeChangeButtons(recalContainer);
			}

			{ //Status
				contentPane.addNewTextBox(1, windowHeight/3); //Divider between schedule and status sections.
				AegisSystemStatusScrollableList statusScrollableList = new AegisSystemStatusScrollableList(getState(), contentPane.getContent(1, 2), systemStats, status);
				statusScrollableList.onInit();
				contentPane.getContent(1, 2).attach(statusScrollableList);

				contentPane.addNewTextBox(1, (bottomHeight/2) - 28); //Add another text box to hold the status text.
				String statusText = getStatusText();
				int textHeight = FontLibrary.getFont(FontLibrary.FontSize.MEDIUM).getHeight(statusText);
				statusTextOverlay = new GUITextOverlay(rightWidth, textHeight, getState());
				statusTextOverlay.setFont(FontLibrary.FontSize.MEDIUM.getFont());
				statusTextOverlay.setTextSimple(statusText);
				statusTextOverlay.setColor(status.getPromptColor());
				statusTextOverlay.autoWrapOn = contentPane.getContent(1,3); //TODO: Doesn't work with padding
				statusTextOverlay.autoHeight = true;
				statusTextOverlay.onInit();
				statusTextOverlay.getPos().add(padding);
				statusTextOverlay.setWidth((int) (statusTextOverlay.getWidth() - padding.x));
				contentPane.getContent(1, 3).attach(statusTextOverlay);
				contentPane.getContent(1,3).setHeight(statusTextOverlay.getHeight() + 5);

				contentPane.addNewTextBox(1,0); //Add another text box to hold the activation button.
				//GUITextButton activationButton = getActivationButton(contentPane.getContent(1, 4));
				//contentPane.getContent(1, 4).attach(activationButton);
				//contentPane.setTextBoxHeight(1, 4, (int) activationButton.getHeight());
				createStatusButtons(contentPane.getContent(1,4)); //add the button
				//TODO: add 'remove fuel' button with dialogue box to get Aegis Cells back out of the system
				contentPane.setTextBoxHeight(1,4,28); //hold button area height constant
				contentPane.setTextBoxHeightLast(1,28); //???
			}
		}
		return contentPane;
	}

	@Override
	public void draw() {
		statusTextOverlay.setTextSimple(getStatusText()); //just updates any live timestamps, but doesn't actually update the info beyond that
		statusTextOverlay.setColor(status.getPromptColor());
		tempScheduleTextOverlay.setTextSimple(getTempScheduleString());
		contentPane.getContent(1,3).setHeight(statusTextOverlay.getHeight() + 5);
		//TODO: Overhaul this to a subscriber system where active UI will be updated by the server itself whenever something happens with the Aegis Core

		//if(getHeight() != prevHeight) {
			int heightOfOtherStuff = 0;
			heightOfOtherStuff += contentPane.getContent(1, 0).getHeight();
			heightOfOtherStuff += contentPane.getContent(1, 1).getHeight();
			heightOfOtherStuff += contentPane.getContent(1, 2).getHeight();
			int dividerSize = 13; //????
			heightOfOtherStuff += (5 * dividerSize); //3 dividers above + 2 below need to be accounted for

			contentPane.setTextBoxHeight(1, 3, guiWindow.getInnerHeigth() - (heightOfOtherStuff + 25)); //set 4th box to fill rest of panel except button
			contentPane.setTextBoxHeight(1, 4, 25); //hold button area height constant
			contentPane.setTextBoxHeightLast(1, 25); //???
		//	prevHeight = getHeight(); //didn't work until rescaled, even though I set the initial prevHeight value to nonsense. :\
		//}

		super.draw();
	}

	private String getTempScheduleString() {
		long startTime = systemStats.getRecalibrationStart();
		long now = TemporalShortcuts.currentTimeOfDay();
		Calendar start = TemporalShortcuts.msToTimeToday(startTime);
		Calendar end = TemporalShortcuts.msToTimeToday(startTime + systemStats.getRecalibrationDuration());
		long tMinus;
		if(startTime > now) tMinus = startTime - now;
		else tMinus = (MS_TO_DAYS - now) + startTime;

		String startHour = String.format("%02d",start.get(Calendar.HOUR_OF_DAY));
		String startMinute = String.format("%02d",start.get(Calendar.MINUTE));

		String endHour = String.format("%02d",end.get(Calendar.HOUR_OF_DAY));
		String endMinute = String.format("%02d",end.get(Calendar.MINUTE));

		String result =  "Current Recalibration Time: " + startHour + ":" + startMinute + " - " + endHour + ":" + endMinute + " GMT" +
				"\n(" + StringTools.formatTimeFromMS(tMinus) + " from now";
		if(status == ACTIVE_HACKED && (systemStats.getLatestDisruptStart() - now()) < MS_TO_DAYS) result += "; structure will be vulnerable";
		result += ")";

		if(systemStats.getQueuedTimeChange() != -1L){
			Calendar newStart = TemporalShortcuts.msToTimeToday(systemStats.getQueuedTimeChange());
			String nsHour = String.format("%02d",newStart.get(Calendar.HOUR_OF_DAY));
			String nsMinute = String.format("%02d",newStart.get(Calendar.MINUTE));

			result += "\nQueued Recalibration Time: " + nsHour + ":" + nsMinute + " GMT" +
					" (Will apply after " + StringTools.formatTimeFromMS(systemStats.getTimeChangeCooldownRemaining()) + ")";
		}

		return result;
	}

	private String getStatusText() {
		String result = status.getInfoLong();
		if(status == ACTIVE_HACKED){
			long currentTime = now();
			long recalStart = systemStats.getLatestDisruptStart();
			long timeUntilVulnerable = recalStart - currentTime;
			result += "\nWILL GO OFFLINE IN " + StringTools.formatTimeFromMS(timeUntilVulnerable) + ", FOR " + StringTools.formatTimeFromMS(systemStats.getRecalibrationDuration());
		}
		if(status == VULNERABLE){
			long windowEndTime = (systemStats.getLatestDisruptStart() + systemStats.getRecalibrationDuration()) - now();
			result += "\nWILL REACTIVATE IN " + StringTools.formatTimeFromMS(windowEndTime);
		}
		return result;
	}

	/**
	 * @param attachTo div to attach the button pane to
	 */
	private GUIHorizontalButtonTablePane createRecalibrationTimeChangeButtons(GUIAncor attachTo){ //assume target already initialized
		GUIHorizontalButtonTablePane buttPane = new GUIHorizontalButtonTablePane(getState(),1,1,attachTo); //a pane in the back-end... or actually the front-end
		buttPane.onInit();

		final boolean isCoolingDown = systemStats.getTimeChangeCooldownRemaining() > 0;
		String text = "SET RECALIBRATION TIME";
		if(isCoolingDown) text += "  (COOLING DOWN: " + StringTools.formatTimeFromMS(systemStats.getTimeChangeCooldownRemaining()) + "; NEW TIME WILL BE QUEUED)"; //TODO linebreak may such
		HButtonType type = isCoolingDown ? HButtonType.BUTTON_BLUE_MEDIUM : HButtonType.BUTTON_BLUE_LIGHT;

		GUIHorizontalArea btn = buttPane.addButton(0,0, text, type, new GUICallback(){

			@Override
			public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
				// activate time changing dialogue box. Needs to be able to read time inputs somehow...
				// (either a series of dropdowns or string interpretation.)
				if (mouseEvent.pressedLeftMouse()) {
					if(systemStats.getQueuedTimeChange() != -1) recalibrationTimeDialog.setCurrentRecalibrationTime(systemStats.getQueuedTimeChange());
					else recalibrationTimeDialog.setCurrentRecalibrationTime(systemStats.getRecalibrationStart());
					recalibrationTimeDialog.activate(systemStats.getUID(),systemStats.getCoreLocationAbsIndex());
				}
			}

			@Override
			public boolean isOccluded() {
				return !isActive();
			}
		}, new GUIActivationCallback() {
			@Override
			public boolean isVisible(InputState inputState) {
				return true;
			}

			@Override
			public boolean isActive(InputState inputState) {
				return true;
			}
		});
		btn.onInit();
		attachTo.attach(buttPane);
		return buttPane;
	}

	/**
	 * @param attachTo div to attach the button pane to
	 */
	private GUIHorizontalButtonTablePane createStatusButtons(GUIAncor attachTo){ //assume target already initialized
		GUIHorizontalButtonTablePane buttPane = new GUIHorizontalButtonTablePane(getState(),1,1,attachTo);
		buttPane.onInit();

		String text;
		HButtonType type;
		boolean buttonActiveColorMode = true;
		if(!systemStats.isActivated()){
			text = "ACTIVATE";
			type = HButtonType.BUTTON_BLUE_MEDIUM;
		}
		else if(status == ONLINING){
			buttonActiveColorMode = false;
			text = "DEACTIVATE";
			type = HButtonType.BUTTON_BLUE_MEDIUM;
		}
		else if(systemStats.isDisrupted()){
			buttonActiveColorMode = false;
			text = "DEACTIVATE (CANNOT PROCEED: SYSTEM IS DISRUPTED)";
			type = HButtonType.BUTTON_RED_LIGHT;
		}
		else{ //active
			buttonActiveColorMode = false;
			text = "DEACTIVATE";
			type = HButtonType.BUTTON_RED_DARK;
		}

		//final boolean finalButtonActiveColorMode = buttonActiveColorMode;
		GUIHorizontalArea activationButton = buttPane.addButton(0,0, text, type, new GUICallback(){

			@Override
			public void callback(GUIElement guiElement, MouseEvent event) {
				if(!systemStats.isDisrupted() && event.pressedLeftMouse()) {
					PacketUtil.sendPacketToServer(new AegisUIInputPacket(!systemStats.isActivated(), systemStats.getUID(), systemStats.getCoreLocationAbsIndex()));
				} //else do nothing
			}

			@Override
			public boolean isOccluded() {
				return !isActive();
			}
		}, new GUIActivationCallback() {
			@Override
			public boolean isVisible(InputState inputState) {
				return true;
			}

			@Override
			public boolean isActive(InputState inputState) {
				return true;
			}
		});
		activationButton.onInit();

		/* TODO GUIHorizontalArea takeOutFuelButton = buttPane.addButton(0,0,"REMOVE FUEL",HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback(){
				@Override
				public void callback(GUIElement guiElement, MouseEvent event) {
					if (mouseEvent.pressedLeftMouse()) {
						fuelReclaimDialog.setAvailableFuel(status.getAegisChargeStored());
						fuelReclaimDialog.activate();
					}
				}

				@Override
				public boolean isOccluded() {
					return !isActive();
				}
			}, new GUIActivationCallback() {
				@Override
				public boolean isVisible(InputState inputState) {
					return true;
				}

				@Override
				public boolean isActive(InputState inputState) {
					return true;
				}
		});
		takeOutFuelButton.onInit();
		*/

		attachTo.attach(buttPane);
		return buttPane;
	}

	/*

	private GUITextButton getRecalibrationTimeChangeButton(GUIAncor contentPane) {
		if(systemStats.getTimeChangeCooldownRemaining() > 0) {
			return new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "CHANGE RECALIBRATION TIME\n(COOLING DOWN: " + StringTools.formatTimeFromMS(systemStats.getTimeChangeCooldownRemaining()) + ": NEW TIME WILL BE QUEUED)", new GUICallback() {
				@Override
				public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
					if(mouseEvent.pressedLeftMouse()) {
						//Todo: Queue recalibration time change request.
					}
				}

				@Override
				public boolean isOccluded() {
					return false;
				}
			});
		} else {
			return new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "CHANGE RECALIBRATION TIME", new GUICallback() {
				@Override
				public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
					if(mouseEvent.pressedLeftMouse()) {
						//Todo: Change recalibration time.
					}
				}

				@Override
				public boolean isOccluded() {
					return false;
				}
			});
		}
	}

	private GUITextButton getActivationButton(GUIAncor contentPane) {
		GUITextButton button = null;
		switch(status) {
			case ACTIVE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "DEACTIVATE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						if(mouseEvent.pressedLeftMouse()) {
							//Todo: Deactivate the system here
						}
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.OK);
				break;
			case ACTIVE_HACKED:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "DEACTIVATE\n(NOT ALLOWED: SYSTEM IS DISRUPTED)", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Do nothing
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return true;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.HOSTILE);
				break;
			case VULNERABLE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "VULNERABLE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Todo: IDK
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.CANCEL);
				break;
			case ONLINING: //Not a word really, but it's a good enough description of the state.
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "POWERING UP", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Todo: IDK
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.TUTORIAL);
				break;
			case NO_CHARGE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "INSUFFICIENT CHARGE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Todo: IDK
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return true;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.NEUTRAL);
				break;
			case DISABLED:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "ACTIVATE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						if(mouseEvent.pressedLeftMouse()) {
							//Todo: Activate the system here
						}
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.FRIENDLY);
				break;
			case ERROR_STALE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "ERROR", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Do nothing
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return true;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.HOSTILE);
				break;
		}
		return button;
	}

	 */
}
