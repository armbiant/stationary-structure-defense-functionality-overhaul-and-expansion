package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class AegisEnhancerUnit extends ElementCollection<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager> {
    int enhancement;

    public void refreshSubsysCapabilities(AegisEnhancementCollectionManager m) {
        this.enhancement = getNeighboringCollection().size();
        m.addEnhancement(this.enhancement);
    }

    @Override
    public String toString() {
        return "Subsystem Enhancer Unit";
    }

    @Override
    public ControllerManagerGUI createUnitGUI(GameClientState state, ControlBlockElementCollectionManager<?, ?, ?> controlBlockElementCollectionManager, ControlBlockElementCollectionManager<?, ?, ?> controlBlockElementCollectionManager1) {
        return ControllerManagerGUI.create(state, Lng.str("Aegis Unit"), this,
                new ModuleValueEntry(Lng.str("Subsystem Enhancement"), enhancement));
    }
    //TODO: Might be able to do event-based aegis enhancer management instead of these things
    // after all, most "unit" capability is unneeded here
}
