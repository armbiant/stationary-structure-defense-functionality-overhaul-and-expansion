package org.ithirahad.bastioninitiative.persistence;

import org.apache.poi.util.NotImplemented;

@NotImplemented
public class VirtualStellarNexus extends VirtualAegisSubsystem {
    @Override
    public String getBlockEntryName() {
        return "Aegis Stellar Nexus";
    }

    @Override
    public void onLoadFromPersistence() {

    }

    @Override
    protected void onUpdate(boolean powered) {

    }
}
