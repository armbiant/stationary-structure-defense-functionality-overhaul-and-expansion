package org.ithirahad.bastioninitiative.entitysystems.aegis;

import api.mod.StarMod;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSubsystem;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;

import java.io.Serializable;

import static org.ithirahad.bastioninitiative.util.BIUtils.syncToNearbyClientsSafe;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;

public abstract class PersistentAegisSubsystem<VIRTUAL extends VirtualAegisSubsystem> extends AegisSubsystem implements Serializable {
    protected VIRTUAL virtualSystem;
    private final String blkEntryName;
    private final Class<VIRTUAL> virtualClass;

    public PersistentAegisSubsystem(SegmentController segmentController, ManagerContainer<?> managerContainer, StarMod modInstance, String blockEntryName, Class<VIRTUAL> virtual) {
        super(segmentController, managerContainer, modInstance, elementEntries.get(blockEntryName).id);
        blkEntryName = blockEntryName;
        virtualClass = virtual;
    }

    /**
    Creates a new persistent Aegis Subsystem with Bastion Initiative as the owning mod. Not recommended for extensions' subsystems.
     */
    public PersistentAegisSubsystem(SegmentController segmentController, ManagerContainer<?> managerContainer, String blockEntryName, Class<VIRTUAL> virtual) {
        super(segmentController, managerContainer, elementEntries.get(blockEntryName).id);
        blkEntryName = blockEntryName;
        virtualClass = virtual;
    }

    @SuppressWarnings("unchecked") //it actually is checked, just with a janky entry string :D
    @Override
    public void onServerBlockLinkedOrLoadedLinked(){
        initialize();
    }

    protected void initialize() {
        //connect with or create virtual system
        if(isOnServer() && getParentCore() != null) {
            boolean makeNew = false;
            AegisCore parent = getParentCore();
            VirtualAegisSystem parentVirtual = parent.getVirtualSystem();
            if (getParentCore().getVirtualSystem().hasSubsystem(getBlockEntryName())) {
                virtualSystem = (VIRTUAL) parentVirtual.getSubsystem(getBlockEntryName());
                makeNew = virtualSystem == null;
            } else makeNew = true;
            if(makeNew) {
                try {
                    virtualSystem = virtualClass.newInstance();
                    parentVirtual.putSubsystem(getBlockEntryName(),virtualSystem);
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }

        initialized = true;
        System.err.println("[MOD][BastionInitiative] Initialized " + getFullSystemName() + " for " + segmentController.getRealName() + " successfully.");
    }

    @Override
    protected void onServerBlockUnlinked() {
        deinitOnServer();
    }

    @Override
    public void onDeInitialize() {
        if(isOnServer()) deinitOnServer();
    }

    protected void deinitOnServer(){
        if(isOnServer() && getParentCore() != null) {
            VirtualAegisSystem parentVirtual = getParentCore().getVirtualSystem();
            if (parentVirtual != null && parentVirtual.hasSubsystem(blkEntryName)) {
                parentVirtual.removeSubsystem(blkEntryName);
            }
        }
        virtualSystem = null;
        initialized = false;
        System.err.println("[MOD][BastionInitiative] Deinitialized " + getFullSystemName() + " for " + segmentController.getRealName() + " successfully.");
        if(isOnServer()) syncToNearbyClientsSafe(this);
    }

    public boolean isInitialized(){
        return initialized;
    }

    protected final String getBlockEntryName(){
        return blkEntryName;
    };
}
