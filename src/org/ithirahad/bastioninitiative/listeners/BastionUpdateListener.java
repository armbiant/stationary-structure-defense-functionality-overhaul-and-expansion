package org.ithirahad.bastioninitiative.listeners;

import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;

import static org.ithirahad.bastioninitiative.BIConfiguration.BASTION_FIELD_PROTECTS_TURRETS;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.aegisInterference;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.bastionFieldStatusEffect;
import static org.schema.game.common.controller.damage.effects.InterEffectHandler.InterEffectType.*;

public class BastionUpdateListener {
    public static void setBastionDefense(SegmentController sc){
        if (sc instanceof ManagedUsableSegmentController) {
            ConfigEntityManager c = sc.getConfigManager();
            if(c != null && c.isActive(bastionFieldStatusEffect) && !c.isActive(aegisInterference)) {
                ManagedUsableSegmentController<?> msc = (ManagedUsableSegmentController<?>) sc;
                if (BASTION_FIELD_PROTECTS_TURRETS || (!sc.railController.isTurretDocked() || !sc.isAIControlled())) {
                    InterEffectSet block = sc.getEffectContainer().get(HitReceiverType.BLOCK);
                    block.setStrength(EM, 10);
                    block.setStrength(HEAT, 10);
                    block.setStrength(KIN, 10);
                }
            }
        }
    }
}
