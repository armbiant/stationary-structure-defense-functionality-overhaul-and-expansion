package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSupplier;
import org.ithirahad.bastioninitiative.entitysystems.aegis.PersistentAegisSubsystem;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.schine.graphicsengine.core.Timer;

import java.util.List;

import static  org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.schema.common.FastMath.ceil;

public class AegisTransmitter extends PersistentAegisSubsystem<VirtualAegisSupplier> {
    public AegisTransmitter(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, "Aegis Transmitter", VirtualAegisSupplier.class);
    }

    @Override
    public void handleServer(Timer timer, boolean hasBlock) {
        if(isInitialized()) {
            //TODO: Unloaded transmitter profile would distribute charge to all registered aegis recievers in range, proportional to their reqs. and time elapsed.
            // Simple on-demand provisioning with lazy updating would mean that the first station to update would hog power;
            // this would inadvertently lower the overall aegis requirement of a remote system, as only the stations actually getting updated
            // would ever draw aegis charge, and anything awaiting update would be irrelevant.
            // Just transmitting to all stations any time one updates should be fine.
        }
    }

    @Override
    public void onSerialize(PacketWriteBuffer b){

    }

    @Override
    public void onDeserialize(PacketReadBuffer b){

    }

    @Override
    public void onServerBlockLinkedOrLoadedLinked() {

    }

    @Override
    protected void onServerBlockUnlinked() {

    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        if(isPresentAndLinked()) {
            return (int) (AEGIS_TRANSMITTER_FUEL_CONSUMPTION_BASE + ceil(getFuelOutputWithLosses()));
        }
        else return 0;
    }

    private float getFuelOutputWithLosses() {
        if(isOnServer()) return AEGIS_TRANSMITTER_FUEL_CONSUMPTION_BASE + virtualSystem.getDailyOutputWithLosses();
        else return AEGIS_TRANSMITTER_FUEL_CONSUMPTION_BASE;
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return AEGIS_TRANSMITTER_FUEL_CONSUMPTION_ONLINING_BASE;
    }

    @Override
    public double getPowerConsumptionActive() {
        return 0;
    }

    @Override
    public String getFullSystemName() {
        return "Aegis Remote Transfer Subsystem";
    }

    @Override @NotImplemented
    public List<String> getInfoLines() {
        return null;
    }

    @Override
    public String getShortPurposeString() {
        return "Transmits Aegis Charge to other bases in the star system remotely";
    }
}
