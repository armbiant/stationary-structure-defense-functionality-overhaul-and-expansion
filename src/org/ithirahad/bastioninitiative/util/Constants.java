package org.ithirahad.bastioninitiative.util;

import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;

import javax.vecmath.Vector3f;

public class Constants {
    public static final Vector3f coreOffset = new Vector3f(-16F, -16F, -16F);
    public static final int MIN_PLAYER_FACTION_ID = 10000;

    public static final long MS_TO_YEARS = 31556925216L;
    public static final int MS_TO_DAYS = 86400000;
    public static final int MS_TO_HOURS = 3600000;
    public static final int MS_TO_MINUTES = 60000;

    public static int STATUS_UPDATE_MAX_ITERATIONS = VirtualAegisSystem.AegisSystemStatus.values().length + 1;
    //number of states possible plus 1, any more than this pretty much must be an infinite loop bug.
}
