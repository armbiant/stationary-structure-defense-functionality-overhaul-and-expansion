package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.network.packets.PacketUtil;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

import java.io.IOException;

import static org.ithirahad.bastioninitiative.util.BIUtils.factionAccessMayBeAllowed;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

/**
 Packet sent by a client to the server when they need information about an Aegis System in order to populate the UI.
 */
public class AegisPromptRequest extends Packet {
    String entityUID = "";
    long blockAbsIndex = 0;

    //not doing anything with this for now, but eventually we'll confirm the block is an Aegis Core
    //and calculate the relative positions of the block and the player trying to access it on serverside
    //to try and weed out a lot of potential shenaniganeering.

    public AegisPromptRequest(String entity, long absindex){
        this.entityUID = entity;
        blockAbsIndex = absindex;
    }

    public AegisPromptRequest(){}

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        entityUID = b.readString();
        blockAbsIndex = b.readLong();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeString(entityUID);
        b.writeLong(blockAbsIndex);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        SegmentController sc = GameServerState.instance.getSegmentControllersByName().get(entityUID);
        if(!(sc == null) && validOnServer(sc,playerState)) {
            short coreID = elementEntries.get("Aegis Core").id;
            if(sc.getSegmentBuffer().getPointUnsave(blockAbsIndex).getType() == coreID) //TODO: && worldPosition(sc,blockAbsIndex).sub(getPlayerWorldPosition(playerState)) <= activationRadiusMax)
            { //we can be reasonably sure that there is no direct tampering going on here
                AegisCore aegisCore = (AegisCore) ((ManagedUsableSegmentController<?>) sc).getManagerContainer().getModMCModule(coreID);
                //PlaceholderAegisPromptPacket uglyUIPacket = new PlaceholderAegisPromptPacket(aegisCore.getInfoAsServer(), aegisCore.isPowerConsumerActive(), sc.getUniqueIdentifier(), blockAbsIndex);
                //PacketUtil.sendPacket(playerState,uglyUIPacket);

                AegisSystemUIPromptPacket pkt = aegisCore.getUIInformationPacket();
                PacketUtil.sendPacket(playerState,pkt); //right back atchya!
            }
            else playerState.sendServerMessage(Lng.astr(sc.getRealName() + " does not have an Aegis Core here! Are you tampering with your local block data? You cheeky bugger, you..."), MESSAGE_TYPE_WARNING);
        }
    }

    private boolean validOnServer(SegmentController sc, PlayerState playerState){
        return sc.getSegmentBuffer().getPointUnsave(blockAbsIndex).getType() == elementEntries.get("Aegis Core").id &&
                factionAccessMayBeAllowed(playerState.getFactionId(),sc.getFactionId()) &&
                sc.getSector(new Vector3i()).equals(playerState.getCurrentSector());
    }

    @Override
    public void processPacketOnClient() {/*How did we even get here?*/}
}
