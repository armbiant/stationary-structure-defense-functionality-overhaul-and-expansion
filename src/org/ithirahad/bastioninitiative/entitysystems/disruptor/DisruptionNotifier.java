package org.ithirahad.bastioninitiative.entitysystems.disruptor;

import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class DisruptionNotifier {
    private static final long COOLDOWN_MS = 2500; //cooldown for notifs about one entity
    private static final Int2LongOpenHashMap lastNotificationTimes = new Int2LongOpenHashMap(); //TODO move to Aegis Core value if performance is poor
    public static void onDisrupt(int attackingFaction, ManagedUsableSegmentController<?> structure) {
        if(!lastNotificationTimes.containsKey(structure.getId()) || (now() - lastNotificationTimes.get(structure.getId()) > COOLDOWN_MS)){
            doNotif(structure,attackingFaction);
        }
        lastNotificationTimes.put(structure.getId(),now());
    }

    private static void doNotif(ManagedUsableSegmentController<?> structure, int attackingFaction) {
        if(structure.getFactionId() != 0) {
            Faction owner = structure.getFaction();
            if(owner.isPlayerFaction()) {
                String attacker;
                if (attackingFaction == 0) attacker = "A neutral entity";
                else {
                    Faction enemy = GameServerState.instance.getFactionManager().getFaction(attackingFaction);
                    attacker = enemy.getName();
                }
                owner.broadcastMessage(Lng.astr(attacker + " is attempting to disrupt your structure in " + structure.getSector(new Vector3i())), MESSAGE_TYPE_INFO, GameServerState.instance);
            }
        }
    }
}
