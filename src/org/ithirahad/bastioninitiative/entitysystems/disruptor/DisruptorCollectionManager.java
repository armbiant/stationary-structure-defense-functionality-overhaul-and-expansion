package org.ithirahad.bastioninitiative.entitysystems.disruptor;

import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.beam.AbstractBeamHandler;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;

public class DisruptorCollectionManager extends BeamCollectionManager<DisruptorUnit,DisruptorCollectionManager,DisruptorElementManager> {

    private final DisruptorBeamHandler handler;

    public DisruptorCollectionManager(SegmentPiece segmentPiece, SegmentController segmentController, DisruptorElementManager disruptorElementManager) {
        super(segmentPiece, elementEntries.get("Hacking Module").id, segmentController, disruptorElementManager);
        handler = new DisruptorBeamHandler(segmentController, this);
    }

    @Override
    public AbstractBeamHandler<SegmentController> getHandler() {
        return handler;
    }

    @Override
    public void addHudConext(ControllerStateUnit controllerStateUnit, HudContextHelpManager hcm, HudContextHelperContainer.Hos hos) {
        hcm.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.str("Disrupt Aegis System"), hos, ContextFilter.IMPORTANT);
    }


    @Override
    public FireMode getFireMode() {
        return FireMode.FOCUSED;
    }

    @Override
    protected Class<DisruptorUnit> getType() {
        return DisruptorUnit.class;
    }

    @Override
    public DisruptorUnit getInstance() {
        return new DisruptorUnit();
    }

    @Override
    public String getModuleName() {
        return Lng.str("Disruption Datalink System");
    }
}
