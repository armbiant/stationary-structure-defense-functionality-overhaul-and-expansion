package org.ithirahad.bastioninitiative.persistence;

import me.iron.WarpSpace.Mod.WarpManager;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.StellarSystem;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import static org.ithirahad.bastioninitiative.BastionInitiative.warpSpaceIsPresentAndEnabled;
import static org.ithirahad.bastioninitiative.util.BIUtils.existsInDB;

/**
 * Contains all the persistent information
 */
public class BastionPersistenceContainer implements Serializable {
    /**
     * A map of all extant Aegis Systems (i.e. Aegis Cores) by their entity's UID.
     */
    private final HashMap<String, VirtualAegisSystem> systemsByID = new HashMap<>();
    /**
     * A map of all extant Aegis Systems (i.e. Aegis Cores) by sector.
     */
    private transient final HashMap<Vector3i,HashSet<VirtualAegisSystem>> systemsBySector = new HashMap<>();
    /**
     * A map of all extant Aegis Systems (i.e. Aegis Cores) by star/void system block.
     */
    private transient final HashMap<Vector3i,HashSet<VirtualAegisSystem>> systemsByStarSystem = new HashMap<>();
    /**
     * A map of all extant Aegis Systems (i.e. Aegis Cores) by WarpSpace warp sector. Empty if WarpSpace is not installed.
     */
    private transient final HashMap<Vector3i,HashSet<VirtualAegisSystem>> systemsByWarpLocation = new HashMap<>();


    public void addAegisSystem(VirtualAegisSystem newSys){
        addSysToLocation(newSys);
        systemsByID.put(newSys.uid, newSys);
    }

    private void addSysToLocation(VirtualAegisSystem newSys){
        Vector3i sec = newSys.getSectorLocation();
        addOrGetForSector(sec).add(newSys);
        addOrGetForSystem(StellarSystem.getPosFromSector(sec, new Vector3i())).add(newSys);
        if (warpSpaceIsPresentAndEnabled) {
            Vector3i warpPos = WarpManager.getWarpSpacePos(sec);
            HashSet<VirtualAegisSystem> debugHS = addOrGetForWarpLoc(warpPos);
            debugHS.add(newSys);
        }
    }

    public void removeAegisSystem(VirtualAegisSystem sys){
        Vector3i sec = sys.getSectorLocation();
        if(systemsBySector.containsKey(sec)){
            HashSet<VirtualAegisSystem> set = systemsBySector.get(sec);
            set.remove(sys);
            if(set.isEmpty()) systemsBySector.remove(sec); //This b***h empty; YEET!
        }

        Vector3i stellarSystem = StellarSystem.getPosFromSector(sec, new Vector3i());
        if(systemsByStarSystem.containsKey(stellarSystem)){
            HashSet<VirtualAegisSystem> set = systemsByStarSystem.get(stellarSystem);
            set.remove(sys);
            if(set.isEmpty()) systemsByStarSystem.remove(stellarSystem);
        }

        if(warpSpaceIsPresentAndEnabled){
            Vector3i warpSector = WarpManager.getWarpSpacePos(sec);
            if(systemsByWarpLocation.containsKey(warpSector)){
                HashSet<VirtualAegisSystem> set = systemsByWarpLocation.get(warpSector);
                set.remove(sys);
                if(set.isEmpty()) systemsByWarpLocation.remove(warpSector);
            }
        }

        else if(!systemsByID.containsKey(sys.uid)) return; //there was nothing there at all
        systemsByID.remove(sys.uid);
    } //TODO: if a station/asteroid changes sectors, we have to clean up the extant PAS, refresh the values, and reinsert it!
    // (this seems like an exceptional enough case that I'd be OK with discharging all fuel and resetting it to DISABLED too)

    public void removeAegisSystem(String id){
        if(!aegisSystemExists(id)) return;
        VirtualAegisSystem cons = getAegisSystemsByID(id);
        removeAegisSystem(cons);
    }

    public boolean aegisSystemExists(String id) {
        return systemsByID.containsKey(id);
    }

    public VirtualAegisSystem getAegisSystemsByID(String id){
        return systemsByID.get(id);
    }

    public HashSet<VirtualAegisSystem> getAegisSystemsInSector(Vector3i sector){
        return systemsBySector.get(sector);
    }

    public HashSet<VirtualAegisSystem> getAegisSystemsInSystem(Vector3i sys) {
        return systemsByStarSystem.get(sys);
    }

    public void copyFrom(BastionPersistenceContainer retrievedContainer) {
        systemsBySector.clear();
        systemsBySector.putAll(retrievedContainer.systemsBySector);

        systemsByID.clear();
        systemsByID.putAll(retrievedContainer.systemsByID);
    }

    public void beforeSerialize() {
        LinkedList<String> toRemove = new LinkedList<>();
        for(VirtualAegisSystem sys : systemsByID.values()){
            if (existsInDB(sys.uid)) { //TODO: Make sure existsInDB is working
                sys.update(); //ensure everything has consumed its appropriate fuel/etc., as none will be consumed during downtime
                sys.beforeWriteToPersistence();
            }
            else {
                toRemove.add(sys.uid);
            }
        }
        for(String id : toRemove){
            removeAegisSystem(id);
        }
    }

    public void afterDeserialize() {
        for(VirtualAegisSystem sys : systemsByID.values()) {
            HashSet<VirtualAegisSystem> here = addOrGetForSector(sys.getSectorLocation());
            here.add(sys);
            HashSet<VirtualAegisSystem> inStellar = addOrGetForSystem(StellarSystem.getPosFromSector(sys.getSectorLocation(), new Vector3i()));
            inStellar.add(sys);
            if(warpSpaceIsPresentAndEnabled){
                addOrGetForWarpLoc(WarpManager.getWarpSpacePos(sys.getSectorLocation())).add(sys);
            }
        }
        for(VirtualAegisSystem sys : systemsByID.values()) sys.onLoadFromPersistence();
        //have to do this after, so that virtual subsystems that network (Aegis Hub, Transmitter, etc.) will be able to find everything else.
    }

    private HashSet<VirtualAegisSystem> addOrGetForSector(Vector3i sector) {
        if(!systemsBySector.containsKey(sector)) systemsBySector.put(sector,new HashSet<VirtualAegisSystem>());
        return systemsBySector.get(sector);
    }

    private HashSet<VirtualAegisSystem> addOrGetForSystem(Vector3i systemCoords) {
        if(!systemsByStarSystem.containsKey(systemCoords)) systemsByStarSystem.put(systemCoords,new HashSet<VirtualAegisSystem>());
        return systemsByStarSystem.get(systemCoords);
    }

    private HashSet<VirtualAegisSystem> addOrGetForWarpLoc(Vector3i warpSector) {
        if(!systemsByWarpLocation.containsKey(warpSector)) systemsByWarpLocation.put(warpSector,new HashSet<VirtualAegisSystem>());
        return systemsByWarpLocation.get(warpSector);
    }

    public boolean hasWarpEntries(Vector3i sector) {
        return systemsByWarpLocation.containsKey(sector);
    }

    public HashSet<VirtualAegisSystem> getAegisSystemsInWarpSector(Vector3i loc) {
        return systemsByWarpLocation.get(loc);
    }

    public boolean hasSystemEntries(Vector3i systemPos) {
        return systemsByStarSystem.containsKey(systemPos);
    }
}
