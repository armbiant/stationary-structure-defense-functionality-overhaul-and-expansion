package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import me.iron.WarpSpace.Mod.WarpManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.PersistentAegisSubsystem;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLTrap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.schine.graphicsengine.core.Timer;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Math.round;
import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.util.BIUtils.isSunRangeServer;
import static org.ithirahad.bastioninitiative.util.BIUtils.syncToNearbyClientsSafe;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.warpSpaceIsPresentAndActive;

public class FTLTrap extends PersistentAegisSubsystem<VirtualFTLTrap> {
    private static final String blkEntryName = "Aegis Anti FTL"; //legacy name, kinda stuck with it now
    private transient int lastEnhancement = -1; //must update the first time

    public FTLTrap(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, blkEntryName, VirtualFTLTrap.class);
    }

    public boolean isInitialized(){
        return initialized;
    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        if(initialized) return AEGIS_ANTI_FTL_BASE_FUEL_CONS + round(getEnhancement() * AEGIS_ANTI_FTL_FUEL_PER_ENHANCER);
        else return 0;
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return isPresentAndLinked()? AEGIS_ANTI_FTL_FUEL_CONSUMPTION_ONLINING_BASE : 0;
    }

    @Override
    public double getPowerConsumptionActive() {
        return initialized ? AEGIS_ANTI_FTL_BASE_POWER_CONS + (getEnhancement() * AEGIS_ANTI_FTL_POWER_PER_ENHANCER) : 0;
    }

    @Override
    public String getFullSystemName() {
        return "Aegis FTL Interception Subsystem";
    }

    @Override
    public List<String> getInfoLines() {
        LinkedList<String> result = new LinkedList<>();
        if(isSunRangeServer(segmentController)){
            result.add("!!! DESTABILIZED BY STAR PROXIMITY !!!");
            result.add("Subsystem will not function at this distance from a star.");
        }else {
            result.add("Range: " + (warpSpaceIsPresentAndActive ? WarpManager.getScale() : (AEGIS_ANTI_FTL_RANGE_BASE + (getEnhancement() * AEGIS_ANTI_FTL_RANGE_PER_ENHANCER))) + " Sectors");
        }
        return result;
    }

    @Override
    public String getShortPurposeString() {
        return "Interrupts enemy and neutral FTL travel through the area, redirecting it to this sector.";
    }

    @Override
    protected void handleServer(Timer timer, boolean hasBlocks) {
        if(isInitialized() && !segmentController.getRemoteSector().isWrittenForUnload() && hasBlocks){
            virtualSystem.setPowered(isPowered());
            int enhancement = getEnhancement();
            if(enhancement != lastEnhancement || virtualSystem.neverUpdated()) {
                float v = AEGIS_ANTI_FTL_RANGE_BASE + (enhancement * AEGIS_ANTI_FTL_RANGE_PER_ENHANCER);
                if (warpSpaceIsPresentAndActive) {
                    v *= AEGIS_ANTI_FTL_RANGE_WARPSPACE_MODIFIER_PER_WARP_FACTOR;
                    v *= 1. / WarpManager.getScale();
                }
                virtualSystem.setRange(v);
                virtualSystem.setDisabledByEnvironment(isSunRangeServer(segmentController)); //Flag env. disable if too close to a star, to prevent burn traps
                lastEnhancement = enhancement;
                syncToNearbyClientsSafe(this);
            }
            virtualSystem.update();
        }
    }

    @Override
    protected void onSerialize(PacketWriteBuffer b) throws IOException {
        b.writeInt(lastEnhancement);
    }

    @Override
    protected void onDeserialize(PacketReadBuffer b) throws IOException {
        lastEnhancement = b.readInt();
    }
}
