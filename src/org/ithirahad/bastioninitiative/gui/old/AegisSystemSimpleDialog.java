package org.ithirahad.bastioninitiative.gui.old;

import api.utils.gui.GUIInputDialogPanel;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

/*Tentative dialog system*/
public class AegisSystemSimpleDialog extends GUIInputDialogPanel {
//TODO: For now, recycle time = activation time. (and prompt the player about this)
    GUITextOverlay infoOverlayUpper;
    //GUITextOverlay lowerOverlay;

    public AegisSystemSimpleDialog(InputState inputState, GUICallback guiCallback) {
        super(inputState, "Aegis System Simple Activation", "Aegis System", "", 750, 550, guiCallback); //third string is description text, which overlaps top text...
    }

    @Override
    public void onInit() {
        super.onInit();
        GUIContentPane contentPane = ((GUIDialogWindow) background).getMainContentPane();
        //Get content pane
        contentPane.setTextBoxHeightLast(350);
        //This just ensures the content pane doesn't have a height of 0, the actual height will automatically
        //adjust to fill the area when it's drawn

        GUIAncor mainContent = contentPane.getContent(0);
        //Get the initial content pane before adding new ones

        /*
        GUIAncor lowerContent = contentPane.addNewTextBox(0, 150).getContent();
        //Add a new content pane at the bottom
        contentPane.addDivider(300);
        final GUIAncor rightContent = contentPane.getContent(1, 0);
        //Add a divider to the right of both panes
        */

        infoOverlayUpper = new GUITextOverlay(10, 10, getState());
        infoOverlayUpper.autoWrapOn = mainContent;
        //Have the text overlay automatically wrap text to fit the content area
        infoOverlayUpper.onInit();
        //Initialize overlay
        infoOverlayUpper.setTextSimple("<PLACEHOLDER TEXT>");
        infoOverlayUpper.setFont(FontLibrary.getBlenderProBook14());
        mainContent.attach(infoOverlayUpper);

        /*
        lowerOverlay = new GUITextOverlay(10, 10, getState());
        lowerOverlay.autoWrapOn = lowerContent;
        lowerOverlay.onInit();
        lowerOverlay.setTextSimple("BOTTOM TEXT");
        lowerContent.attach(lowerOverlay);
         */

        /*
        StarLoaderTexture.runOnGraphicsThread(new Runnable() {
            @Override
            public void run() {
                try {
                    URLConnection connection = (new URL("https://www.downloadclipart.net/medium/1978-shield-1-clip-art.png")).openConnection();
                    connection.setRequestProperty("User-Agent", "NING/1.0");
                    BufferedImage image = ImageIO.read(connection.getInputStream());
                    Sprite sprite = StarLoaderTexture.newSprite(image, ModPlayground.inst, "shield");
                    GUIOverlay rightOverlay = new GUIOverlay(sprite, getState());
                    rightOverlay.onInit();
                    rightContent.attach(rightOverlay);
                } catch(IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
         */
    }
}
