package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.textures.StarLoaderTexture;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

import static org.ithirahad.bastioninitiative.gui.corepanel.BIGUIInstanceContainer.aegisSystemUIControlManager;

public class AegisSystemUIPromptPacket extends Packet {
    private boolean active;
    private String subsystemInfoText; //wall of text describing the enhancement and functionality of each attached Aegis Subystem; I will make this better later
    private long recalibrationStart; //time when recalibration period begins
    private long recalibrationDuration; //length of recalibration period; fixed for now but will be changeable later
    private boolean isDisrupted; //whether or not the Aegis System is disrupted
    private long timeChangeCooldownRemaining; //length of time before changing the recalibration period is allowed.
    private long queuedTimeChange; //Currently planned recalibration time after time-change cooldown expires. -1L if no time change is queued
    private int aegisSystemActivationCost; //Cost to turn on the system
    private int aegisSystemDailyCost; //'Fuel' drain per day
    private double aegisChargeStored; //'Fuel' currently stored
    private int aegisChargeCapacity; //-1 if infinite (it's infinite for now)
    private long fuelledUntil; //how long before the system is out of charge. Can be locally calculated, but it seems easier to just provide it (and will show bugs more readily)
    private int statusIndex; //ordinal of the status enum
    private AegisSystemStatus trueStatus; //status of the system
    private boolean isAwaitingActivationCharge;
    private long absPos;
    private String uid;
    private long latestDisruptStart;

    public AegisSystemUIPromptPacket(String uid, long abs, boolean active, boolean isAwaitingActivationCharge, String subsystemInfoText, long recalibrationStart, long recalibrationDuration, boolean isDisrupted, long timeChangeCooldownRemaining, long queuedTimeChange, int aegisSystemActivationCost, int aegisSystemDailyCost, double aegisChargeStored, int aegisChargeCapacity, long fuelledUntil, long latestDisruptStart, AegisSystemStatus status){
        this.uid = uid;
        this.absPos = abs;
        this.active = active;
        this.isAwaitingActivationCharge = isAwaitingActivationCharge;
        this.subsystemInfoText = subsystemInfoText;
        this.recalibrationStart = recalibrationStart;
        this.recalibrationDuration = recalibrationDuration;
        this.isDisrupted = isDisrupted;
        this.timeChangeCooldownRemaining = timeChangeCooldownRemaining;
        this.queuedTimeChange = queuedTimeChange;
        this.aegisSystemActivationCost = aegisSystemActivationCost;
        this.aegisSystemDailyCost = aegisSystemDailyCost;
        this.aegisChargeStored = aegisChargeStored;
        this.aegisChargeCapacity = aegisChargeCapacity;
        this.fuelledUntil = fuelledUntil;
        this.latestDisruptStart = latestDisruptStart;
        this.statusIndex = status.ordinal();
    }

    public AegisSystemUIPromptPacket(){}

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        latestDisruptStart = b.readLong();
        uid = b.readString();
        absPos = b.readLong();
        active = b.readBoolean();
        isAwaitingActivationCharge = b.readBoolean();
        subsystemInfoText = b.readString();
        recalibrationStart = b.readLong();
        recalibrationDuration = b.readLong();
        isDisrupted = b.readBoolean();
        timeChangeCooldownRemaining = b.readLong();
        queuedTimeChange = b.readLong();
        aegisSystemActivationCost = b.readInt();
        aegisSystemDailyCost = b.readInt();
        aegisChargeStored = b.readDouble();
        aegisChargeCapacity = b.readInt();
        fuelledUntil = b.readLong();
        statusIndex = b.readInt();

        trueStatus = AegisSystemStatus.values()[statusIndex];
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeLong(latestDisruptStart);
        b.writeString(uid);
        b.writeLong(absPos);
        b.writeBoolean(active);
        b.writeBoolean(isAwaitingActivationCharge);
        b.writeString(subsystemInfoText);
        b.writeLong(recalibrationStart);
        b.writeLong(recalibrationDuration);
        b.writeBoolean(isDisrupted);
        b.writeLong(timeChangeCooldownRemaining);
        b.writeLong(queuedTimeChange);
        b.writeInt(aegisSystemActivationCost);
        b.writeInt(aegisSystemDailyCost);
        b.writeDouble(aegisChargeStored);
        b.writeInt(aegisChargeCapacity);
        b.writeLong(fuelledUntil);
        b.writeInt(statusIndex);
    }

    @Override
    public void processPacketOnClient() {
        StarLoaderTexture.runOnGraphicsThread(new Runnable() {
            @Override
            public void run() {
                aegisSystemUIControlManager.activateWithInfo(uid, absPos, active, isAwaitingActivationCharge, subsystemInfoText,recalibrationStart,recalibrationDuration,isDisrupted,timeChangeCooldownRemaining,queuedTimeChange,aegisSystemActivationCost,aegisSystemDailyCost,aegisChargeStored,aegisChargeCapacity,fuelledUntil,latestDisruptStart,trueStatus);
            }
        });
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {

    }
}
