package org.ithirahad.bastioninitiative.persistence;

import api.listener.events.entity.ShipJumpEngageEvent;
import me.iron.WarpSpace.Mod.WarpJumpManager;
import me.iron.WarpSpace.Mod.WarpManager;
import org.ithirahad.bastioninitiative.listeners.BIWarpJumpListener;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;

import java.util.HashSet;
import java.util.Iterator;

import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.util.BIUtils.isSelfOrAlly;

public abstract class VirtualFTLInterdictorAbstract extends VirtualAegisSubsystem{
    protected float range; //in sectors
    protected transient boolean didFirstUpdate = false;
    protected boolean disabledByEnvironment = false;
    //takes advantage of how deserialized transient values are set to their defaults - in this case, false.

    public VirtualFTLInterdictorAbstract(){}

    public boolean crossesRange(Vector3i start, Vector3i end){
        return rangeSquared() >= BIUtils.shortestDistanceToLineSegmentSquared(start,end,getParent().getSectorLocation());
    }

    public boolean isInRange(Vector3i sector){
        return rangeSquared() >= Vector3i.getDisatanceSquaredD(sector,getParent().getSectorLocation());
    }

    public boolean isInRangeWarp(Vector3i loc) {
        return rangeSquared() >= Vector3i.getDisatanceSquaredD(loc, WarpManager.getWarpSpacePos(getParent().getSectorLocation()));
    }

    protected float rangeSquared() {
        return range*range;
    }

    public void setRange(float v){
        range = v;
    }

    public void setDisabledByEnvironment(boolean v){
        disabledByEnvironment = v;
    }

    @Override
    public boolean isOperating(boolean respectDisruptionStatus) {
        return super.isOperating(respectDisruptionStatus) && !disabledByEnvironment;
    }

    @Override
    public void onLoadFromPersistence() {

    }

    @Override
    public void onUpdate(boolean powered) {
        didFirstUpdate = true;
    }

    public abstract void handleEntityJumping(ShipJumpEngageEvent info);

    public abstract void handleEntityWarpSpace(ManagedUsableSegmentController<?> entity);

    public float getRange() {
        return range;
    }

    public boolean neverUpdated() {
        return !didFirstUpdate;
    }

    public static void doWarpSpaceInterdictorCheck(SegmentController sc) {
        if(!BIWarpJumpListener.shipQueued(sc.getUniqueIdentifier()) || !WarpJumpManager.dropQueue.contains(sc)) {
            ManagedUsableSegmentController<?> temmsc = (ManagedUsableSegmentController<?>) sc;
            Vector3i shipPos = new Vector3i();
            if (bsiContainer.hasWarpEntries(temmsc.getSector(shipPos))) { //also assigns to shipPos
                HashSet<VirtualFTLInterdictorAbstract> toCheck = new HashSet<>();
                for (VirtualAegisSystem sys : bsiContainer.getAegisSystemsInWarpSector(shipPos)) {
                    if (sys.isActive() && !isSelfOrAlly(sys.getFactionID(), temmsc.getFactionId())) {
                        for(VirtualAegisSubsystem subsys : sys.getSubsystems()) if(subsys instanceof VirtualFTLInterdictorAbstract) {
                            VirtualFTLInterdictorAbstract candidate = (VirtualFTLInterdictorAbstract) subsys;
                            if (candidate.isOperating(true) && candidate.isInRangeWarp(shipPos))
                                toCheck.add(candidate);
                        }
                    }
                }

                if (!toCheck.isEmpty()) {
                    Iterator<VirtualFTLInterdictorAbstract> dics = toCheck.iterator();

                    VirtualFTLInterdictorAbstract biggestDic = dics.next();
                    VirtualFTLInterdictorAbstract thisDic;
                    while (dics.hasNext()) {
                        thisDic = dics.next();
                        if (thisDic.getRange() > biggestDic.getRange()) biggestDic = thisDic;
                    }

                    biggestDic.handleEntityWarpSpace(temmsc);
                }
            }
        } //else already about to get yeeted
    }
}
