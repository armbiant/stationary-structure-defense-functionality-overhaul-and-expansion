package org.ithirahad.bastioninitiative.persistence;

import api.listener.events.entity.ShipJumpEngageEvent;
import api.utils.sound.AudioUtils;
import me.iron.WarpSpace.Mod.WarpJumpManager;
import org.ithirahad.bastioninitiative.listeners.BIWarpJumpListener;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

import static org.ithirahad.bastioninitiative.BIConfiguration.AEGIS_ANTI_FTL_ALLOWED;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class VirtualFTLTrap extends VirtualFTLInterdictorAbstract {

    public VirtualFTLTrap(){}

    @Override
    public boolean isOperating(boolean respectDisruptionStatus) {
        return super.isOperating(respectDisruptionStatus) && AEGIS_ANTI_FTL_ALLOWED;
    }

    @Override
    public String getBlockEntryName() {
        return "Aegis Anti FTL";
    }

    @Override
    public void handleEntityJumping(ShipJumpEngageEvent e) {
        e.getNewSector().set(getParent().getSectorLocation());
        for (PlayerState playerAboard : ((ManagedUsableSegmentController<?>)e.getController()).getAttachedPlayers()) {
            //TODO: time-delayed action, sound effect, particle effects, etc
            playerAboard.sendServerMessage(Lng.astr("WARNING: Jump field captured by warp interceptor! Ship is being pulled in!"), MESSAGE_TYPE_INFO);
        }
        AudioUtils.serverPlaySound("0022_gameplay - low armor _electric_ warning chime", 1.0f, 1.0f, ((ManagedUsableSegmentController<?>)e.getController()).getAttachedPlayers());
    }

    @Override
    public void handleEntityWarpSpace(ManagedUsableSegmentController<?> temmsc) {
        String faction;
        if (getFactionID() == 0) faction = "faction-neutral";
        else
            faction = GameServerState.instance.getFactionManager().getFactionName(getFactionID());
        for (PlayerState playerAboard : temmsc.getAttachedPlayers()) {
            playerAboard.sendServerMessage(Lng.astr("WARNING: Warp field destablilized by a " + faction + "  FTL interceptor system! Vessel is being returned to real space!"), MESSAGE_TYPE_INFO);
        }
        AudioUtils.serverPlaySound("0022_gameplay - low armor _electric_ warning chime", 1.0f, 1.0f, temmsc.getAttachedPlayers());

        BIWarpJumpListener.enqueueDropLocation(temmsc.getUniqueIdentifier(), this);
        WarpJumpManager.invokeDrop(3000, temmsc, true, false);
    }
}
