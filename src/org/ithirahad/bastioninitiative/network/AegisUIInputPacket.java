package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.network.packets.PacketUtil;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

import java.io.IOException;

import static org.ithirahad.bastioninitiative.util.BIUtils.factionAccessMayBeAllowed;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

/**
Packet sent by a client to the server when they desire to make changes to an Aegis System.
 <br\><b>NOTE: As an invulnerability system is especially prone to so-called "PvP" cheat and exploit shenanigans,
 serverside validation is <i>imperative</i> here, even down to (TODO) the player being within activation range of the block.</b>
 */
//for now, let's just be sure that the faction is right and they're actually in the sector. That's a start.
public class AegisUIInputPacket extends Packet {
    boolean requestingStateTransition; //are we activating/deactivating?
    boolean requestingTimeChange;
    boolean requestedState = false; //true = on; false = off
    String entityUID = "";
    long newCycleTime = 0;
    long blockAbsIndex = 0;

    //not doing anything fancy with this for now, but eventually we'll confirm the block is an Aegis Core
    //and calculate the relative positions of the block and the player trying to access it on serverside
    //to try and weed out a lot of potential shenaniganeering.

    public AegisUIInputPacket(boolean requestedActivationState, String entity, long absoluteIndex){
        requestingStateTransition = true;
        requestedState = requestedActivationState;
        this.entityUID = entity;
        blockAbsIndex = absoluteIndex;
        newCycleTime = 0; //TODO: Get rid of this option and prompt user for reset time if first go
        requestingTimeChange = false;
    }

    public AegisUIInputPacket(boolean requestIsForActivation, String entity, long absoluteIndex, long cycleTime){
        requestingStateTransition = true;
        requestedState = requestIsForActivation;
        this.entityUID = entity;
        blockAbsIndex = absoluteIndex;
        requestingTimeChange = true;
        newCycleTime = cycleTime;
    }

    public AegisUIInputPacket(){}

    public AegisUIInputPacket(String entity, long absoluteIndex, long cycleTime) {
        requestingStateTransition = false;
        this.entityUID = entity;
        blockAbsIndex = absoluteIndex;
        newCycleTime = cycleTime;
        requestingTimeChange = true;
    }

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        requestingStateTransition = b.readBoolean();
        entityUID = b.readString();
        blockAbsIndex = b.readLong();
        requestedState = b.readBoolean();
        requestingTimeChange = b.readBoolean();
        newCycleTime = b.readLong();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeBoolean(requestingStateTransition);
        b.writeString(entityUID);
        b.writeLong(blockAbsIndex);
        b.writeBoolean(requestedState);
        b.writeBoolean(requestingTimeChange);
        b.writeLong(newCycleTime);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        SegmentController sc = GameServerState.instance.getSegmentControllersByName().get(entityUID);
        if(!(sc == null) && validOnServer(sc,playerState)) {
            short coreID = elementEntries.get("Aegis Core").id;
            SegmentPiece block = sc.getSegmentBuffer().getPointUnsave(blockAbsIndex);
            if(block.getType() == coreID) //TODO: && worldPosition(sc,blockAbsIndex).sub(getPlayerWorldPosition(playerState)) <= activationRadiusMax)
            { //we can be reasonably sure that there is no direct tampering going on here
                AegisCore aegisCore = (AegisCore) ((ManagedUsableSegmentController<?>) sc).getManagerContainer().getModMCModule(coreID);
                //validated
                if(requestingStateTransition) aegisCore.processActivationInput(requestedState);
                if (requestingTimeChange) {
                    aegisCore.setNewRefresh(newCycleTime);
                }
                AegisSystemUIPromptPacket pkt = aegisCore.getUIInformationPacket();
                PacketUtil.sendPacket(playerState,pkt); //refresh on client
            }
            else playerState.sendServerMessage(Lng.astr(sc.getRealName() + " does not have an Aegis Core here! Are you tampering with your local block data? You cheeky bugger, you..."), MESSAGE_TYPE_WARNING);
        }
    }

    private boolean validOnServer(SegmentController sc, PlayerState playerState){
        return sc.getSegmentBuffer().getPointUnsave(blockAbsIndex).getType() == elementEntries.get("Aegis Core").id &&
                factionAccessMayBeAllowed(playerState.getFactionId(),sc.getFactionId())  &&
                sc.getSector(new Vector3i()).equals(playerState.getCurrentSector());
    }

    @Override
    public void processPacketOnClient() {/*How did we even get here?*/}
}
