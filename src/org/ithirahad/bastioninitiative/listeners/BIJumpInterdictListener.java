package org.ithirahad.bastioninitiative.listeners;

import api.listener.Listener;
import api.listener.events.systems.InterdictionCheckEvent;
import api.utils.sound.AudioUtils;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLTrap;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

import java.util.HashSet;

import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class BIJumpInterdictListener extends Listener<InterdictionCheckEvent> {
    @Override
    public void onEvent(InterdictionCheckEvent e) {
        HashSet<VirtualAegisSystem> systemsInSector = bsiContainer.getAegisSystemsInSector(e.getSegmentController().getSector(new Vector3i()));

        if(systemsInSector != null) for(VirtualAegisSystem agsystem : systemsInSector) {
            if(!BIUtils.isSelfOrAlly(e.getSegmentController().getFactionId(),agsystem.getFactionID())) {
                VirtualFTLTrap candidate = (VirtualFTLTrap) agsystem.getSubsystem("Aegis Anti FTL");
                if(candidate != null && candidate.isOperating(true)){
                    e.setInterdicted(true);
                    for (PlayerState playerAboard : ((ManagedUsableSegmentController<?>)e.getSegmentController()).getAttachedPlayers()) {
                        //TODO: time-delayed action, sound effect, particle effects, etc
                        playerAboard.sendServerMessage(Lng.astr("WARNING: Jump field suppressed by warp interceptor. Unable to engage jump drive."), MESSAGE_TYPE_INFO);
                    }
                    AudioUtils.serverPlaySound("0022_gameplay - low armor _electric_ warning chime", 1.0f, 1.0f, ((ManagedUsableSegmentController<?>)e.getSegmentController()).getAttachedPlayers());
                }
            }
        }
    }
}
