package org.ithirahad.bastioninitiative.vfx.particle;

import api.utils.particle.ModParticle;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.lwjgl.util.vector.Quaternion;

import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import static org.schema.schine.graphicsengine.core.Controller.getCamera;

/**
A simple vertical line particle similar to various Star Trek Online effects. Should have slightly randomized speed, rising straight upwards or downwards,
 and fade in and out of transparency.
 */
public class VerticalSlideLineParticle extends ModParticle {
    final Vector3f vecNoY = new Vector3f();
    private final Vector3f tmpCamera = new Vector3f();
    public VerticalSlideLineParticle(Vector3f position) {
        sizeY = 15f; // long bois
        sizeX = 5f;
        /*
        normalOverride = new Matrix3f();
        normalOverride.setColumn(1, 1, 1, 1); //up?
        normalOverride.setColumn(2,0,0,0); //idk at this point
        /*
        super();
        //TODO: Rotation matrix maths X_x
        // We basically need to get the thing to point at the camera horizontally, but upward vertically
        normalOverride = universalNrmOverride;
         */
    }

    @Override
    public void update(long l) {
        super.update(l);
        rotation.set(0,0,0,1);
        vecNoY.set(getCamera().getRight()); //Should be parallel to camera, not relative pos in any case
        //vecNoY.sub(position); //backwards? idk
        vecNoY.y = 0; //only want xz plane offset; should still point straight vertical as if we were on-plane with the station
        vecNoY.z = 0; //yz zero makes vertical but locks both orientation axes... and y makes it sideways... ???????
        vecNoY.normalize();
        setArbitraryNormalOverride(vecNoY);
    }
}
