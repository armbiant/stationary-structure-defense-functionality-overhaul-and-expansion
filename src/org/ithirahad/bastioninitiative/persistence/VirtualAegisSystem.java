package org.ithirahad.bastioninitiative.persistence;

import api.common.GameCommon;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.BIConfiguration;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;

import javax.vecmath.Vector4f;
import java.io.Serializable;
import java.util.*;

import static java.lang.Math.max;
import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.*;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_DAYS;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.*;

/**
 * Persistent class which tracks the overall operational status, disruption status, and Aegis Charge consumption of an aegis system.
 */
public class VirtualAegisSystem implements Serializable {
    private static final double FUEL_EPSILON = 0.00001; //functional zero for fuel. tiny time steps seem to result in some weird Zeno shenanigans; this counters that.
    public final String uid;
    private final Vector3i location;
    private int faction;

    private int chargeConsPerDay;
    private int chargeConsToOnline;

    public enum AegisSystemStatus {
        ACTIVE("Active","ALL SYSTEMS NOMINAL",0.9,0.9,1), //System online
        ACTIVE_HACKED("Active <Disrupted>","WARNING: SYSTEM IS DISRUPTED!",1,0.5,0.1), //System was disrupted during current cycle; will go vulnerable
        VULNERABLE("Rebooting","AEGIS SYSTEM OFFLINE DUE TO DISRUPTION. REBOOTING...",1,0.1,0.1), //Cycle passed after disruption
        ONLINING("Awaiting Activation Charge","INSUFFICIENT AEGIS CHARGE TO ACTIVATE SYSTEM. ADD MORE AEGIS CELLS TO A LINKED STORAGE TO CHARGE THE AEGIS CORE.",1,1,0.1), //System activated but awaiting sufficient charge to go online
        NO_CHARGE("Insufficient Charge", "AEGIS SYSTEM OFFLINE: AEGIS CHARGE DEPLETED. ADD MORE AEGIS CELLS TO A LINKED STORAGE TO CHARGE THE CORE AND REACTIVATE THE SYSTEM.",1,0.1,0.1), //System was active, but did not have charge
        DISABLED("Inactive", "AEGIS SYSTEM IS NOT ENABLED. PRESS THE BUTTON BELOW TO ACTIVATE.", 1, 0.2, 0.2),
        ERROR_STALE("[ERROR] Ghost System","[MOD ERROR] GHOST SYSTEM; SHOULD NOT BE ACCESSIBLE.",1,0,1);

        private final String niceName;
        private final String promptInfo;
        private final Vector4f promptColor;

        AegisSystemStatus(String niceName, String longInfo, double r, double g, double b){
            this.niceName = niceName;
            this.promptInfo = longInfo;
            this.promptColor = new Vector4f((float)r,(float)g,(float)b,1);
        }

        public String getNiceName() {
            return niceName;
        }

        public String getInfoLong() {
            return promptInfo;
        }

        public Vector4f getPromptColor(){
            return new Vector4f(promptColor);
        }
    }

    private AegisSystemStatus status = DISABLED;
    private double aegisCharge = 0; //TODO: backup list for this so random issues don't cause people to lose ungodly amounts of fuel

    transient private long lastUpdate;
    private long lastDisrupt = 0;
    private long mostCurrentVulnWindowStart;
    public int disruptionDurationMS = DISRUPTION_VULNERABILITY_WINDOW_DURATION_BASE; //how long (in MS) to wait before reactivating if disrupted
    private long recycleTime = 79200000; //time in GMT (in MS) when the system recycles.
    // Defaults to 10:00 PM GMT/6:00 PM EST, i.e. compromise transatlantic time

    //TODO: Longs as time is nice, but we have to be careful about solar years and such for long-term.
    //easiest way is to start with a Calendar-based date 00:00:00 and do our MS from there.

    //TODO: replace all of these weird time comparisons with a countdown timer object of some sort, where applicable

    public long newRecycleTime; //new cycle time
    private boolean queuedForChange; //supposed to be authoritative; validate BEFORE setting this
    public int changeCycleCounter = 0; //after this many cycles, allow a cycle time change (currentRecycleTime = new Time(nextRecycleTime))
    public long lastCycleTimeChange;
    public long prevRecycleTime = 0;

    transient private HashMap<String, VirtualAegisSubsystem> subsystems = new HashMap<>(); //full of subclass objects; GSON does not like this
    private Collection<VirtualAegisSubsystemCrate> persistencePack = new ArrayList<>();

    public VirtualAegisSystem(SegmentController segmentController) {
        uid = DatabaseEntry.removePrefix(segmentController.getUniqueIdentifier());
        location = segmentController.getSector(new Vector3i());
        faction = segmentController.getFactionId();
        lastUpdate = now();
        if(GameCommon.isClientConnectedToServer()) throw new RuntimeException(){
            @Override
            public String getMessage() {
                return "[MOD][BastionInitiative] Attempted to instantiate server-based virtual object on dedicated client!" + super.getMessage();
            }
        };
    }

    public void beforeWriteToPersistence(){
        if(persistencePack == null) persistencePack = new ArrayList<>(); //no idea how this can happen, but it did, so... defensive programming yay
        else persistencePack.clear();

        for(String key : subsystems.keySet()){
            VirtualAegisSubsystem ssys = subsystems.get(key);
            if(ssys != null) persistencePack.add(new VirtualAegisSubsystemCrate(key,ssys));
        }
        //TODO: Since the persistence packs are unreadable gobbledygook, we must eventually write a log listing all the subsystems' relevant properties for debugging.
    }

    public void onLoadFromPersistence(){
        rollLastUpdateToNow(); //don't consume fuel for time offline
        subsystems = new HashMap<>();
        for(VirtualAegisSubsystemCrate pak : persistencePack){
            try {
                pak.retrieveTo(subsystems);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                System.err.println("[MOD][BastionInitiative][ERROR] Unable to retrieve persistent aegis subsystem. Structure will need to be loaded in order to reestablish unloaded persistence.");
                e.printStackTrace();
            }
        }

        for(VirtualAegisSubsystem s : subsystems.values()){
            s.setParent(this); //double-sided links probably wouldn't survive serialization, so we just set it transient and reconnect it here when the subsystems are unrolled
            s.onLoadFromPersistence();
        }
    }

    public VirtualAegisSubsystem getSubsystem(String name){
        return (VirtualAegisSubsystem) subsystems.get(name);
    }

    public boolean hasSubsystem(String name) {
        if(subsystems == null){
            subsystems = new HashMap<>();
            if(!persistencePack.isEmpty()) onLoadFromPersistence();
            System.err.println("[MOD][BastionInitiative][ERROR] Subsystem map was null on " + uid + "!!!");
        }
        return name != null && subsystems.containsKey(name);
    }

    public void putSubsystem(String elementEntriesName, VirtualAegisSubsystem subsystem){
        subsystems.put(elementEntriesName,subsystem);
        subsystem.setParent(this);
    }

    public void removeSubsystem(String elementEntriesName) {
        subsystems.remove(elementEntriesName);
    }

    public void DEBUG_clearDisruptInfo() {
        lastDisrupt = 0;
        mostCurrentVulnWindowStart = 0;
    }

    public void changeStatus(AegisSystemStatus v) {
        status = v;
        update();
    }

    public Collection<VirtualAegisSubsystem> getSubsystems() {
        return subsystems.values();
    }

    public AegisSystemStatus getStatus(){
        return status;
    }

    public boolean isDisrupted() {
        return status == ACTIVE_HACKED || status == VULNERABLE;
    }

    public void giveCharge(int val){
        if(val < 0) {
            System.err.println("[MOD][BastionInitiative][WARNING] Attempted to give negative amounts of charge to an Aegis Nexus. What happened here?");
        }
        aegisCharge += val;
    }

    public void update() {
        if(queuedForChange && isTimeChangeAllowed()){
            lastCycleTimeChange = getTimeChangeAllowedThreshold(); //we 'pretend' that it changed as soon as it was allowed to, rather than when the update executed
            setRecycleTime(newRecycleTime);
            queuedForChange = false;
            newRecycleTime = -1;
        }

        //---------------------------------------------------------------------------------
        AegisSystemStatus originalStatus = status;
        boolean consumeCharge = false;
        double originalFuel = aegisCharge;
        int loops = 0;
    //int chargePerCycle = calcAegisChargePerCycle(); //TODO do this in MMCM
        long now = now();
        long zeroToday = getLastMidnight();
        long clockTimeMs = now - zeroToday; //time since 0000 GMT today
        // Java rounds down, so the remainder of a day partially elapsed before the previous update shouldn't prompt extra fuel usage.

        long msSincePrevUpdate = now - lastUpdate; //time since last update()
        long postCycleTime = max(0L,clockTimeMs - recycleTime); //amount of time that has passed since the most recent recycle time
        int cyclesCompletedSinceUpdate = (int) (max(0,msSincePrevUpdate-postCycleTime) / MS_TO_DAYS); //Number of days from last update to last cycle turnover.
        if(postCycleTime < msSincePrevUpdate) cyclesCompletedSinceUpdate++;

        if(status == ONLINING) {
            if (aegisCharge >= chargeConsToOnline) {
                //if we have enough to online, then we (at some point) went online. Next state can handle real-time fuel usage, whatever it is.
                aegisCharge -= chargeConsToOnline;
                status = ACTIVE;
                //this has the useful side effect of cancelling out any "completed" cycles while onlining - it's not actually active, so it shouldn't consume any aegis yet other than the activation cost
            }
        }
        if(status == NO_CHARGE) {
            if (tryConsumeChargeToRun(msSincePrevUpdate)) {
                status = ACTIVE;
                lastUpdate = now();
            }
        }
        if(status == ACTIVE || status == ACTIVE_HACKED) {
            if (now < getCurrentVulnerabilityStart()) {
                status = ACTIVE_HACKED; //let that state take care of fuel consumption next iteration
            } else if (now < getCurrentVulnerabilityEnd()) status = VULNERABLE;
            else status = ACTIVE;
            consumeCharge = true;
        }
        if(status == VULNERABLE) {
            if (now > getCurrentVulnerabilityEnd()) {
                status = ACTIVE;
                //unless a later state behaviour says otherwise, we've been active since the window cleared.
            }
            consumeCharge = true;
        }
        if(status == DISABLED || status == ERROR_STALE) {
            //do nothing for now
        }

        if(consumeCharge){
            boolean charged = tryConsumeChargeToRun(msSincePrevUpdate);
            if(!(charged || status == VULNERABLE)){ //if it is vulnerable, we'll switch later when it comes out of vuln
                status = NO_CHARGE;
            }
        }

        if(originalStatus != NO_CHARGE && status == NO_CHARGE){
            sendNoFuelNotification();
        }else {
            if(getMaxChargeCapacity() > 0) {
                int lowFuelLevel = (int) (getChargeConsPerDay() * LOW_FUEL_ALERT_FACTOR);
                if (originalFuel >= lowFuelLevel && aegisCharge < lowFuelLevel) {
                    sendLowFuelNotification();
                }
            }
        }

        //TODO: Update consumption rate in receiver registry.
        lastUpdate = now;
    }

    private void sendNoFuelNotification() {

    }

    private void sendLowFuelNotification() {

    }

    public int getMaxChargeCapacity() {
        return AEGIS_CORE_CELL_CAPACITY; //for now. this might eventually be a variable
    }

    /**
     * Consume an amount of fuel corresponding to the amount of time that passed since last update.
     * @param ms Amount of time worth of fuel to try consuming
     * @return whether sufficient fuel was present
     */
    private boolean tryConsumeChargeToRun(long ms) {
        if(aegisCharge < FUEL_EPSILON) return false;
        double amount = (((double)ms)/MS_TO_DAYS) * getChargeConsPerDay(); //per cycle means per day basically, so we need the fraction of a day
        //TODO: transmitter changes this. should be (amount > getAvailableCharge()) if transmitter is passive.
        // else if enough remote charge use that; else use remote then local.
        if(amount > aegisCharge){
            aegisCharge = 0;
            return false;
        }
        else{
            aegisCharge -= amount;
            return true;
        }
    }

    public void setOrQueueNewRecycleTime(long t) {
        if (t != recycleTime) {
            if (isTimeChangeAllowed()) { //TODO UI warn that new time will queue if disrupted
                recycleTime = t; //we're past the cooldown, so just set the cycle time directly
                lastCycleTimeChange = now();
                queuedForChange = false;
                newRecycleTime = -1;
            } else {
                newRecycleTime = t;
                queuedForChange = true;
            }
        } else {
            queuedForChange = false; //user keyed in the current recycle time; nothing happens here
            newRecycleTime = -1;
        }
    }

    public void tryDisrupt() {
        if(((now() - lastDisrupt)/MS_TO_DAYS) > DISRUPTION_SAFETY_DAYS) {
            lastDisrupt = now(); //variable not used for anything atm
            long timeToCycle = getNextCycleTime().getTimeInMillis() - now(); //this is a bug, but should behave consistently. need to fix though as it doens't cover all cases

            mostCurrentVulnWindowStart = getNextCycleTime().getTimeInMillis(); //set vulnerability start
            if(timeToCycle > 0 && timeToCycle < MINIMUM_DISRUPTION_PREPARATION_TIME_MS) mostCurrentVulnWindowStart += MS_TO_DAYS; //roll forward one day if we're too close to the cycle time
            update();
        }
    }

    public void handleDestroyed() {
        if(status == VULNERABLE){
            //TODO: if system has hub, update hub's most recent energy backlash time to now.
            // (that way the hub can't just start linking to new systems again instantly)
        }

        bsiContainer.removeAegisSystem(this);
        changeStatus(ERROR_STALE); //if it persists, we can see that it's a bug

        for (Iterator<String> keys = subsystems.keySet().iterator(); keys.hasNext(); ) {
            String s = keys.next();
            subsystems.get(s).onDestroy();
            keys.remove();
        }
    }

    public int getChargeConsToPutOnline() {
        return chargeConsToOnline;
    }

    public void setChargeConsRequiredToOnLine(int chargeConsToOnline) {
        this.chargeConsToOnline = chargeConsToOnline;
    }

    public double getAegisCharge() {
        return aegisCharge;
    }

    public void setCharge(int i) {
        aegisCharge = i;
    }

    public Vector3i getSectorLocation() {
        return new Vector3i(location);
    }

    @Override
    public int hashCode() {
        return ("CONS" + uid).hashCode();
    }

    public int getChargeConsPerDay(){
        return chargeConsPerDay;
    }

    public void setChargeConsPerDay(int cost) {
        chargeConsPerDay = cost;
    }

    private void setRecycleTime(long t) {
        recycleTime = t;
    }

    public int getFactionID() {
        return faction;
    }

    public void setFaction(int faction) {
        this.faction = faction;
    }

    public Calendar getNextCycleTime() {
        Calendar result = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.ENGLISH);
        result.setTimeInMillis(getLastMidnight());
        result.add(Calendar.MILLISECOND,(int)recycleTime);
        if(currentTimeOfDay() > recycleTime) result.roll(Calendar.DATE,1);
        if(queuedForChange){
            long timeUntilCycleTimeChanges = getTimeChangeAllowedThreshold() - now();
            long timeUntilNextCycle = result.getTimeInMillis();
            if(timeUntilCycleTimeChanges < timeUntilNextCycle){
                result.setTimeInMillis(getLastMidnight());
                result.add(Calendar.MILLISECOND,(int)newRecycleTime);
                if(currentTimeOfDay() > newRecycleTime) result.roll(Calendar.DATE,1);
            }
        } //TODO: optimize/debug?

        return result;
    }

    public long getCurrentVulnerabilityStart() {
        return mostCurrentVulnWindowStart;
    }

    public long getCurrentVulnerabilityEnd(){
        return mostCurrentVulnWindowStart + getRecycleDuration();
    }

    public long getTimeChangeCooldownRemaining() {
        return getTimeChangeAllowedThreshold() - now();
    }

    /**
     * @return the time after which the system will no longer have enough Aegis Charge to function. Among other things, this is useful for determining whether or not a system should act when unloaded, e.g. as a FTL inhibitor.
     */
    public long getFuelledUntil(){
        double chargeConsPerMs = ((double)chargeConsPerDay)/MS_TO_DAYS;
        return lastUpdate + (long) Math.floor(aegisCharge/chargeConsPerMs);
        //TODO: should this be cached?
    }

    /**
     * @return whether or not the system is providing Aegis Charge to its subsystems.
     * This will be true while the system is fuelled and not in a vulnerable or offline state.
     */
    public boolean isActive() {
        return (status == ACTIVE || status == ACTIVE_HACKED);
    }

    /**
     * @return whether or not the system is in an "activated" state. This includes vulnerable and out-of-fuel status, but does not include the true offline state or "expired" error state.
     */
    public boolean isActivatedState(){
        return !(status == DISABLED || status == ERROR_STALE);
    }

    public void activate(){
        if(status == DISABLED || status == ERROR_STALE) {
            status = ONLINING;
            update();
        }
    }

    //allows time skips e.g. when game is offline and turns back on again
    public void rollLastUpdateToNow() {
        lastUpdate = now();
    }

    @NotImplemented
    public boolean hasBenefactorNexus(){
        return false;
    }

    public boolean isNexusOnlining(){return status == ONLINING;}

    public long getLastUpdate(){
        return lastUpdate;
    }

    public long getLastCycleTimeChange() {
        return lastCycleTimeChange;
    }

    public long getLastDisrupt() {
        return lastDisrupt;
    }

    public long getRecycleTime() {
        return recycleTime;
    }

    public long getRecycleDuration() {
        return DISRUPTION_VULNERABILITY_WINDOW_DURATION_BASE; //todo: mass based or something?
    }


    public boolean isQueuedForChange() {
        return queuedForChange;
    }

    public long recycleTimeAfterChange() {
        return newRecycleTime;
    }

    public boolean isTimeChangeAllowed(){
        return !isDisrupted() && //tl;dr you can't get cucked out of a siege by a defender initiating a timely time change
                now() > getTimeChangeAllowedThreshold();
    }

    /**
    @return After what time the system will be allowed to change its recycle times
     */
    public long getTimeChangeAllowedThreshold(){
        return lastCycleTimeChange + ((long) BIConfiguration.AEGIS_RECYCLE_TIME_CHANGE_COOLDOWN_DAYS *MS_TO_DAYS);
    }

    public long getRemainingCyclesUntilDischarged(){
        update();
        if(aegisCharge > 0) return Math.round(Math.floor(aegisCharge / chargeConsPerDay));
        return 0;
    }

    public static class VirtualAegisSubsystemCrate implements Serializable {
        //lol
        //lmao
        //...Aiiiye, we actually are doing this. Aiya aelen syai se.
        transient private Class<? extends VirtualAegisSubsystem> cls;
        private byte[] object;
        private String label;
        private String className;

        VirtualAegisSubsystemCrate(String key, VirtualAegisSubsystem ssys){
            label = key;
            cls = ssys.getClass();
            object = SerializationUtils.serialize(cls.cast(ssys));
            className = cls.getName();
        }

        VirtualAegisSubsystemCrate(){};

        @SuppressWarnings("unchecked")
        void beforeUnpack() throws ClassNotFoundException, ClassCastException {
            cls = (Class<? extends VirtualAegisSubsystem>) Class.forName(className);
        }

        Class<? extends VirtualAegisSubsystem> getContainedClass(){
            return cls;
        }

        void retrieveTo(HashMap<String, VirtualAegisSubsystem> target) throws ClassNotFoundException {
            beforeUnpack();
            VirtualAegisSubsystem unpacked = cls.cast(SerializationUtils.deserialize(object));
            target.put(label,unpacked);
        }
    }

    public static class AegisSystemStats {
        private String subsystemInfoText;
        private long recalibrationStart;
        private long recalibrationDuration;
        private long latestDisruptStart;
        private boolean isDisrupted;
        private long timeChangeCooldownRemaining;
        private long queuedTimeChange; //-1 if none
        private int aegisSystemActivationCost;
        private int aegisSystemDailyCost;
        private double aegisChargeStored;
        private int aegisChargeCapacity;
        private long fuelledUntil;
        private boolean activated;
        private boolean awaitingActivationCharge;
        private long coreLocationAbsIndex;
        private String uid;

        public AegisSystemStats(String uid, long coreAbsPos, boolean activated, boolean awaitingActivationCharge, String subsystemInfoText, long recalibrationStart, long recalibrationDuration, boolean isDisrupted, long timeChangeCooldownRemaining, long queuedTimeChange, int aegisSystemActivationCost, int aegisSystemDailyCost, double aegisChargeStored, int aegisChargeCapacity, long fuelledUntil, long latestDisruptStart) {
            this.coreLocationAbsIndex = coreAbsPos;
            this.uid = uid;
            this.subsystemInfoText = subsystemInfoText;
            this.recalibrationStart = recalibrationStart;
            this.recalibrationDuration = recalibrationDuration;
            this.isDisrupted = isDisrupted;
            this.timeChangeCooldownRemaining = timeChangeCooldownRemaining;
            this.queuedTimeChange = queuedTimeChange;
            this.aegisSystemActivationCost = aegisSystemActivationCost;
            this.aegisSystemDailyCost = aegisSystemDailyCost;
            this.aegisChargeStored = aegisChargeStored;
            this.aegisChargeCapacity = aegisChargeCapacity;
            this.fuelledUntil = fuelledUntil;
            this.activated = activated;
            this.awaitingActivationCharge = awaitingActivationCharge;
            this.latestDisruptStart = latestDisruptStart;
        }

        public String getSubsystemInfoText() {
            return subsystemInfoText;
        }

        public void setSubsystemInfoText(String subsystemInfoText) {
            this.subsystemInfoText = subsystemInfoText;
        }

        public long getRecalibrationStart() {
            return recalibrationStart;
        }

        public void setRecalibrationStart(long recalibrationStart) {
            this.recalibrationStart = recalibrationStart;
        }

        public long getRecalibrationDuration() {
            return recalibrationDuration;
        }

        public void setRecalibrationDuration(long recalibrationDuration) {
            this.recalibrationDuration = recalibrationDuration;
        }

        public boolean isDisrupted() {
            return isDisrupted;
        }

        public void setDisrupted(boolean disrupted) {
            isDisrupted = disrupted;
        }

        public long getTimeChangeCooldownRemaining() {
            return timeChangeCooldownRemaining;
        }

        public void setTimeChangeCooldownRemaining(long timeChangeCooldownRemaining) {
            this.timeChangeCooldownRemaining = timeChangeCooldownRemaining;
        }

        public long getQueuedTimeChange() {
            return queuedTimeChange;
        }

        public void setQueuedTimeChange(long queuedTimeChange) {
            this.queuedTimeChange = queuedTimeChange;
        }

        public int getAegisSystemActivationCost() {
            return aegisSystemActivationCost;
        }

        public void setAegisSystemActivationCost(int aegisSystemActivationCost) {
            this.aegisSystemActivationCost = aegisSystemActivationCost;
        }

        public int getAegisSystemDailyCost() {
            return aegisSystemDailyCost;
        }

        public void setAegisSystemDailyCost(int aegisSystemDailyCost) {
            this.aegisSystemDailyCost = aegisSystemDailyCost;
        }

        public double getAegisChargeStored() {
            return aegisChargeStored;
        }

        public void setAegisChargeStored(double aegisChargeStored) {
            this.aegisChargeStored = aegisChargeStored;
        }

        public int getAegisChargeCapacity() {
            return aegisChargeCapacity;
        }

        public void setAegisChargeCapacity(int aegisChargeCapacity) {
            this.aegisChargeCapacity = aegisChargeCapacity;
        }

        public long getFuelledUntil() {
            return fuelledUntil;
        }

        public void setFuelledUntil(long fuelledUntil) {
            this.fuelledUntil = fuelledUntil;
        }

        public boolean isActivated() {
            return this.activated;
        }

        public void setActivated(boolean v){
            this.activated = v;
        }

        public boolean isAwaitingActivationCharge() {
            return awaitingActivationCharge;
        }

        public String getUID() {
            return uid;
        }

        public long getCoreLocationAbsIndex() {
            return coreLocationAbsIndex;
        }

        public long getLatestDisruptStart() {
            return latestDisruptStart;
        }

        public void setLatestDisruptStart(long latestDisruptStart) {
            this.latestDisruptStart = latestDisruptStart;
        }
    }
}
