package org.ithirahad.bastioninitiative.gui.old;

import api.network.packets.PacketUtil;
import api.utils.gui.GUIInputDialog;
import org.ithirahad.bastioninitiative.network.AegisUIInputPacket;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

import java.util.List;

public class AegisSystemSimpleInput extends GUIInputDialog {
    String activationStateString = "<DO SOMETHING TO>";
    AegisSystemSimpleDialog dialogPanel;

    long lastTargetBlock = 0;
    String lastTargetEntity = "";
    boolean lastStatus = false;

    public AegisSystemSimpleInput() {
        super();
    }

    @Override
    public AegisSystemSimpleDialog createPanel() {
        dialogPanel = new AegisSystemSimpleDialog(getState(), this);
        return dialogPanel;
    }

    public void activate(List<String> infos, boolean lastStatus, String entityUID, long uid) { //why is it a list?? uhh, long story.
        populateLatestInfo(infos);
        this.lastStatus = lastStatus;
        lastTargetEntity = entityUID;
        lastTargetBlock = uid;
        super.activate();
    }

    private void populateLatestInfo(List<String> content) {
        dialogPanel.setOkButtonText("YES");
        dialogPanel.setCancelButtonText("NO");

        StringBuilder wrappedContent = new StringBuilder();
        for(String line : content) wrappedContent.append(line).append("\r\n");
        dialogPanel.infoOverlayUpper.setTextSimple(Lng.str(wrappedContent.toString()));
    }

    @Override
    public void callback(GUIElement callingElement, MouseEvent mouseEvent) {
        if(!isOccluded() && mouseEvent.pressedLeftMouse()) {
            //User pointer is typically the displayed label / text on the button
            switch((String) callingElement.getUserPointer()) {
                case "X":
                case "CANCEL":
                    //Deactivate dialog
                    deactivate();
                    break;
                case "OK":
                case "YES":
                    AegisUIInputPacket pk = new AegisUIInputPacket(!lastStatus, lastTargetEntity, lastTargetBlock);
                    PacketUtil.sendPacketToServer(pk);
                    deactivate();
                    break;
            }
        }
    }
}

