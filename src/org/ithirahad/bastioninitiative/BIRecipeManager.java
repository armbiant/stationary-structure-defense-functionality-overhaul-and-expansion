package org.ithirahad.bastioninitiative;

import api.config.BlockConfig;
import org.ithirahad.resourcesresourced.RRSElementInfoManager;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.element.FixedRecipeProduct;

import java.util.ArrayList;

import static org.ithirahad.bastioninitiative.BIConfiguration.CENTRIFUGE_BASE_REFINE_RATIO;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.industry.RRSRecipeManager.*;

public class BIRecipeManager {
    public static final int CENTRIFUGE_REFINE_TIME = 50000;
    public static final int CENTRIFUGE_YIELD = 1;
    public static final int RAW_RESOURCE_COMPENSATION_BONUS_FACTOR = 2;

    private static final ArrayList<FixedRecipeProduct> centrifugeProducts = new ArrayList<>();
    public static FixedRecipe centrifugeRecipes = new FixedRecipe(){
        {
            name = "Castellium Isolation";
            recipeProducts = new FixedRecipeProduct[]{};
        }

        @Override
        public float getBakeTime(){
            return CENTRIFUGE_REFINE_TIME;
        }

    };

    public static void addRecipes() {
        addCentrifugeRefine(fr(CENTRIFUGE_BASE_REFINE_RATIO,"Anbaric Vapor"), fr(CENTRIFUGE_YIELD * RAW_RESOURCE_COMPENSATION_BONUS_FACTOR,"Castellium Condensate"));
        addCentrifugeRefine(fr(CENTRIFUGE_BASE_REFINE_RATIO * RRSElementInfoManager.BASE_GAS_CAPSULE_CONVERSION_FACTOR,"Anbaric Capsule"), fr(CENTRIFUGE_YIELD,"Castellium Condensate"));
        addCentrifugeRefine(fr(CENTRIFUGE_BASE_REFINE_RATIO,"Parsyne Plasma"), fr(CENTRIFUGE_YIELD * RAW_RESOURCE_COMPENSATION_BONUS_FACTOR,"Castellium Condensate"));
        addCentrifugeRefine(fr(CENTRIFUGE_BASE_REFINE_RATIO * RRSElementInfoManager.BASE_GAS_CAPSULE_CONVERSION_FACTOR,"Parsyne Capsule"), fr(CENTRIFUGE_YIELD,"Castellium Condensate"));
        addCentrifugeRefine(fr(CENTRIFUGE_BASE_REFINE_RATIO,"Thermyn Amalgam"), fr(CENTRIFUGE_YIELD * RAW_RESOURCE_COMPENSATION_BONUS_FACTOR,"Castellium Condensate"));
        addCentrifugeRefine(fr(CENTRIFUGE_BASE_REFINE_RATIO * RRSElementInfoManager.BASE_LIQUID_CAPSULE_CONVERSION_FACTOR,"Thermyn Capsule"), fr(CENTRIFUGE_YIELD,"Castellium Condensate"));

        centrifugeRecipes.recipeProducts = centrifugeProducts.toArray(new FixedRecipeProduct[0]);

        addRefine(fr(1,"Castellium Condensate"),fr(5, "Castellium Capsule"));

        elementEntries.get("Component: Field-Stabilized Canister").type = ElementKeyMap.getCategoryHirarchy().find("Components");
        addComponent("Field-Stabilized Canister", 3, fr(rareAmountSmall, "Macetine Capsule"), fr(3, "Ferron Capsule"), fr(1, "Parsyne Capsule"));

        addBlock("Aegis Cell", comp(1, "Field-Stabilized Canister"), comp(2,"Energy Cell"), fr(10,"Castellium Capsule"));

        //TODO: consider spent aegis cell casing refurbishment w/ discount

        addBlock("Castellium Centrifuge", comp(2, "Metal Frame"), comp(1, "Metal Sheet"), comp(1, "Standard Circuitry"));

        addBlock("Aegis Core", comp(1, "Metal Frame"), comp(10, "Prismatic Circuit"), comp(10, "Parsyne Holographic Processor"), comp(10,"Field-Stabilized Canister"));
        addBlock("Aegis Enhancer", comp(1, "Metal Frame"), comp(10, "Ferron Resonant Circuitry"), comp(1, "Parsyne Amplifying Focus"), comp(1,"Field-Stabilized Canister"));
        addBlock("Aegis Anti Stealth", comp(1, "Metal Frame"), comp(10, "Ferron Resonant Circuitry"), comp(1, "Aegium Field Emitter"), comp(1,"Field-Stabilized Canister"));
        addBlock("Aegis Invulnerability", comp(1, "Metal Frame"), comp(10, "Anbaric Distortion Coil"), comp(1, "Aegium Field Emitter"), comp(1,"Field-Stabilized Canister"));
        addBlock("Aegis Anti FTL",comp(1,"Metal Frame"),comp(10,"Anbaric Distortion Coil"),comp(1,"Ferron Resonant Circuitry"), comp(1,"Field-Stabilized Canister"));
        //addBlock("Aegis Transmitter", comp(1, "Metal Frame"), comp(10, "Aegium Field Emitter"), comp(1, "Ferron Resonant Circuitry"), comp(1, "Crystal Energy Focus"), comp(1,"Field-Stabilized Canister"));

        addBlock("Hacking Computer", comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(10, "Ferron Resonant Circuitry"), comp(10, "Parsyne Amplifying Focus"), comp(1,"Field-Stabilized Canister"));
        addBlock("Hacking Module", comp(1, "Metal Frame"), comp(5, "Ferron Resonant Circuitry"), comp(1, "Parsyne Amplifying Focus"));
    }

    public static void addCentrifugeRefine(FactoryResource input, FactoryResource... outputs) {
        FixedRecipeProduct product = new FixedRecipeProduct();
        product.input = new FactoryResource[]{input};
        product.output = outputs;
        centrifugeProducts.add(product);
        for(FactoryResource output : outputs) ElementKeyMap.getInfo(output.type).setFactoryBakeTime(CENTRIFUGE_REFINE_TIME);
        BlockConfig.addRefineryRecipe(centrifugeRecipes, new FactoryResource[]{input}, outputs);

        for(FactoryResource r : outputs){
            short t = r.type;
            hasRRSRecipe[t] = true;
            hasComponentRecipe[t] = true; //idfk, I don't think this is actually used
        }
    }
}
