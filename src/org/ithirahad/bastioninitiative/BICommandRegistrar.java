package org.ithirahad.bastioninitiative;

import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.CommandInterface;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.ithirahad.bastioninitiative.vfx.particle.SuccessfulDisruptParticleEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import javax.annotation.Nullable;
import javax.vecmath.Vector3f;

import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.modInstance;

public class BICommandRegistrar {
    public static void registerCommands(){
        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "bsi_debug";
            }

            @Override
            public String[] getAliases() {
                return new String[]{"bsid","bi_debug","bsdebug"};
            }

            @Override
            public String getDescription() {
                return "Bastion Initiative debug command.\r\n" +
                        "/bsi_debug ddnotify: Sends an Aegis System disruption notification to the owner of the target structure. (Don't troll with this, please.)\r\n" +
                        "/bsi_debug status: Sends info about Aegis System status of the target structure.\r\n"+
                        "/bsi_debug refuel: Refuels a targeted entity's Aegis System with the specified amount of fuel.\r\n"+
                        "/bsi_debug ddclear: Clears disruption status on a target entity.";
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState sender, String[] args) {
                try {
                    SegmentController sc;
                    Sendable sendable = GameServerState.instance.getLocalAndRemoteObjectContainer().getLocalObjects().get(sender.getSelectedEntityId());
                    if(sendable instanceof SegmentController){
                        sc = ((SegmentController) sendable);
                        if (sc instanceof ManagedUsableSegmentController && !(sc instanceof Ship)) {
                            AegisCore coreSys = (AegisCore)(((ManagedUsableSegmentController<?>)sc).getManagerContainer().getModModuleMap().get(elementEntries.get("Aegis Core").id));
                            if(coreSys != null && args[0] != null){
                                String cmd = args[0];
                                if(cmd.equals("ddnotify")) coreSys.sendFactionsDisruptionNotification();
                                else if(cmd.equals("ddclear")) coreSys.DEBUG_clearDisrupt();
                                else if(cmd.equals("status")) PlayerUtils.sendMessage(sender,coreSys.getStatusString());
                                else if(cmd.equals("refuel")) coreSys.giveCharge(Integer.parseInt(args[1]));
                                else if(cmd.equals("ddforce")) {
                                    coreSys.setLastHitter(sender.getFactionId());
                                    coreSys.doDisruptionDamage((float) (coreSys.getCurrentHP() + 1.0f));
                                }
                                else if(cmd.equals("ddfx")){
                                    SuccessfulDisruptParticleEffect.FireEffectServer(sc.getSectorId(), sc.getSector(new Vector3i()), sc.getWorldTransform().origin,new Vector3f(/*Zero velocity*/), (int) Math.max(Math.max(sc.getBoundingBox().sizeX(), sc.getBoundingBox().sizeY()), sc.getBoundingBox().sizeZ()));
                                }
                            }
                        }
                    }
                    return true;
                } catch (Exception e) {
                    PlayerUtils.sendMessage(sender,"[ERROR]: " + e.getClass().getName());
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void serverAction(@Nullable PlayerState playerState, String[] strings) {

            }

            @Override
            public StarMod getMod() {
                return modInstance;
            }
        });
    }
}
