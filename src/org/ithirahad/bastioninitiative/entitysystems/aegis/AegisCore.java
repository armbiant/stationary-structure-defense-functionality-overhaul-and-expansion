package org.ithirahad.bastioninitiative.entitysystems.aegis;

import api.common.GameCommon;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.game.module.ModManagerContainerModule;
import api.utils.sound.AudioUtils;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import org.ithirahad.bastioninitiative.BIElementInfoManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancementCollectionManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancementElementManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancerUnit;
import org.ithirahad.bastioninitiative.network.AegisSystemUIPromptPacket;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus;
import org.ithirahad.bastioninitiative.util.TemporalShortcuts;
import org.ithirahad.bastioninitiative.vfx.particle.SuccessfulDisruptParticleEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.elements.*;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorSet;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import javax.vecmath.Vector3f;
import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Math.abs;
import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.aegisInterference;
import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.*;
import static org.ithirahad.bastioninitiative.util.BIUtils.*;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.getFormattedGMTTimeAndDate;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.getServerSendables;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.modInstance;
import static org.schema.game.common.data.element.ElementKeyMap.STASH_ELEMENT;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT;
import static org.schema.game.common.data.world.planet.texgen.MathUtil.min;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

/**
 * Handles the loaded behaviour of the overall Aegis System and Aegis Core.
 * Feeds the <code>PersistentAegisSystem</code> the relevant information about aegis consumption mechanics, based on what is happening with the physical blocks.
 * <br/><br/>
 * Notably, the representative <code>PersistentAegisSystem</code> should not have to ever pull any information from this class - it should already be provided
 * with all the info it needs at any given time by push.
 */
public class AegisCore extends ModManagerContainerModule implements Serializable {
    public static final String NAME = "Aegis Core";
    private transient boolean loaded = false; //loading process must happen every time
    private transient boolean initialized = false; //initialization must happen every time
    private transient VirtualAegisSystem virtualSystem = null; //ditto; serializing this means duplicates and all kinds of tomfoolery
    private float disruptionHP;
    private float disruptionHPMax = 100f;
    public static float DISRUPTION_REGEN_FRACTION_PER_SECOND = 0.01f;
    public static long DISRUPTION_REGEN_OUTAGE_TIME_MS = 5000; //time between disruption damage and regen restarting
    private long lastDisruptionDamage;
    //TODO: very fuel-intensive regen booster subsystem?
    private transient int collectedCharge = 0; //cache of locally collected Aegis Cells

    public Short2ObjectOpenHashMap<ManagerModuleCollection<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager>> subsystemEnhancerManagerMap = new Short2ObjectOpenHashMap<>();

    private transient long lastColl = 0; //last time that fuel was collected
    private transient long lastInterferenceAlert = 0; //last time the interference alert went out

    private transient final List<SegmentController> tmpList = new LinkedList<>();

    public transient final Short2ObjectOpenHashMap<AegisSubsystem> subsystems = new Short2ObjectOpenHashMap<AegisSubsystem>(){
        @Override
        public AegisSubsystem remove(short k) {
            System.err.println("[MOD][BastionInitiative][WARNING] Removed subsys with id " + k + "from list for some reason.");
            return super.remove(k);
        }
    };
    private static final ShortSet linkableInventoryBlocks = new ShortOpenHashSet();

    private double remotePowerCons = 0;
    private int remoteOnliningCost = 0;
    private double remoteCycleCost = 0;
    private long remoteCycleTime = 0;
    private double remoteCharge = 0;
    private AegisSystemStatus remoteStatus = DISABLED;
    private long remoteDisruptWindowStart = 0; //time of latest set disrupt window

    /**
     * The ID of the faction that last hit the structure with a Disruption Datalink beam.<br/>
     * (Do recall that the Disruption Datalink can only affect the structure if it has a Bastion Field subsystem.)
     */
    private int lastHitter = 0; //will update every time the aegis system gets DD'd
    private long lastServerUpdate = 0;
    private transient AegisSystemStatus queuedStatus = null;

    public AegisCore(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, modInstance, elementEntries.get("Aegis Core").id);
        onLoad();
    }

    private void onLoad(){
        //setBlockSpecialInContainer(getManagerContainer(), getBlockId());
        linkableInventoryBlocks.addAll(ElementKeyMap.getInfo(getBlockId()).getControlling());
        linkableInventoryBlocks.removeAll(BIElementInfoManager.aegisSubsystemControllers); //subsystem cores don't have inventories :P
        loaded = true;
    }

    private String getUID(){
        return DatabaseEntry.removePrefix(segmentController.getUniqueIdentifier());
    }

    private boolean tryInit(){
        //This is a really awkward way of doing things,but it's not always clear when an entity is "mature" enough for this module to get information from it
        //and link up with its virtualized counterpart, so we just keep trying until it is actually ready, basically
        if (segmentController.getSector(new Vector3i()) == null)
            return false; //If the entity doesn't even know where it is, it's not ready for this module to init

        if(isOnServer()) {
            if (bsiContainer.aegisSystemExists(getUID()))
                virtualSystem = bsiContainer.getAegisSystemsByID(getUID());
            else {
                disruptionHP = disruptionHPMax; //this is a slightly spaghettish way of determining whether or not a system is "new" for this purpose... but I think it'll do.
                virtualSystem = new VirtualAegisSystem(this.segmentController);
                bsiContainer.addAegisSystem(virtualSystem);
            }
            lastServerUpdate = now();
        }

        //point subsystem enhancer ECMs, as they rely on core status to determine their status though maybe they don't need to
        for(ManagerModuleCollection<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager> mmc : subsystemEnhancerManagerMap.values()){
            for(AegisEnhancementCollectionManager ecm : mmc.getCollectionManagers()) ecm.setCore(this);
        }

        initialized = true;
        System.err.println("[MOD][BastionInitiative] Initialized Aegis Core system for " + segmentController.getRealName() + " successfully.");
        if(isOnServer()) syncToNearbyClientsSafe(this);
        return true;
    }

    public boolean isInitialized(){
        return initialized;
    }


    @Override
    public void handle(Timer timer) {
        if(subsystems.isEmpty()) populateSubsystemList();
        if(this.blocks != null && !this.blocks.isEmpty()) {
            if (isInitialized() || tryInit()) {
                setMaxHP(getStructurePoints(false) * DISRUPTION_HP_PER_STRUCTURE_POINT);
                if (isOnServer()) handleServerSide(timer);
                else handleClientSide(timer);
            }
        }
    }

    /**
     * Get the number of Structure Points that an entity or planet has. This is intended to be an approximate measure of an entity's value. Active turrets are not counted, as no Aegis System protects them.
     * @param includeOptionalFromDocks
     * @return
     */
    public float getStructurePoints(boolean includeOptionalFromDocks) {
        float val = 0;
        tmpList.clear();
        if(includeOptionalFromDocks) {
            getSelfAndAllDocks(segmentController, tmpList, true);
            for (SegmentController seg : tmpList) {
                if (segmentController instanceof ManagedUsableSegmentController<?> && !(segmentController.isAIControlled() && segmentController.railController.isTurretDocked())) {
                    val += getDockOptionalStructurePoints(seg);
                }
            }
        } else {
            if(segmentController.getType() == PLANET_SEGMENT){
                getAllPlates((Planet)segmentController,tmpList);
            } else tmpList.add(segmentController);
            for(SegmentController seg : tmpList) {
                val += getDockOptionalStructurePoints(seg);
            }
            /*
            tmpList.clear();
            getSelfAndAllDocks(segmentController, tmpList, true);
            for (SegmentController seg : tmpList) if(!(segmentController.isAIControlled() && segmentController.railController.isTurretDocked())) {
                if (segmentController instanceof ManagedUsableSegmentController<?>) {
                    //TODO add/move MANDATORY from docks if present
                }
            }
             */
        }
        return val;
    }

    private float getDockOptionalStructurePoints(SegmentController seg){
        float val = 0;

        ManagedUsableSegmentController<?> msc = (ManagedUsableSegmentController<?>) seg;
        ReactorSet reactors = msc.getManagerContainer().getMainReactor().getPowerInterface().getReactorSet();
        for (ReactorTree reactor : reactors.getTrees()){
            val += reactor.getLevel() * STRUCTURE_POINTS_PER_REACTOR_LEVEL;
            val += reactor.getLevel() * reactor.getChamberCapacity() * STRUCTURE_POINTS_REACTOR_ADDITIONAL_MULT_PER_CHAMBER_UTILIZATION;
        }
        val += msc.getElementClassCountMap().get(ElementKeyMap.SHIELD_REGEN_ID) * STRUCTURE_POINTS_PER_SHIELD_GEN;
        val += msc.getElementClassCountMap().get(ElementKeyMap.SHIELD_CAP_ID) * STRUCTURE_POINTS_PER_SHIELD_CAP;

        val += msc.getElementClassCountMap().get(ElementKeyMap.MISSILE_CAPACITY_MODULE) * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK;
        val += msc.getManagerContainer().getModulesMap().get((short) 32).getElementManager().totalSize * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK; //missile toob
        val += msc.getManagerContainer().getModulesMap().get((short) 38).getElementManager().totalSize * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK; //missile computer

        val += msc.getElementClassCountMap().get(ElementKeyMap.DAMAGE_BEAM_MODULE) * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK;
        val += msc.getElementClassCountMap().get(ElementKeyMap.DAMAGE_BEAM_COMPUTER) * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK;

        val += msc.getElementClassCountMap().get(ElementKeyMap.WEAPON_ID) * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK; //cannons
        val += msc.getElementClassCountMap().get(ElementKeyMap.WEAPON_CONTROLLER_ID) * STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK; //cannon computer

        val += msc.getManagerContainer().getModulesMap().get(STASH_ELEMENT).getElementManager().totalSize * STRUCTURE_POINTS_PER_CARGO_SPACE; //cargo

        if(msc.getManagerContainer() instanceof StationaryManagerContainer)
        val += ((StationaryManagerContainer<?>)msc.getManagerContainer()).getWarpgate().getElementManager().totalSize * STRUCTURE_POINTS_PER_WARPGATE; //warpgates

        if(msc.getManagerContainer() instanceof FactoryAddOnInterface) {
            FactoryAddOn fack = ((FactoryAddOnInterface)msc.getManagerContainer()).getFactory();
            for (short factoryID : fack.map.keySet()) {
                ManagerModuleCollection<?,?,?> factoriesOfType = fack.map.get(factoryID);
                if (factoriesOfType != null) {
                    val += factoriesOfType.getElementManager().totalSize * STRUCTURE_POINTS_PER_FACTORY_CAPABILITY; //any kind of factory
                }
            }
        }

        return val;
    }

    private void handleServerSide(Timer timer){
        if(segmentController.isFullyLoaded() && segmentController.completedFirstUpdate) handleServerSideInstantaneous();
    }

    private void handleServerSideInstantaneous(){
        boolean doGlobalUpdate = false;

        if(segmentController.getConfigManager().isActive(aegisInterference)){
            //TODO: Play particle effect on short timer?
            if(now() - lastInterferenceAlert > ALERT_NOTIFICATION_INTERVAL_MS){
                for (PlayerState playerHere : ((ManagedUsableSegmentController<?>) segmentController).getAttachedPlayers()) {
                    playerHere.sendServerMessage(Lng.astr(segmentController.getRealName() + " is experiencing Aegis Interference! Until this is resolved, your Aegis System defenses cannot function."), MESSAGE_TYPE_INFO);
                }
                lastInterferenceAlert = now();
            }
        }

        if(queuedStatus != null){
            virtualSystem.changeStatus(queuedStatus);
            remoteStatus = queuedStatus;
            queuedStatus = null;
            doGlobalUpdate = true;
        }

        AegisSystemStatus origStatus = virtualSystem.getStatus();
        SegmentPiece block = segmentController.getSegmentBuffer().getPointUnsave(getBlock());

        long collectionDelta = now() - lastColl;
        if (collectionDelta >= AEGIS_CORE_COLLECTION_INTERVAL_MS) {
            //collect cells from storage
            collectedCharge = collectFromLinkedStorage();
        }

        long updateDelta = now() - virtualSystem.getLastUpdate();
        if(updateDelta >= AEGIS_CORE_SYSTEM_UPDATE_INTERVAL_MS) {
            //do virtual system updates (status, etc)
            doUpdateFromServerLocalInfo();
            if (remoteStatus != virtualSystem.getStatus() || remoteCharge != virtualSystem.getAegisCharge() || remoteDisruptWindowStart != virtualSystem.getCurrentVulnerabilityStart()) {
                doGlobalUpdate = true;
                remoteStatus = virtualSystem.getStatus();
                remoteDisruptWindowStart = virtualSystem.getCurrentVulnerabilityStart();
                remoteCharge = virtualSystem.getAegisCharge();
            }

            if (remoteStatus != origStatus) {
                Vector3f loc = block.getWorldPos(new Vector3f(), segmentController.getSectorId());
                Vector3i tmp = new Vector3i();
                Collection<PlayerState> players = GameServerState.instance.getPlayerStatesByName().values();

                if (!GameCommon.isDedicatedServer()) players = getServerSendables(new ArrayList<>(players));

                if (virtualSystem.isActive() && (origStatus == DISABLED || origStatus == NO_CHARGE || origStatus == VULNERABLE || origStatus == ONLINING)) {
                    for (PlayerState player : players) {
                        if (player.getCurrentSector().equals(segmentController.getSector(tmp))) {
                            AudioUtils.serverPlaySound("0022_gameplay - cockpit warning beep", loc.x, loc.y, loc.z, 1.0f, 1.0f, player);
                            player.sendServerMessage(Lng.astr("The Aegis System on " + segmentController.getRealName() + " is now functioning!"), MESSAGE_TYPE_INFO);
                        } else if (player.getFactionId() != 0 && player.getFactionId() == segmentController.getFactionId()) {
                            player.sendServerMessage(Lng.astr("The Aegis System on " + segmentController.getRealName() + "(Sector " + segmentController.getSector(tmp).toStringPure() + ")" + " is now functioning!"), MESSAGE_TYPE_INFO);
                        }
                    }
                } else if (remoteStatus == VULNERABLE && (origStatus == ACTIVE_HACKED || origStatus == ACTIVE)) {
                    for (PlayerState player : players) {
                        if (player.getCurrentSector().equals(segmentController.getSector(tmp))) {
                            AudioUtils.serverPlaySound("0022_item - forcefield powerdown", loc.x, loc.y, loc.z, 1.0f, 1.0f, player);
                            AudioUtils.serverPlaySound("0022_item - use special light beam", loc.x, loc.y, loc.z, 1.0f, 1.0f, player);
                            player.sendServerMessage(Lng.astr("The Aegis System on " + segmentController.getRealName() + " is offline due to disruption!"), MESSAGE_TYPE_INFO);
                        } else if (player.getFactionId() != 0 && player.getFactionId() == segmentController.getFactionId()) {
                            player.sendServerMessage(Lng.astr("The Aegis System on " + segmentController.getRealName() + "(Sector " + segmentController.getSector(tmp).toStringPure() + ")" + " is now offline due to disruption!"), MESSAGE_TYPE_INFO);
                        }
                    }
                } else if (remoteStatus == NO_CHARGE && (origStatus == ACTIVE || origStatus == ACTIVE_HACKED)) {
                    for (PlayerState player : players) {
                        if (player.getCurrentSector().equals(segmentController.getSector(tmp))) {
                            AudioUtils.serverPlaySound("0022_item - forcefield powerdown", loc.x, loc.y, loc.z, 1.0f, 1.0f, player);
                            AudioUtils.serverPlaySound("0022_item - use special light beam", loc.x, loc.y, loc.z, 1.0f, 1.0f, player);
                            player.sendServerMessage(Lng.astr("The Aegis System on " + segmentController.getRealName() + " is offline: out of Aegis Charge!"), MESSAGE_TYPE_INFO);
                        } else if (player.getFactionId() != 0 && player.getFactionId() == segmentController.getFactionId()) {
                            player.sendServerMessage(Lng.astr("The Aegis System on " + segmentController.getRealName() + "(Sector " + segmentController.getSector(tmp).toStringPure() + ")" + " has depleted its Aegis Charge!"), MESSAGE_TYPE_INFO);
                            //TODO: faction news?
                            //TODO: switch the sounds to a distance around the Aegis Core world pso
                        }
                    }
                }
            }
        }

        if(block != null && block.isActive() != (remoteStatus == ACTIVE || remoteStatus == ACTIVE_HACKED)){
            //block.setActive(remoteStatus == ACTIVE || remoteStatus == ACTIVE_HACKED); //it's a light; activation status should track with the system
            long a = ElementCollection.getEncodeActivation(block, true, remoteStatus == ACTIVE || remoteStatus == ACTIVE_HACKED, false);
            ((SendableSegmentController) segmentController).getBlockActivationBuffer().enqueue(a);
        }

        if(doGlobalUpdate) for(AegisSubsystem subsys : subsystems.values()){
            subsys.setSuppliedAegisCharge(virtualSystem.isActive());
        }

        //regenerate HP
        if(disruptionHP != disruptionHPMax && (now() - lastDisruptionDamage) > DISRUPTION_REGEN_OUTAGE_TIME_MS) {
            doGlobalUpdate = true; //TODO: synchro spam?
            if (disruptionHP < disruptionHPMax && virtualSystem.isActive()) {
                if (disruptionHP < 0) disruptionHP = 0;
                double delta = now() - lastServerUpdate;
                disruptionHP += min(disruptionHPMax - disruptionHP, disruptionHPMax * DISRUPTION_REGEN_FRACTION_PER_SECOND * (float) (delta / 1000)); //ms to seconds
            } else disruptionHP = disruptionHPMax;
        }

        if(doGlobalUpdate){
            //TODO activate/deactivate adjacent activators
            syncToNearbyClientsSafe(this);
            syncSubsystems();
        }
        lastServerUpdate = now();
    }

    private void syncSubsystems() {
        for(AegisSubsystem subsys : subsystems.values()){
            syncToNearbyClientsSafe(subsys);
        }
    }

    private void populateSubsystemList(){
        //add subsystem MCMs
        synchronized (subsystems) {
            for (ModManagerContainerModule mcmodule : getManagerContainer().getModModuleMap().values()) {
                if (mcmodule instanceof AegisSubsystem && !subsystems.containsKey(mcmodule.getBlockId())) {
                    subsystems.put(mcmodule.getBlockId(), (AegisSubsystem) mcmodule);
                }
            }
        }
    }

    private void handleClientSide(Timer timer) {

    }

    private int collectFromLinkedStorage(){
        int cells = 0;
        double origCharge = virtualSystem.getAegisCharge() + collectedCharge;
        if((AEGIS_CORE_CELL_CAPACITY < 0 || origCharge < AEGIS_CORE_CELL_CAPACITY)) {
            Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> linkedBlocksByType;
            short aegisCell = elementEntries.get("Aegis Cell").id;
            for(long blockAbsIndex : blocks.keySet()){ //technically just supposed to be one, but meh
                linkedBlocksByType = findLinkedBlocks(segmentController, blockAbsIndex);
                for(short id : linkableInventoryBlocks) if(linkedBlocksByType.containsKey(id)){
                    LongOpenHashSet storageBlockPositions = linkedBlocksByType.get(id);
                    for(long abs : storageBlockPositions){
                        Inventory inventory = getInventoryAt(segmentController,abs);
                        if(inventory == null)
                            continue;
                        int available = inventory.getOverallQuantity(aegisCell);
                        int toTake;
                        int invChange;

                        if(AEGIS_CORE_CELL_CAPACITY > 0){

                            int freeCapacity = (int) (AEGIS_CORE_CELL_CAPACITY - Math.ceil(origCharge));
                            if(freeCapacity > available){
                                toTake = available;
                            } else toTake = freeCapacity;

                            if(AEGIS_CORE_COLLECTION_MAX_THROUGHPUT >= 0 && AEGIS_CORE_COLLECTION_MAX_THROUGHPUT < toTake){
                                toTake = AEGIS_CORE_COLLECTION_MAX_THROUGHPUT;
                            }
                        } else toTake = available;

                        if(toTake > 0) {
                            invChange = inventory.incExistingOrNextFreeSlotWithoutException(aegisCell, -toTake);
                            inventory.sendInventoryModification(invChange);
                            cells = toTake;
                        }
                    }
                }
            }
        } else if(origCharge > AEGIS_CORE_CELL_CAPACITY) cells = (int) (AEGIS_CORE_CELL_CAPACITY - Math.floor(origCharge)); //negative change
        lastColl = now();
        return cells;
    }

    public void doUpdateFromServerLocalInfo(){
        virtualSystem.giveCharge(collectedCharge);
        if(collectedCharge < 0){
            SegmentPiece seg = segmentController.getSegmentBuffer().getPointUnsave((Long) blocks.keySet().toArray()[0]);
            Vector3f loc = seg.getWorldPos(new Vector3f(), segmentController.getSectorId());
            dropItems(abs(collectedCharge), blocks.get(elementEntries.get("Aegis Cell").id), loc, segmentController.getSector(new Vector3i()));
        }
        collectedCharge = 0;
        virtualSystem.setChargeConsPerDay(calcAegisChargePerCycle());
        virtualSystem.setChargeConsRequiredToOnLine(calcAegisChargeToOnline());
        virtualSystem.setFaction(segmentController.getFactionId()); //TODO: segment controller faction change event to avoid this spam!
        virtualSystem.update();
    }

    public void doDisruptionDamage(float v) {
        disruptionHP -= v;
        lastDisruptionDamage = now();
        if (disruptionHP <= 0) handleCompleteDisruptionDamage();
    }

    public void handleCompleteDisruptionDamage(){
        if(!isOnServer()) return; //TODO error

        if (virtualSystem.getStatus() != DISABLED && virtualSystem.getStatus() != ERROR_STALE && !virtualSystem.isDisrupted()){
            virtualSystem.tryDisrupt();
            if(virtualSystem.isDisrupted()){
                SuccessfulDisruptParticleEffect.FireEffectServer(segmentController.getSectorId(), segmentController.getSector(new Vector3i()), segmentController.getWorldTransform().origin,new Vector3f(/*Zero velocity*/), (int) Math.max(Math.max(segmentController.getBoundingBox().sizeX(), segmentController.getBoundingBox().sizeY()), segmentController.getBoundingBox().sizeZ()));
                String message = segmentController.getRealName() + "has successfully been disrupted!\nIt will become vulnerable to attack on " + getFormattedGMTTimeAndDate(virtualSystem.getCurrentVulnerabilityStart()) + ".";
                Vector3i loc = segmentController.getSector(new Vector3i());
                Faction owner = GameServerState.instance.getFactionManager().getFaction(segmentController.getFactionId());
                boolean fallbackToSectorMessage = false;

                if(owner != null && segmentController.getFactionId() > 0) {
                    owner.broadcastMessage(Lng.astr("In sector " + loc + ", your Bastion Field on " + message), MESSAGE_TYPE_WARNING, GameServerState.instance);
                    sendFactionsDisruptionNotification(); //Gondor calls for aid!
                    //TODO Discord notify - can of worms though.
                } else fallbackToSectorMessage = true;

                if(lastHitter != 0) {
                    String ownerName = owner == null ? "an unclaimed" : owner.getName() + "'s";
                    Faction attacker = GameServerState.instance.getFactionManager().getFaction(lastHitter);
                    String attmsg = "In sector " + loc + ", " + ownerName + " Bastion Field on " + message;
                    attacker.broadcastMessage(Lng.astr(attmsg), MESSAGE_TYPE_INFO, GameServerState.instance);
                    if(owner == null){
                        FactionNewsPost fallbackNotif = new FactionNewsPost();
                        fallbackNotif.set(lastHitter,attacker.getName(),now(),"Neutral Aegis System disrupted at " + loc + "!",attmsg,0, false);
                        GameServerState.instance.getFactionManager().addNewsPostServer(fallbackNotif);
                    }
                } else fallbackToSectorMessage = true;

                if(fallbackToSectorMessage) segmentController.getRemoteSector().getServerSector().sendServerMessage(Lng.astr(message),MESSAGE_TYPE_INFO); //just send the info into the sector; what else can we do
            }
            lastHitter = 0;
        }
        disruptionHP = 0;
        //TODO Move disruption HP to the virtual system with lazy-simulated regen.
    }

    public void sendFactionsDisruptionNotification() {
        int owner = segmentController.getFactionId();
        int attacker = lastHitter;
        boolean attackerIsPlayerFaction = (attacker > 0);
        FactionManager facman = GameServerState.instance.getFactionManager();
        long vulnStart = virtualSystem.getCurrentVulnerabilityStart();
        String entityName;
        String assetType;
        String entityType;
        switch (segmentController.getType()){
            case ASTEROID:
            case ASTEROID_MANAGED:
                assetType = "asteroid base";
                entityType = "asteroid";
                entityName = segmentController.getRealName();
                break;
            case SPACE_STATION:
                assetType = "space station";
                entityType = assetType;
                entityName = segmentController.getRealName();
                break;
            case PLANET_SEGMENT:
                assetType = "planetary base";
                entityType = "planet";
                entityName = ((Planet)segmentController).getCore().getRealName();
                break;
            case PLANET_ICO: //one can always hope...
                assetType = "planetary base";
                entityType = "planet";
                entityName = segmentController.getRealName();
                break;
            default:
                assetType = "asset";
                entityType = segmentController.getType().getName(); //should never fire
                entityName = segmentController.getRealName();
        }

        String defenderNews = "Your " + assetType + ", " + (entityType.equals("planet")? "on " : "") + entityName + ", in sector " + virtualSystem.getSectorLocation().toString() + ",\n has experienced Aegis System disruption due to the actions of ";
        if(attackerIsPlayerFaction){
            defenderNews += facman.getFactionName(attacker);
        } else defenderNews += "an unknown aggressor";
        defenderNews += "!\r\n";
        defenderNews += "It will become vulnerable at " + getFormattedGMTTimeAndDate(vulnStart) + '.';
        //TODO: Get downtime in minutes
        //TODO: Send to allies? Maybe make that a new faction menu option idk
        FactionNewsPost notification = new FactionNewsPost();
        notification.set(owner,facman.getFactionName(owner),now(),"Aegis System Compromised At " + virtualSystem.getSectorLocation().toString() + "!",defenderNews,0, false); //wtf is that last "permission" value?
        facman.addNewsPostServer(notification);

        if(segmentController.getOwnerState() != null){
            AbstractOwnerState personalOwner = segmentController.getOwnerState();
            if(personalOwner instanceof PlayerState){
                ((PlayerState) personalOwner).getClientChannel().getPlayerMessageController().serverSend("Aegis System",personalOwner.getName(),"Your Aegis System Was Compromised At" + virtualSystem.getSectorLocation().toString() + "!", defenderNews);
                //TODO: test this!
            }
        }

        if(attackerIsPlayerFaction){
            String attackerNews = "You or your comrades have successfully disrupted the Aegis Bastion Field of " + facman.getFactionName(owner) + "'s " + assetType + ", \n" + entityName + ", in sector " + virtualSystem.getSectorLocation().toString();
            attackerNews += "!\r\n";
            attackerNews += "The " + entityType + "'s Aegis System will go offline and the " + assetType + " will become vulnerable at " + getFormattedGMTTimeAndDate(vulnStart) + '.';
            FactionNewsPost anotification = new FactionNewsPost();
            anotification.set(attacker,facman.getFactionName(attacker),now(),"Bastion Disruption Succeeded At " + virtualSystem.getSectorLocation().toString() + "!",attackerNews,0, false);
            facman.addNewsPostServer(anotification);
        }
    }


    private int calcAegisChargeToOnline() {
        float structurePoints = getStructurePoints(false); //if we include docks it just means you undock everything before onlining lol
        int result = AEGIS_ONLINING_COST_BASE + Math.round(AEGIS_ONLINING_COST_PER_STRUCTURE_POINT * structurePoints);
        for(AegisSubsystem subsystem : subsystems.values()){
            if(subsystem.isPresentAndLinked()) result += subsystem.getAegisChargeConsumptionToPutOnline(structurePoints); //TODO: * enhancer count?
        }
        return result;
    }

    /**
     * @return Aegis Charge requirement per day cycle
     */
    private int calcAegisChargePerCycle() {
        int result = 0;
        for(AegisSubsystem subsystem : subsystems.values()){
            if(subsystem.isPresentAndLinked()) result += subsystem.getAegisChargeConsumptionPerDay(getStructurePoints(subsystem.includesDockedInCostCalculation())); //TODO: * enhancer count?
        }
        return result;
    }

    @Override
    public void handlePlace(long absIndex, byte orientation) {
        super.handlePlace(absIndex, orientation);
        if (!isInitialized()) tryInit();
    }

    @Override
    public void handleRemove(long absIndex) {
        super.handleRemove(absIndex);
        if(this.blocks.isEmpty() && !segmentController.getRemoteSector().isWrittenForUnload()){ //TODO: We need this determination to actually work
            String side = isOnServer()? "[SERVER]" : "[CLIENT]";
            System.out.println("[MOD][BastionInitiative]"+side+" Last Aegis Core removed from " + segmentController.getRealName() + ". Cleaning up and deleting aegis system...");

            if(isOnServer() && virtualSystem != null) {
                //Drop remaining charge as Aegis Cell items in space.
                int fuelToDrop = (int)Math.floor(virtualSystem.getAegisCharge());
                if(fuelToDrop > 0) {
                    short fuelID = elementEntries.get("Aegis Cell").id;
                    SegmentPiece seg = segmentController.getSegmentBuffer().getPointUnsave(absIndex);
                    Vector3f loc = seg.getWorldPos(new Vector3f(), segmentController.getSectorId());
                    dropItems(fuelToDrop, fuelID, loc, segmentController.getSector(new Vector3i()));
                }
                virtualSystem.setCharge(0);
            }

            //De-register aegis system from persistence container, deactivate and remove
            deinitialize();
        }
    }

    private void deinitialize(){
        //subsystems.clear(); //DO NOT DO THIS. Those subsystems are just inactive now. They aren't going anywhere, so having to re-add them later is silly.
        if(isInitialized() && isOnServer()) {
            virtualSystem.handleDestroyed();
            virtualSystem = null;
        }
        for(AegisSubsystem sbs : subsystems.values()){
            sbs.onDeInitialize();
        }
        initialized = false;
        if(isOnServer()) syncToNearbyClientsSafe(this);
    }

    public void processActivationInput(boolean isActivate) {
        if(isOnServer() && !virtualSystem.isDisrupted()){ //if it's disrupted you're not allowed to turn it off
            if(isActivate) virtualSystem.activate();
            else virtualSystem.changeStatus(DISABLED);
            handleServerSideInstantaneous();
        }
    }

    @Override
    public double getPowerConsumedPerSecondResting() {
        return powerConsumption();
    }

    @Override
    public double getPowerConsumedPerSecondCharging() {
        return powerConsumption();
    }

    private double powerConsumption(){
        if (initialized) {
            if (isOnServer()) {
                double truePowerCons;
                if (virtualSystem.isActive())
                    truePowerCons = AEGIS_CORE_BASE_POWER_PER_STRUCTURE_POINT * getStructurePoints(true);
                else truePowerCons = 0;
                remotePowerCons = truePowerCons;
                return truePowerCons;
            }
            else return remotePowerCons;
        }
        else{
            remotePowerCons = 0;
            return 0;
        }
    }

    @Override
    public boolean isPowerCharging(long l) {
        return true;
    }

    @Override
    public void setPowered(float v) {
        super.setPowered(v);
    }

    @Override
    public float getPowered() {
        return super.getPowered();
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return super.getPowerConsumerCategory();
    }

    @Override
    public void reloadFromReactor(double secTime, Timer timer, float tickTime, boolean powerCharging, float poweredResting) {
        super.reloadFromReactor(secTime, timer, tickTime, powerCharging, poweredResting);
    }

    @Override
    public boolean isPowerConsumerActive() {
        if(!isInitialized()) return false;
        AegisSystemStatus status;
        if(isOnServer() && virtualSystem != null) {
            status = virtualSystem.getStatus();
        } else status = remoteStatus;
        return !(status == DISABLED || status == ERROR_STALE);
    }

    @Override
    public void dischargeFully() {
        super.dischargeFully();
    }

    @Override
    public void onReceiveDataServer(PacketReadBuffer b) throws IOException {
        super.onReceiveDataServer(b);
    }

    @Override
    public void onTagSerialize(PacketWriteBuffer b) throws IOException {
        b.writeBoolean(initialized);

        b.writeFloat(disruptionHP);

         if(isOnServer() && virtualSystem != null) remoteStatus = virtualSystem.getStatus();
        b.writeInt(remoteStatus.ordinal());

        b.writeDouble(remotePowerCons);

         if(isOnServer() && virtualSystem != null) remoteCycleTime = virtualSystem.getNextCycleTime().getTimeInMillis();
        b.writeLong(remoteCycleTime);

         if(isOnServer() && virtualSystem != null) remoteOnliningCost = virtualSystem.getChargeConsToPutOnline();
        b.writeInt(remoteOnliningCost);

        if(isOnServer() && virtualSystem != null) remoteCycleCost = virtualSystem.getChargeConsPerDay();
        b.writeDouble(remoteCycleCost);

        if(isOnServer() && virtualSystem != null) remoteOnliningCost = virtualSystem.getChargeConsToPutOnline();
        b.writeInt(remoteOnliningCost);

        if(isOnServer() && virtualSystem != null) remoteCharge = virtualSystem.getAegisCharge();
        b.writeDouble(remoteCharge);

        if(isOnServer() && virtualSystem != null) remoteDisruptWindowStart = virtualSystem.getCurrentVulnerabilityStart();
        b.writeLong(remoteDisruptWindowStart);
    }

    @Override
    public void onTagDeserialize(PacketReadBuffer b) throws IOException {
        if(!loaded) onLoad();
        boolean remoteInitialized = b.readBoolean();
        if(initialized && !remoteInitialized) deinitialize();
        else if(!isOnServer()) initialized = remoteInitialized;

        disruptionHP = b.readFloat();

        remoteStatus = AegisSystemStatus.values()[b.readInt()];

        remotePowerCons = b.readDouble();

        remoteCycleTime = b.readLong();

        remoteOnliningCost = b.readInt();

        remoteCycleCost = b.readDouble();

        remoteOnliningCost = b.readInt();

        remoteCharge = b.readDouble();

        try {
            remoteDisruptWindowStart = b.readLong();
        } catch (EOFException ignore) {
            remoteDisruptWindowStart = 0L;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    public String getStatusString() {
        if(isOnServer()){
            if(virtualSystem != null) return virtualSystem.getStatus().getNiceName();
            else return "No Virtual System Connected";
        }
        else return remoteStatus.name();
    }

    public ArrayList<String> getInfoAsServer(){ //due to MMCM weirdness, on client this would not work
        String activationStateString;
        boolean lastStatus = (getRemoteStatus() != DISABLED); //true = active
        if (!lastStatus) activationStateString = "activate";
        else activationStateString = "deactivate";

        ArrayList<String> content = getAttachedSystemInfo();
        content.add("--- --- ---");
        content.add("Disruption HP: " + disruptionHP + '/' + disruptionHPMax);
        content.add("");
        content.add("Current Charge: " + getRemoteCharge());
        content.add("");
        content.add("Would you like to " + activationStateString + " this Aegis System?");
        if(getRemoteStatus() == DISABLED) content.add("- The recycle time would be set to now, " + TemporalShortcuts.getFormattedGMTTime(now()));
        String onliningInfo;
        if(getRemoteStatus() == NO_CHARGE) {
            onliningInfo = "- Bringing this system back online requires " + (getRemoteCycleCost() - getRemoteCharge()) + " more Aegis Charge.";
        } else if(lastStatus) { //system online
            onliningInfo = "- Once deactivated, bringing this system back online would consume " + getRemoteOnliningCost() + " Aegis Charge.";
        } else { //system offline
            if(getRemoteOnliningCost() > getRemoteCharge()) {
                onliningInfo = "- Bringing this system online requires " + (getRemoteOnliningCost() - getRemoteCharge()) + " more Aegis Charge.";
            } else onliningInfo = "- Bringing this system online will consume " + (getRemoteOnliningCost()) + " Aegis Charge.";
        }
        content.add(onliningInfo);
        return content;
    }

    public AegisSystemStatus getRemoteStatus() {
        return remoteStatus;
    }
    public double getRemoteCharge() {
        return remoteCharge;
    }

    public int getRemoteOnliningCost() {
        return remoteOnliningCost;
    }


    public double getRemoteCycleCost() {
        return remoteCycleCost;
    }

    public void giveCharge(int charge) {
        virtualSystem.giveCharge(charge);
    }

    public void setNewRefresh(long newCycleTime) {
        virtualSystem.setOrQueueNewRecycleTime(newCycleTime);
    }

    private ArrayList<String> getAttachedSystemInfo() {
        ArrayList<String> lines = new ArrayList<>();
        lines.add("((- (Aegis Core) -))");
        lines.add("Status: " + remoteStatus.getNiceName());
        lines.add("Structure Rating: " + getStructurePoints(false));
        lines.add("With Docks: " + getStructurePoints(true));
        lines.add("Aegis Charge Consumption Per Day: " + remoteCycleCost);
        lines.add("Systems: ");
        for(AegisSubsystem sys : subsystems.values()){
            if(sys.isPresentAndLinked()) {
                lines.add("\r\n - " + sys.getFullSystemName() + " -\r\n(" + sys.getShortPurposeString() + ')');
                lines.add("\r\nEnhancers: " + sys.getEnhancement());
                lines.add("Power Consumption: " + sys.getPowerConsumptionActive());
                lines.add("Aegis Consumption: " + sys.getAegisChargeConsumptionPerDay(getStructurePoints(sys.includesDockedInCostCalculation())));
                List<String> infoLines = sys.getInfoLines();
                if(infoLines != null) lines.addAll(sys.getInfoLines());
            }
        }
        return lines;
    }

    public long getBlock() {
        return (long) blocks.keySet().toArray()[0];
    }

    //TODO: Display block variable!
    public double getMaxHP() {
        return disruptionHPMax;
    }

    public void setMaxHP(float v) {
        if(disruptionHP == disruptionHPMax) disruptionHP = v;
        disruptionHPMax = v;
        if(disruptionHP > v) disruptionHP = v;
    }

    public void setLastHitter(int factionId) {
        lastHitter = factionId;
    }

    //TODO: Display block variable! Also sensor variable!
    public double getCurrentHP() {
        return disruptionHP;
    }

    public boolean canDisrupt() {
        AegisSystemStatus s = getRemoteStatus();
        return (s == ACTIVE || s == NO_CHARGE);
    }

    public void DEBUG_clearDisrupt() {
        virtualSystem.DEBUG_clearDisruptInfo();
        virtualSystem.changeStatus(ACTIVE);
        disruptionHP = disruptionHPMax;
    }

    public VirtualAegisSystem getVirtualSystem() {
        return virtualSystem;
    }

    public AegisSystemUIPromptPacket getUIInformationPacket() {
        return new AegisSystemUIPromptPacket(
                segmentController.getUniqueIdentifier(),
                getBlock(),
                virtualSystem.isActivatedState(),
                virtualSystem.getStatus() == ONLINING,
                getAttachedSystemInfoAsString(),
                virtualSystem.getRecycleTime(),
                virtualSystem.getRecycleDuration(),
                virtualSystem.isDisrupted(),
                virtualSystem.getTimeChangeCooldownRemaining(),
                virtualSystem.isQueuedForChange() ? virtualSystem.recycleTimeAfterChange() : -1L,
                virtualSystem.getChargeConsToPutOnline(),
                virtualSystem.getChargeConsPerDay(),
                virtualSystem.getAegisCharge(),
                virtualSystem.getMaxChargeCapacity(),
                virtualSystem.getFuelledUntil(),
                virtualSystem.getCurrentVulnerabilityStart(),
                virtualSystem.getStatus()
        );
    }

    private String getAttachedSystemInfoAsString() {
        StringBuilder result = new StringBuilder();
        for(String str : getAttachedSystemInfo()) result.append(str).append("\r\n");
        return result.toString();
    }

    public void onSubsystemLinkageChanged() {
        if (isOnServer()) {
            queueStatusChange(DISABLED); //queue status change ensures it won't try to change a nonexistent virtual system's status or get the status change lost
            if (remoteStatus != DISABLED) {
                Object[] msg = Lng.astr("A subsystem was added, removed, linked, or unlinked. The Aegis System on " + segmentController.getRealName() + "is now disabled.");
                Collection<PlayerState> players = GameServerState.instance.getPlayerStatesByName().values();

                if (!GameCommon.isDedicatedServer()) players = getServerSendables(new ArrayList<>(players));

                Vector3i tmp = new Vector3i();
                for (PlayerState player : players) {
                    if (player.getCurrentSector().equals(segmentController.getSector(tmp))) {
                        player.sendServerMessage(msg, MESSAGE_TYPE_INFO);
                    }
                }
            }
        }
    }

    private void queueStatusChange(AegisSystemStatus v) {
        if(isOnServer()) {
            queuedStatus = v;
        } //else invalid operation
    }

    public long getRemoteDisruptWindowStart() {
        return remoteDisruptWindowStart;
    }
}
