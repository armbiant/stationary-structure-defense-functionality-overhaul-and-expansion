package org.ithirahad.bastioninitiative.gui.corepanel;

import org.ithirahad.bastioninitiative.gui.flighthud.DisruptionProgressHUDBar;
import org.ithirahad.bastioninitiative.gui.old.AegisSystemSimpleInput;

public class BIGUIInstanceContainer { //BI GUI, not Big UI! :P
    public static AegisSystemSimpleInput temporaryAegisSystemDialog;

    public static DisruptionProgressHUDBar disruptionDatalinkBarHUD;

    public static AegisSystemManagementPanel aegisSystemPanel;
    public static AegisSystemManagementControlManager aegisSystemUIControlManager;

    public static RecalibrationTimeInput recalibrationTimeDialog;
}
