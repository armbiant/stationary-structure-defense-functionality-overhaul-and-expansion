package org.ithirahad.bastioninitiative.util;

import api.common.GameServer;
import api.utils.game.module.ModManagerContainerModule;
import api.utils.particle.ModParticleUtil;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.element.ControlElementMapper;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

import javax.vecmath.Vector3f;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.schema.game.common.data.player.faction.FactionRelation.RType.FRIEND;

public class BIUtils {
    public static void syncToNearbyClientsSafe(ModManagerContainerModule toSynch){
        if (!toSynch.isOnServer()) {
            throw new IllegalStateException("Method cannot be called from client");
        } else {
            for (PlayerState player : ModParticleUtil.getPlayersInRange(toSynch.getManagerContainer().getSegmentController().getSector(new Vector3i()))) {
                RegisteredClientOnServer remoteClient = GameServer.getServerClient(player);
                if (remoteClient != null && remoteClient.getProcessor() != null) {
                    toSynch.syncToClient(player);
                }
            }

        }
    }

    public static boolean existsInDB(String uid){
        try {
            return GameServerState.instance.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(uid, 1).size() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static ManagerContainer<?> getManagerContainer(SegmentController sgc){
        if(sgc instanceof ManagedUsableSegmentController){
            return ((ManagedUsableSegmentController<?>)sgc).getManagerContainer();
        }
        else return null;
    }

    /**
     * Returns map of blocks linked to a specified block.
     * Use <code>SegmentController.getSegmentBuffer().getPointUnsave(absIndex)</code> to get the SegmentPiece at a given index.<br/><br/>
     * <b>NOTE: StarMade can leave "ghost" entries in these hashmaps with empty hashsets, so in order to determine if a given block type is linked,
     * you must ensure that the hashmap not only contains a key, but that the stored <code>FastCopyLongOpenHashSet</code> is not empty.</b>
     * @param in The entity to look for linked blocks inside
     * @param linkedTo The abs. index of the "master" block
     * @return The map of absolute indices of blocks linked to the block at index "linkedTo", by type.
     */
    public static Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> findLinkedBlocks(SegmentController in, long linkedTo){
        Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> result = new Short2ObjectOpenHashMap<>();
        ControlElementMapper c = in.getControlElementMap().getControllingMap();
        if(c.containsKey(linkedTo)) result.putAll(c.get(linkedTo));
        return result;
    }

    /**
     * @param in The entity to look inside
     * @param absIndex The absolute index position of the storage block (cargo, factory, etc.)
     * @return the inventory held in this block position.
     */
    public static Inventory getInventoryAt(SegmentController in, long absIndex){
        if(in instanceof ManagedUsableSegmentController) {
            return ((ManagedUsableSegmentController<?>) in).getInventory(ElementCollection.getPosIndexFrom4(absIndex));
        } else return null;
    }

    /**
     * ManagerContainers won't bother with calling handlePlace unless the block's ID is marked "true" in this weird boolean array of specialBlocks,
     * derived from specialBlocksStatic if not present.
     * It's hard to tell honestly, but it looks as though any normal ManagerModuleCollection related block will be added,
     * along with a hard-coded selection of other blocks that need the special handling... however ModManagerContainerModule tracked blocks won't,
     * so we have this function which will slip our block in edgewise until StarLoader sorts this weirdness out.
     * <br/><br/>
     * TODO: This does not work for some reason, which made my life mildly more annoying as I had to turn any 'special' blocks into lights. :P <br/>
     *  -Ith
     * @param managerContainer The target managerContainer
     * @param blockID The ID we want to set as "special"
     */
    public static void setBlockSpecialInContainer(ManagerContainer<?> managerContainer, short blockID){
        try {
            Class<? extends ManagerContainer> cls = (managerContainer instanceof ShipManagerContainer) ? ShipManagerContainer.class : StationaryManagerContainer.class;
            Field arr = cls.getDeclaredField("specialBlocksStatic"); //why tf is this even duplicated? idk tbh
            arr.setAccessible(true);
            Object specialBlocksArrayObj = arr.get(null); // (In truth, it's really a boolean array.)
            if(specialBlocksArrayObj == null){
                Method gsm = ManagerContainer.class.getDeclaredMethod("getSpecialMap", boolean[].class);
                gsm.setAccessible(true);
                boolean[] newVal = new boolean[ElementKeyMap.highestType + 1]; //just copying the internal initialization
                gsm.invoke(managerContainer,newVal);
                gsm.setAccessible(false);
                arr.set(null, newVal);
                specialBlocksArrayObj = arr.get(null);
            }
            java.lang.reflect.Array.setBoolean(specialBlocksArrayObj, blockID, true); //im a special boi
            arr.setAccessible(false);

            Method afterInitialize = cls.getDeclaredMethod("afterInitialize");
            afterInitialize.setAccessible(true);
            afterInitialize.invoke(managerContainer); //copies from the static to the variable that's actually used. (Not sure why this is so complex, or WHY THE HELL THIS PROPERTY IS NOT IN ELEMENTINFORMATION
            afterInitialize.setAccessible(false);

            System.out.println("[MOD][BastionInitiative] Set " + ElementKeyMap.getInfo(blockID).getName() + " as a special block.");
        } catch(Throwable ex){
            ex.printStackTrace();
        }
    }

    public static List<SegmentController> getSelfAndAllDocks(SegmentController root, List<SegmentController> out, boolean searchOtherPlanetPlatesIfPresent){
        if(root == null) return out;
        if(searchOtherPlanetPlatesIfPresent && root.getType() == SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT){
            List<SegmentController> plates = new ArrayList<>();
            getAllPlates((Planet)root,plates);
            for(SegmentController plate : plates) getSelfAndAllDocks(plate,out,false);
        }
        else{
            out.add(root);
            for(RailRelation d : root.railController.next){
                getSelfAndAllDocks(d.docked.getSegmentController(), out, false);
            }
        }
        return out;
    }

    public static List<SegmentController> getAllPlates(Planet onePlate, List<SegmentController> out) {
        if(onePlate == null || onePlate.getCore() == null) return out; //no core to reference; can't do anything
        StateInterface core = onePlate.getCore().getState();

        synchronized (core.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            for (Sendable sendableInSector : core.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values()) {

                if (sendableInSector instanceof Planet && ((Planet) sendableInSector).getPlanetCoreUID().equals(onePlate.getPlanetCoreUID())) {
                    out.add((Planet) sendableInSector);
                }
            }
        }

        return out;
    }

    /**
    @return the point on the line segment between "lineP1" and "lineP2" that is closest to the point "point".<br/>
     Behaviour is not necessarily defined if the point is "ahead of" or "behind" the line segment...
     */
    public static Vector3f nearestApproachOnLineSegment(Vector3i lineP1, Vector3i lineP2, Vector3i point) {
        Vector3f result = lineP2.toVector3f();
        result.sub(lineP1.toVector3f()); //span between P1 and P2
        result.normalize(); //just the direction

        Vector3f toPoint = point.toVector3f();
        toPoint.sub(lineP1.toVector3f()); //span between P1 and point

        result.scale(toPoint.dot(result));

        return result;
    }

    //TODO: switch fully to Vector3fs
    public static float shortestDistanceToLineSegmentSquared(Vector3i lineP1, Vector3i lineP2, Vector3i point) {
        Vector3f l2 = lineP2.toVector3f();
        l2.sub(lineP1.toVector3f());
        float segmentLengthSquared = l2.lengthSquared();
        if (segmentLengthSquared == 0) return Vector3i.getDisatanceSquaredD(lineP1,point);
        float t = (((float)point.x - (float)lineP1.x) * ((float)lineP2.x - (float)lineP1.x) + ((float)point.y - (float)lineP1.y) * ((float)lineP2.y - (float)lineP1.y) + ((float)point.z - (float)lineP1.z) * ((float)lineP2.z - (float)lineP1.z)) / segmentLengthSquared;
        t = max(0,min(t,1));
        return getDistanceFromSectorSquared(point, new Vector3f(lineP1.x + t * (lineP2.x - lineP1.x), lineP1.y + t * (lineP2.y - lineP1.y), lineP1.z + t * (lineP2.z - lineP1.z)));
    }

    private static float getDistanceFromSectorSquared(Vector3i sector, Vector3f pos){
        float x2 = sector.x - pos.x;
        x2 *= x2;

        float y2 = sector.y - pos.y;
        y2 *= y2;

        float z2 = sector.z - pos.z;
        z2 *= z2;

        return x2+y2+z2;
    }

    public static boolean isSelfOrAlly(int self, int otherFaction){
        if(self == 0) return false; //it's neutral, so there can be no allies. For the purposes of this system decloak it anyway
        else return (self == otherFaction || GameServerState.instance.getFactionManager().getRelation(self, otherFaction) == FRIEND);
    }

    public static boolean factionAccessMayBeAllowed(int playerFaction, int structureFaction) {
        return structureFaction == 0 || playerFaction == structureFaction;
    }

    public static void dropItems(int quantity, short type, Vector3f location, Vector3i sector){
        try {
            RemoteSector sec = GameServerState.instance.getUniverse().getSector(sector).getRemoteSector();
            sec.addItem(location, type, -1, quantity);
        } catch(Exception ex) {
            System.err.println("[MOD][BastionInitiative][ERROR] Unable to drop loose items: " + quantity + " of " + ElementKeyMap.getInfo(type).name + " in sector " + sector);
            ex.printStackTrace();
        }
    }

    public static boolean isSunRangeServer(SegmentController segmentController) {
        if(GameServerState.instance == null) throw new RuntimeException("ERROR: Method should not be called on client!");
        float heatRange = GameServerState.instance.getGameConfig().sunMinIntensityDamageRange;
        Sector sector = segmentController.getRemoteSector().getServerSector();
        StellarSystem system = sector._getSystem();
        if(system.isHeatDamage(null /*unused*/, sector._getSunIntensity(), sector._getDistanceToSun(), heatRange)){
            return true;
        }
        return false;
    }
}
