package org.ithirahad.bastioninitiative.persistence;

import java.io.Serializable;

import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.*;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;

public abstract class VirtualAegisSubsystem implements Serializable {
    /**
     * The parent PersistentAegisSystem.
     */
    private transient VirtualAegisSystem parent;
    private boolean powered;

    public VirtualAegisSubsystem(){}

    public int getFactionID() {
        return parent.getFactionID();
    }

    public abstract String getBlockEntryName();

    public abstract void onLoadFromPersistence();

    protected abstract void onUpdate(boolean powered);

    public final VirtualAegisSystem getParent(){
        return parent;
    }

    public final void update(){
        onUpdate(powered);
    }

    public final void setPowered(boolean powered){
        this.powered = powered;
    };

    public boolean isOperating(boolean respectDisruptionStatus){
        boolean active;
        if(respectDisruptionStatus) active = parent.isActive();
        else active = parent.isActive() || parent.getStatus() == VULNERABLE;
        return powered &&
                !(parent == null) &&
                active &&
                now() < parent.getFuelledUntil();
    }

    public final void setParent(VirtualAegisSystem parent){
        this.parent = parent;
    };

    public void onDestroy(){
        parent = null;
    }
}
