package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class AegisEnhancementElementManager extends UsableControllableElementManager<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager> {
    private final short subsysCoreID;
    private final short enhancerID;

    public AegisEnhancementElementManager(SegmentController segmentController, short subsysCoreID, short enhancerID) {
        super(subsysCoreID, enhancerID, segmentController);
        this.subsysCoreID = subsysCoreID;
        this.enhancerID = enhancerID;
    }

    public short getEnhancerID() {
        return enhancerID;
    }

    public short getSubsysCoreID() {
        return subsysCoreID;
    }

    @Override
    public boolean isCheckForUniqueConnections() {
        return true;
    }

    @Override
    public ControllerManagerGUI getGUIUnitValues(AegisEnhancerUnit relevantUnit, AegisEnhancementCollectionManager primaryCol, ControlBlockElementCollectionManager<?, ?, ?> secondaryCol, ControlBlockElementCollectionManager<?, ?, ?> tertiaryCol) {
        return ControllerManagerGUI.create((GameClientState) getState(), "Aegis Subsystem Enhancer Unit", relevantUnit);
    }

    @Override
    protected String getTag() {
        return "mainreactor";
    } //empty, ergo no errors possible :DDDDD

    @Override
    public AegisEnhancementCollectionManager getNewCollectionManager(SegmentPiece pos, Class<AegisEnhancementCollectionManager> clazz) {
        return new AegisEnhancementCollectionManager(getEnhancerID(), pos, getSegmentController(), this);
    }

    @Override
    public String getManagerName() {
        return Lng.str(ElementKeyMap.getInfo(subsysCoreID).name + " Collective");
    }

    @Override
    protected void playSound(AegisEnhancerUnit aegisEnhancerUnit, Transform transform) {

    }

    @Override
    public void handle(ControllerStateInterface controllerStateInterface, Timer timer) {

    }
}
