package org.ithirahad.bastioninitiative.events;

import api.listener.events.Event;
import api.mod.StarMod;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSubsystem;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisSubsystem;
import org.ithirahad.bastioninitiative.entitysystems.aegis.PersistentAegisSubsystem;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class AegisSubsystemRegisterEvent extends Event {
    private final SegmentController seg;
    private final ManagerContainer<?> cont;
    private final Set<AegisSubsystem> modules = new HashSet<>();
    public AegisSubsystemRegisterEvent(SegmentController segmentController, ManagerContainer<?> container) {
        seg = segmentController;
        cont = container;
    }

    public ManagerContainer<?> getContainer() {
        return cont;
    }

    public SegmentController getSegmentController(){
        return seg;
    }

    public <T extends PersistentAegisSubsystem<V>, V extends VirtualAegisSubsystem> void addPersistentSubsystem(Class<T> subsystem, Class<V> virtual, String elementEntryName, StarMod modInstance){
        try {
            modules.add(subsystem.getConstructor(seg.getClass(), cont.getClass(), StarMod.class, String.class, Class.class).newInstance(seg, cont, modInstance, elementEntryName, virtual));
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public <T extends AegisSubsystem> void addSubsystem(Class<T> cls, StarMod modInstance, short controllerID){
        try {
            if(cls.isAssignableFrom(PersistentAegisSubsystem.class)) throw new IllegalArgumentException("Your Aegis Subsystem is persistent! Use addPersistentSubsystem() instead.");
            modules.add(cls.getConstructor(seg.getClass(), cont.getClass(), StarMod.class, short.class).newInstance(seg, cont, modInstance, controllerID));
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public Iterator<AegisSubsystem> getAddedSubsystems(){
        return modules.iterator();
    }
}
