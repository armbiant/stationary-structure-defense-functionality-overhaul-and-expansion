package org.ithirahad.bastioninitiative.persistence;

import api.listener.events.entity.ShipJumpEngageEvent;
import org.schema.game.common.controller.ManagedUsableSegmentController;

import static org.ithirahad.bastioninitiative.BIConfiguration.AEGIS_ANTI_FTL_ALLOWED;

public class VirtualFTLWall extends VirtualFTLInterdictorAbstract {
    @Override
    public String getBlockEntryName() {
        return "Aegis FTL Wall";
    }

    @Override
    public boolean isOperating(boolean respectDisruptionStatus) {
        return super.isOperating(respectDisruptionStatus) && AEGIS_ANTI_FTL_ALLOWED;
    }

    @Override
    public void handleEntityJumping(ShipJumpEngageEvent info) {
        //TODO: cut off ANY jump that counts as "inbound", i.e. ends closer to the FTL wall while within it.
        // If jump comes from outside, stop it at the place where the bubble would be crossed. If jump comes from inside, interdict (or dump them in same sector)
    }

    @Override
    public void handleEntityWarpSpace(ManagedUsableSegmentController<?> entity) {
        // TODO: maybe use kinetics to repel ships??? WS may be abandoned so no need to think too hard about this I guess, but I don't want to drop support (yet)
    }
}
