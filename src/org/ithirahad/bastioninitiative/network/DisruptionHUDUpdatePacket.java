package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.gui.corepanel.BIGUIInstanceContainer;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

/**
Packet sent by servers to clients aboard a ship using a Disruption Datalink.
 Updates the client HUD with the current disruption progress (and causes it to be shown, if hidden).
 */
public class DisruptionHUDUpdatePacket extends Packet {
    boolean canBeDisrupted = false; //greys out UI. Can't be disrupted if already successfully disrupted or vulnerable.
    double currentHP = 0;
    double maxHP = 0;
    public DisruptionHUDUpdatePacket(double currentHP, double maxHP, boolean disruptable) {
        this.currentHP = currentHP;
        this.maxHP = maxHP;
        canBeDisrupted = disruptable;
    }

    public DisruptionHUDUpdatePacket(){};

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        canBeDisrupted = b.readBoolean();
        currentHP = b.readDouble();
        maxHP = b.readDouble();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeBoolean(canBeDisrupted);
        b.writeDouble(currentHP);
        b.writeDouble(maxHP);
    }

    @Override
    public void processPacketOnClient() {
        BIGUIInstanceContainer.disruptionDatalinkBarHUD.updateInfo(canBeDisrupted,currentHP,maxHP);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //wat
    }
}
