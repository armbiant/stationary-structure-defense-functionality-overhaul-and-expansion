package org.ithirahad.bastioninitiative.world;

public class SystemControlSheet {
    /*
    TODO:
     **GENERAL**
     The more control a faction has, the better their mining bonus, and at certain control levels new options open up.
     e.g. a faction can levy taxes on shops above 70% control,
     and - provisionally, pending Phase 3 station systems - have better intel on stuff in their territory above say 50% control.
     Whichever faction has a plurality of control gets the technical "claim" on the system though.
     ...
     **NPC INTERACTION**
     Force 100% control for NPCs in NPC territory for now.
     Otherwise, systems start off with some % "Neutral" control (immediately filled by claiming faction)
     and the remaining % "Outlaw" control (slowly fills by claiming faction if uncontested; fill rate boosted by eliminating/neutralizing pirate stations)
     The ratio depends on the amount of NPC stations - we will assume that 1/3 belong to Pirates and work from there.
     ...
     **CONTIGUITY**
     In order for contiguous territory to matter,
     owning a neighboring territory should boost your claim by 2x iff you have any source of claim at all.
     Being a homebase should boost your claim by like 3x.
     (On that note, maybe homebase invulnerability should drop if, despite the giant claim boost, you manage to lose the plurality of control.
     At least, it should be an option.)
     ...
    */
}
