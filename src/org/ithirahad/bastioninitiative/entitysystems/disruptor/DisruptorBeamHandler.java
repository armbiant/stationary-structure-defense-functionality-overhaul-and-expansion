package org.ithirahad.bastioninitiative.entitysystems.disruptor;

import com.bulletphysics.collision.dispatch.CollisionObject;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.BastionField;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Collection;

import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.ACTIVE_HACKED;
import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.VULNERABLE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

public class DisruptorBeamHandler extends BeamHandler {
    private static final Vector4f haxGreen = new Vector4f(0.13f,0.8f,0f,1f);

    public DisruptorBeamHandler(SegmentController segmentController, BeamHandlerContainer<?> owner) {
        super(segmentController, owner);
    }

    @Override
    public boolean canhit(BeamState con, SegmentController controller, String[] cannotHitReason, Vector3i position) {
        return !controller.equals(getBeamShooter());
    }

    @Override
    public boolean ignoreBlock(short type) {
        ElementInformation f = ElementKeyMap.getInfoFast(type);
        return f.isDrawnOnlyInBuildMode() && !f.hasLod();
    }
    @Override
    public float getBeamTimeoutInSecs() {
        return BEAM_TIMEOUT_IN_SECS;
    }

    @Override
    public float getBeamToHitInSecs(BeamState beamState) {
        return beamState.getTickRate();
    }

    @Override
    protected boolean isHitsCubesOnly() {
        return true;
    }
    @Override
    protected boolean canHit(CollisionObject obj) {
        return !(obj.getUserPointer() instanceof StabilizerPath);
    }

    @Override
    public int onBeamHit(BeamState hittingBeam, int hits, BeamHandlerContainer<SegmentController> container, SegmentPiece hitPiece, Vector3f from,
                         Vector3f to, Timer timer, Collection<Segment> updatedSegments) {
        //TODO do disruption damage if right target, mark client target for HUD bar and reset last used time (for hide timing) to now
        SegmentController root = hitPiece.getSegmentController().railController.getRoot();
        if(isOnServer() &&
            root instanceof ManagedUsableSegmentController<?> &&
            !(root instanceof Ship))
        { //else no need to waste our time
            ManagedUsableSegmentController<?> structure = (ManagedUsableSegmentController<?>) root;
            BastionField f = (BastionField) structure.getManagerContainer().getModMCModule(elementEntries.get("Aegis Invulnerability").id);
            f = f.getReferenceModule();

            if(f.getParentCore() != null) {
                SegmentController shooter = getBeamShooter();
                if(shooter != null) { //dunno how this would happen, but meh
                    VirtualAegisSystem.AegisSystemStatus status = f.getParentCore().getRemoteStatus();
                    if (status == ACTIVE_HACKED || status == VULNERABLE)
                        getBeamShooter().sendServerMessage(Lng.str("Cannot Use Disruption Datalink: System is already disrupted!"),MESSAGE_TYPE_WARNING);
                    else if(status == VirtualAegisSystem.AegisSystemStatus.DISABLED)
                        getBeamShooter().sendServerMessage(Lng.str("Cannot Use Disruption Datalink: No Aegis System is active on the structure!\nStructure is already vulnerable to standard weaponry."),MESSAGE_TYPE_WARNING);
                    else if(!f.getCachedActiveStatus())
                        getBeamShooter().sendServerMessage(Lng.str("Cannot Use Disruption Datalink: No functional Bastion Field to interface with!\nStructure is already vulnerable to standard weaponry."),MESSAGE_TYPE_WARNING);
                    else{
                        DisruptionNotifier.onDisrupt(getBeamShooter().getFactionId(),structure);
                        f.doDisruptionDamage(hittingBeam.getPower(), getBeamShooter().getFactionId());
                    }
                }
            }
            f.sendDisruptionUIInfo(getBeamShooter());
        }
        //TODO: play particle effect if timer sufficient
        return hits;
    }

    @Override
    protected boolean onBeamHitNonCube(BeamState con, int hits,
                                       BeamHandlerContainer<SegmentController> owner, Vector3f from,
                                       Vector3f to, CubeRayCastResult cubeResult, Timer timer,
                                       Collection<Segment> updatedSegments) {
        return false;
    }

    @Override
    protected boolean ignoreNonPhysical(BeamState con) {
        return false;
    }

    @Override
    protected Vector4f getDefaultColor(BeamState beamState) {
        return haxGreen;
    }
}
