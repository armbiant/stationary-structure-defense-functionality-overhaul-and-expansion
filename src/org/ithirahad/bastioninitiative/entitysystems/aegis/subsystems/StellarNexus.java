package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.persistence.VirtualStellarNexus;
import org.ithirahad.bastioninitiative.entitysystems.aegis.PersistentAegisSubsystem;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Timer;

import javax.annotation.CheckForNull;
import java.util.List;

import static org.ithirahad.bastioninitiative.BIConfiguration.*;

public class StellarNexus extends PersistentAegisSubsystem<VirtualStellarNexus> {
    public StellarNexus(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, "Aegis Stellar Nexus", VirtualStellarNexus.class);
    }

    @Override
    public void onSerialize(PacketWriteBuffer b){
    }

    @Override
    public void onDeserialize(PacketReadBuffer b){
    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        return STELLAR_NEXUS_FUEL_BASE;
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return STELLAR_NEXUS_ONLINING_COST;
    }

    @Override
    public double getPowerConsumptionActive() {
        return STELLAR_NEXUS_POWER_CONSUMPTION;
    }

    @Override
    public String getName() {
        return ElementKeyMap.getInfo(getBlockId()).getName();
    }

    @Override
    public String getFullSystemName() {
        return "Aegis Stellar Nexus Subsystem";
    }

    @CheckForNull
    @NotImplemented
    @Override
    public List<String> getInfoLines() {
        return null;
    }

    @Override
    protected void handleServer(Timer timer, boolean hasBlocks) {
        //TODO if more than one in faction in system, apply aegis interference to everything!
        // Once there is more than one interference source, we're going to need a consensus system to avoid constantly adding/stripping interference for different reasons.
    }

    @Override
    public String getShortPurposeString() {
        return "Stakes your faction's claim on the star system.";
    }
}
