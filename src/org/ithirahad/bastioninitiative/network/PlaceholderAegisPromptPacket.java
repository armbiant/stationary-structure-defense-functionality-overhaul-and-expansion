package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;
import java.util.ArrayList;

import static org.ithirahad.bastioninitiative.gui.corepanel.BIGUIInstanceContainer.temporaryAegisSystemDialog;

/**
 * Prompts clients to open the placeholder/temporary Aegis System information and control dialogue window.
 */
public class PlaceholderAegisPromptPacket extends Packet {
    ArrayList<String> reply;
    boolean lastActivationState;
    String entity;
    long absp;

    public PlaceholderAegisPromptPacket(){}

    public PlaceholderAegisPromptPacket(ArrayList<String> info, boolean lastActivationState, String entityUID, long absPos) {
        reply = info;
        this.lastActivationState = lastActivationState;
        entity = entityUID;
        absp = absPos;
    }

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        reply = b.readStringList();
        lastActivationState = b.readBoolean();
        entity = b.readString();
        absp = b.readLong();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeStringList(reply);
        b.writeBoolean(lastActivationState);
        b.writeString(entity);
        b.writeLong(absp);
    }

    @Override
    public void processPacketOnClient() {
        temporaryAegisSystemDialog.activate(reply, lastActivationState, entity, absp);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {/*what?*/}
}
