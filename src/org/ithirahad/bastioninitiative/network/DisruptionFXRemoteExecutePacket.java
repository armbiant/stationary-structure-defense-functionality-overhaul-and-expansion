package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.vfx.particle.DecloakParticleEffect;
import org.ithirahad.bastioninitiative.vfx.particle.SuccessfulDisruptParticleEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;

public class DisruptionFXRemoteExecutePacket extends Packet {
    private int sectorId;
    private Vector3i sector;
    private Vector3f position;
    private Vector3f velocity;
    private int diameterMultiplier;
    private Vector4f color;

    public DisruptionFXRemoteExecutePacket(){}

    public DisruptionFXRemoteExecutePacket(int sectorId, Vector3i sector, Vector3f position, Vector3f velocity, int diameterMultiplier, Vector4f color) {
        this.sectorId = sectorId;
        this.sector = sector;
        this.position = position;
        this.velocity = velocity;
        this.diameterMultiplier = diameterMultiplier;
        this.color = color;
    }

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        sectorId = b.readInt();
        sector = b.readVector();
        position = b.readVector3f();
        velocity = b.readVector3f();
        diameterMultiplier = b.readInt();
        color = b.readVector4f();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeInt(sectorId);
        b.writeVector(sector);
        b.writeVector3f(position);
        b.writeVector3f(velocity);
        b.writeInt(diameterMultiplier);
        b.writeVector4f(color);
    }

    @Override
    public void processPacketOnClient() {
        SuccessfulDisruptParticleEffect.FireLineCloudEffectClient(sectorId,sector,position,velocity,diameterMultiplier,color);
        //TODO: fire a burst or something?
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //how tf
    }
}
