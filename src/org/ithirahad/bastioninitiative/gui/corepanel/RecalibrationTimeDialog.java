package org.ithirahad.bastioninitiative.gui.corepanel;

import api.utils.gui.GUIInputDialogPanel;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;

import java.util.Calendar;

import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_HOURS;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_MINUTES;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.msToTimeToday;

public class RecalibrationTimeDialog extends GUIInputDialogPanel {
    GUITextOverlay infoOverlay;
    GUIDropDownList hours;
    private int hour = 0;
    GUIDropDownList minutes;
    private int minute = 0;

    public RecalibrationTimeDialog(InputState inputState, GUICallback guiCallback) {
        super(inputState, "Aegis System Recalibration Time Changer", "SET RECALIBRATION TIME", "", 350, 250, guiCallback); //third string is description text, which overlaps top text...
    }

    @Override
    public void onInit() {
        super.onInit();
        GUIContentPane contentPane = ((GUIDialogWindow) background).getMainContentPane();
        //Get content pane
        contentPane.setTextBoxHeightLast(75);
        //This just ensures the content pane doesn't have a height of 0, the actual height will automatically
        //adjust to fill the area when it's drawn

        GUIAncor text = contentPane.getContent(0);

        infoOverlay = new GUITextOverlay(10, 10, getState());
        infoOverlay.autoWrapOn = text;
        //Have the text overlay automatically wrap text to fit the content area
        infoOverlay.onInit();
        //Initialize overlay
        infoOverlay.setTextSimple("Set the time (hours and minutes) in GMT when the Aegis System should enter recalibration. New time will be queued if changing the time is still on cooldown.");
        infoOverlay.autoWrapOn = text;
        infoOverlay.setFont(FontLibrary.getBlenderProBook14());
        text.attach(infoOverlay);

        GUIInnerTextbox dropDownsArea = contentPane.addNewTextBox(0,(int)(contentPane.getHeight()/2));

        hours = new GUIDropDownList(getState(), 50, 30, 150, new DropDownCallback() { //state, width, height, expHeight, callback
            @Override
            public void onSelectionChanged(GUIListElement guiListElement) {
                hour = Integer.parseInt((String) guiListElement.getContent().getUserPointer()); //this is a cringe-inducing way to do this, but dropdowns don't appear to know what selection index they are at
                //I guess the 'right' way is for each list element to callback their value to the panel or something, but... ugh
            }
        });

        for(int i = 0; i <= 23; i++){
            if(i < 10) hours.add(createDropdownLine("0" + i));
            else hours.add(createDropdownLine(String.valueOf(i)));
        }

        minutes = new GUIDropDownList(getState(), 50, 30, 150, new DropDownCallback() {
            @Override
            public void onSelectionChanged(GUIListElement guiListElement) {
                minute = Integer.parseInt((String) guiListElement.getContent().getUserPointer());
            }
        });

        for(int i = 0; i <= 59; i++){
            if(i < 10)
                minutes.add(createDropdownLine("0" + i));
            else
                minutes.add(createDropdownLine(String.valueOf(i)));
        }
        //TODO: Horizontal arrange
        //TODO: 12-hour time with AM-PM and maybe even timezone changer. Thing is, 12-hour time is bad and awful and terrible and should burn. So... eh.

        hours.onInit();
        minutes.onInit();
        dropDownsArea.attach(hours);
        dropDownsArea.attach(minutes);
        hours.getPos().x += 13; //padding
        hours.getPos().y += 10;
        minutes.getPos().x += hours.getWidth() + hours.getPos().x;
        minutes.getPos().y = hours.getPos().y;
        dropDownsArea.onInit();
        orientate(ORIENTATION_HORIZONTAL_MIDDLE | ORIENTATION_VERTICAL_MIDDLE);
    }

    private GUIListElement createDropdownLine(String content) {
        GUIAncor c = new GUIAncor(getState(), 10, 24);
        GUITextOverlayTable t = new GUITextOverlayTable(10, 10, getState());
        t.setTextSimple(content);
        t.setPos(4, 4, 0);
        c.setUserPointer(content);
        c.attach(t);
        GUIListElement result = new GUIListElement(c, getState()); //copypasta; hopefully it works
        return result;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setCurrentTime(long recalibrationStart) {
        Calendar c = msToTimeToday(recalibrationStart);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        hours.setSelectedIndex(hour);
        minutes.setSelectedIndex(minute);
    }

    public long getSetTime() {
        return ((long) hour * MS_TO_HOURS) + ((long) minute * MS_TO_MINUTES);
    }
}
